---
title: Example Emails
sidebar: auto
# prev: false
# next: ./styles
---

## Standard

Customer, Client and User Messages

## Promotional

Campaigns and Lead Generation

## Previous

-   [link to proud email](https://1worldsync.com)
-   [hfhg](google.com)

<!--
  <ProfileCard img="https://placeimg.com/640/480/any">
    ### Profile card example
    - Any markdown
    - you put here
    > will be slotted in
  </ProfileCard>
-->
