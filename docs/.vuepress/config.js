const {path} = require("@vuepress/utils");
const {defaultTheme} = require('@vuepress/theme-default');
const {copyCodePlugin } = require("vuepress-plugin-copy-code2");
const {registerComponentsPlugin} = require("@vuepress/plugin-register-components");
// vuepress-plugin-container

module.exports = {
  lang: "en-US",
  title: "Email Design",
  base: '/',
  head: [
    ['link', { rel: 'preload', as: 'font', type: 'font/woff2', href: '/fonts/Inter-roman.var.woff2?v=3.19', crossorigin: 'anonymous' }],
    ['link', { rel: 'preload', as: 'font', type: 'font/woff2', href: '/fonts/Poppins-Variable.woff2', crossorigin: 'anonymous' }],
    ['link', { media: 'all', rel: 'stylesheet', href: '/fonts/poppins.css' }],
    ['link', { media: 'all', rel: 'stylesheet', href: '/fonts/inter.css' }],
    ['link', { media: 'all', rel: 'stylesheet', href: '/fonts/recursive.css' }]
  ],
  alias: {
    '@src': path.resolve(__dirname, './src'),
    '@demo': path.resolve(__dirname, './demo'),
    '@inc': path.resolve(__dirname, './partials'),
  },
  theme: defaultTheme({
    logo: '/images/logo-horizontal.svg',
    docsDir: 'docs',
    // docsRepo: 'https://bitbucket.org/1worldsync/1worldsync.bitbucket.io',
    // docsBranch: 'master',
    // editLinkPattern: ':repo/src/:branch/:path?mode=edit&at=master',
    navbar: [
      {
        text: 'Patterns',
        children: [
          '/patterns/buttons',
          '/patterns/header/',
        ]
      },
      {
        text: 'Tools',
        children: [
          {
            text: 'MJML',
            link: '/tools/index.html#mjml'
          },
          {
            text: 'Marketo',
            link: '/tools/index.html#marketo'
          }
        ]
      },
      {
        text: 'Examples',
        link: '/templates/'
      }
    ],
    sidebar: {
      '/patterns/': [
        {
          text: 'Patterns',
          children: [
            '/patterns/buttons/index.md',
            '/patterns/header/index.md',
            '/patterns/notification/index.md'
          ]
        }
      ],
      '/templates/': 'auto',
      '/tools/': 'auto'
    },
  }),
  plugins: [
    registerComponentsPlugin({
      componentsDir: path.resolve(__dirname, "components"),
    }),
    copyCodePlugin({})
  ],
  markdown: {
    anchor: {
      permalink: false
    },
    importCode: {
      handleImportPath: (str) => str.replace(/^@inc/, path.resolve(__dirname, "public/partials"))
    }
  },
  bundler: viteBundler({
    viteOptions: {
      css: {
        postcss: {
          plugins: [
            require('tailwindcss'),
            require('autoprefixer')
          ]
        }
      },
    },
    // vuePluginOptions: {
    //   template: {
    //     compilerOptions: {
    //       isCustomElement: (tag) => tag === 'center',
    //     },
    //   },
    // },
  }),
};
