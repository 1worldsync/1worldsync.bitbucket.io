---
home: true
editLink: false
heroImage: /images/cover.svg
tagline: An email framework to help us work together and create email consistently for all.

actions:
  - text: Get Started
    link: /templates/
    type: primary

  # - text: Grab Code
  #   link: /patterns/buttons/
  #   type: default

footer: Intended for anyone who works with outbound communication at 1WorldSync. 1WorldSync uses multiple platforms for operations across the organization to communicate. This resource gets you started in the right direction and helps us ensure brand consistency.
---
