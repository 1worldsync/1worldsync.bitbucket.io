---
title: Buttons
# prev: false
# next:
#   text: Blocks
#   link: Blocks.md
---

## Styles

Buttons are graphical links used to call attention to your message and signal the user where to click.
The button elements must be a block to be used in a layout.

[Try Online](https://mjml.io/try-it-live/W8gVCg7SK)

**Visual Hierarchy**

1.  [Primary / Bold](#primary)
2.  [Secondary / Subtle](#secondary)
3.  [Tertiary / link](#tertiary)
4.  [Default / Uncolored](#styles)

#### Base

<div class="split">

<p><img src="/images/spec-button.png" height="88" width="184"></p>

|         |                     |
| :------ | :------------------ |
| font    | 600 14px/16px Inter |
| height  | 40px                |
| radius  | 60px                |
| padding | 12px 24px           |

</div>

::: warning
<code>mj-classes</code> are required in <code>MJML</code> document header to generate button styles
:::

@[code html:no-line-numbers](@inc/button-attributes.mjml)

#### Default

<div class="split">

<Button type="default" />

|             |         |
| :---------- | :------ |
| font-weight | 700     |
| color       | #FFFFFF |
| background  | #787D84 |

</div>

::: details View Code

<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-button mj-class="button-cta">Primary </mj-button>
```

  </CodeGroupItem>
  <CodeGroupItem title="HTML">

@[code html:no-line-numbers](@inc/button-default.html)

  </CodeGroupItem>
</CodeGroup>

:::

::: tip
Add <code>button-cta-{style}</code> class to mj-class get variant styles
:::

<br>

### Primary

<div class="split">

<Button type="primary" />

|             |         |
| :---------- | :------ |
| font-weight | 700     |
| color       | #FFFFFF |
| background  | #008855 |

</div>

::: details View Code

<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-button mj-class="button-cta button-cta-bold">Primary</mj-button>
```

  </CodeGroupItem><CodeGroupItem title="HTML">

  @[code html:no-line-numbers](@inc/button-bold.html)

  </CodeGroupItem>
</CodeGroup>
:::

<br>

### Secondary

<div class="split">

<Button type="secondary" />

|             |         |
| :---------- | :------ |
| font-weight | 600     |
| color       | #343638 |
| background  | #E7E8E9 |

</div>

::: details View Code
<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-button mj-class="button-cta button-cta-subtle">Secondary</mj-button>
```

  </CodeGroupItem>
  <CodeGroupItem title="HTML">

@[code html:no-line-numbers](@inc/button-subtle.html)

  </CodeGroupItem>
</CodeGroup>
:::

<br>

### Tertiary

<div class="split">

<Button type="tertiary" />

|             |                                                                             |
| :---------- | :-------------------------------------------------------------------------- |
| font-weight | 600                                                                         |
| color       | #008855                                                                     |
| icon        | [arrow.png ](https://static.basedcdn.com/1ws/src/assets/arrow.png) 32w, 32h |

</div>

::: details View Code
<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-button mj-class="button-cta button-cta-link" href="javascript:void(0)"> Tertiary
  <img src="https://static.basedcdn.com/1ws/src/assets/arrow.png" style="vertical-align: middle; display: inline; margin-left: 6px" width="32" height="auto" alt="." />
</mj-button>
```

  </CodeGroupItem>
  <CodeGroupItem title="HTML">

@[code html:no-line-numbers](@inc/button-link.html)

  </CodeGroupItem>
</CodeGroup>
:::

## Blocks

Keep your content focused by not using more then a few primary buttons in you email. Include as many buttons as you need when composing sections of your email layout.

[Try Online](https://mjml.io/try-it-live/cvHdBxajR)

::: warning
If you are copying the HTML code you need to add the responsive styles to your document <code>head</code> for blocks to display correctly on mobile screens
:::

@[code html:no-line-numbers](@inc/styles.html)

### Single

<img src="/images/spec-button-stack-single.png" height="144" width="632">

#### Primary

<div class="preview">
  <div class="download-code-link"><a href="/demo/button-bold-block.html" class="download-code" download target="_blank" aria-label="Download code" data-balloon-pos="left"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M384 128h-128V0L384 128zM256 160H384v304c0 26.51-21.49 48-48 48h-288C21.49 512 0 490.5 0 464v-416C0 21.49 21.49 0 48 0H224l.0039 128C224 145.7 238.3 160 256 160zM255 295L216 334.1V232c0-13.25-10.75-24-24-24S168 218.8 168 232v102.1L128.1 295C124.3 290.3 118.2 288 112 288S99.72 290.3 95.03 295c-9.375 9.375-9.375 24.56 0 33.94l80 80c9.375 9.375 24.56 9.375 33.94 0l80-80c9.375-9.375 9.375-24.56 0-33.94S264.4 285.7 255 295z"/></svg></a></div>
  <iframe class="canvas canvas-72" src="/demo/button-bold-block.html" height="auto" width="100%" border="0"></iframe>
</div>

::: details View Code
<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-section>
  <mj-column>
    <mj-button mj-class="button-cta button-cta-bold" href="javascript:void(0)" align="center"> Primary </mj-button>
  </mj-column>
</mj-section>
```

  </CodeGroupItem>
  <CodeGroupItem title="HTML">

@[code html:no-line-numbers](@inc/button-bold-block.html)

  </CodeGroupItem>
</CodeGroup>
:::

#### Secondary

<div class="preview">
  <div class="download-code-link"><a href="/demo/button-subtle-block.html" class="download-code" download target="_blank" aria-label="Download code" data-balloon-pos="left"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M384 128h-128V0L384 128zM256 160H384v304c0 26.51-21.49 48-48 48h-288C21.49 512 0 490.5 0 464v-416C0 21.49 21.49 0 48 0H224l.0039 128C224 145.7 238.3 160 256 160zM255 295L216 334.1V232c0-13.25-10.75-24-24-24S168 218.8 168 232v102.1L128.1 295C124.3 290.3 118.2 288 112 288S99.72 290.3 95.03 295c-9.375 9.375-9.375 24.56 0 33.94l80 80c9.375 9.375 24.56 9.375 33.94 0l80-80c9.375-9.375 9.375-24.56 0-33.94S264.4 285.7 255 295z"/></svg></a></div>
  <iframe class="canvas canvas-72" src="/demo/button-subtle-block.html" height="auto" width="100%" border="0"></iframe>
</div>

::: details View Code
<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-section>
  <mj-column>
    <mj-button mj-class="button-cta button-cta-subtle" href="javascript:void(0)" align="center"> Secondary </mj-button>
  </mj-column>
</mj-section>
```

  </CodeGroupItem>
  <CodeGroupItem title="HTML">

@[code html:no-line-numbers](@inc/button-subtle-block.html)

  </CodeGroupItem>
</CodeGroup>
:::

#### Tertiary

<div class="preview">
  <div class="download-code-link"><a href="/demo/button-link-block.html" class="download-code" download target="_blank" aria-label="Download code" data-balloon-pos="left"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M384 128h-128V0L384 128zM256 160H384v304c0 26.51-21.49 48-48 48h-288C21.49 512 0 490.5 0 464v-416C0 21.49 21.49 0 48 0H224l.0039 128C224 145.7 238.3 160 256 160zM255 295L216 334.1V232c0-13.25-10.75-24-24-24S168 218.8 168 232v102.1L128.1 295C124.3 290.3 118.2 288 112 288S99.72 290.3 95.03 295c-9.375 9.375-9.375 24.56 0 33.94l80 80c9.375 9.375 24.56 9.375 33.94 0l80-80c9.375-9.375 9.375-24.56 0-33.94S264.4 285.7 255 295z"/></svg></a></div>
  <iframe class="canvas canvas-72" src="/demo/button-link-block.html" height="auto" width="100%" border="0"></iframe>
</div>

::: details View Code
<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-section>
  <mj-column>
    <mj-button mj-class="button-cta button-cta-link" href="javascript:void(0)" align="center"> Tertiary </mj-button>
  </mj-column>
</mj-section>
```

  </CodeGroupItem>
  <CodeGroupItem title="HTML">

@[code html:no-line-numbers](@inc/button-link-block.html)

  </CodeGroupItem>
</CodeGroup>
:::

### Split

These blocks contain two buttons that stack on mobile screens. Often times buttons that are horizontally aligned are related, an example would be a Create Account button next to a log in link.

<img src="/images/spec-button-stack-mobile.png" height="204" width="332">
<img src="/images/spec-button-stack.png" height="144" width="632">

<br>

<div class="preview">
  <div class="download-code-link"><a href="/demo/button-bold-split.html" class="download-code" download target="_blank" aria-label="Download code" data-balloon-pos="left"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M384 128h-128V0L384 128zM256 160H384v304c0 26.51-21.49 48-48 48h-288C21.49 512 0 490.5 0 464v-416C0 21.49 21.49 0 48 0H224l.0039 128C224 145.7 238.3 160 256 160zM255 295L216 334.1V232c0-13.25-10.75-24-24-24S168 218.8 168 232v102.1L128.1 295C124.3 290.3 118.2 288 112 288S99.72 290.3 95.03 295c-9.375 9.375-9.375 24.56 0 33.94l80 80c9.375 9.375 24.56 9.375 33.94 0l80-80c9.375-9.375 9.375-24.56 0-33.94S264.4 285.7 255 295z"/></svg></a></div>
  <iframe class="canvas canvas-128" src="/demo/button-bold-split.html" height="auto" width="100%" border="0"></iframe>
</div>

::: details View Code
<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-section>
  <mj-column>
    <mj-button mj-class="button-cta button-cta-bold" href="javascript:void(0)" align="right">Create Account
    </mj-button>
  </mj-column>
  <mj-column>
    <mj-button mj-class="button-cta button-cta-link" href="javascript:void(0)" align="left">Portal Login<img alt="alt" height="32" src="https://static.basedcdn.com/1ws/src/assets/arrow.png" style="vertical-align: middle; display: inline; margin-left: 6px" width="32"/>
    </mj-button>
  </mj-column>
</mj-section>
```

  </CodeGroupItem>
  <CodeGroupItem title="HTML">

@[code html:no-line-numbers](@inc/button-bold-split.html)

  </CodeGroupItem>
</CodeGroup>
:::

<br>

<div class="preview">
  <div class="download-code-link"><a href="/demo/button-subtle-split.html" class="download-code" download target="_blank" aria-label="Download code" data-balloon-pos="left"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M384 128h-128V0L384 128zM256 160H384v304c0 26.51-21.49 48-48 48h-288C21.49 512 0 490.5 0 464v-416C0 21.49 21.49 0 48 0H224l.0039 128C224 145.7 238.3 160 256 160zM255 295L216 334.1V232c0-13.25-10.75-24-24-24S168 218.8 168 232v102.1L128.1 295C124.3 290.3 118.2 288 112 288S99.72 290.3 95.03 295c-9.375 9.375-9.375 24.56 0 33.94l80 80c9.375 9.375 24.56 9.375 33.94 0l80-80c9.375-9.375 9.375-24.56 0-33.94S264.4 285.7 255 295z"/></svg></a></div>
  <iframe class="canvas canvas-128" src="/demo/button-subtle-split.html" height="auto" width="100%" border="0"></iframe>
</div>

::: details View Code
<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-section>
  <mj-column>
    <mj-button mj-class="button-cta button-cta-bold" href="javascript:void(0)" align="right">Register
    </mj-button>
  </mj-column>
  <mj-column>
    <mj-button mj-class="button-cta button-cta-subtle" href="javascript:void(0)" align="left">Log in
    </mj-button>
  </mj-column>
</mj-section>
```

  </CodeGroupItem>
  <CodeGroupItem title="HTML">

@[code html:no-line-numbers](@inc/button-subtle-split.html)

  </CodeGroupItem>
</CodeGroup>
:::

<br>

<div class="preview">
  <div class="download-code-link"><a href="/demo/button-link-split.html" class="download-code" download target="_blank" aria-label="Download code" data-balloon-pos="left"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><path fill="currentColor" d="M384 128h-128V0L384 128zM256 160H384v304c0 26.51-21.49 48-48 48h-288C21.49 512 0 490.5 0 464v-416C0 21.49 21.49 0 48 0H224l.0039 128C224 145.7 238.3 160 256 160zM255 295L216 334.1V232c0-13.25-10.75-24-24-24S168 218.8 168 232v102.1L128.1 295C124.3 290.3 118.2 288 112 288S99.72 290.3 95.03 295c-9.375 9.375-9.375 24.56 0 33.94l80 80c9.375 9.375 24.56 9.375 33.94 0l80-80c9.375-9.375 9.375-24.56 0-33.94S264.4 285.7 255 295z"/></svg></a></div>
  <iframe class="canvas canvas-128" src="/demo/button-link-split.html" height="auto" width="100%" border="0"></iframe>
</div>

::: details View Code
<CodeGroup>
  <CodeGroupItem title="MJML" active>

```html:no-line-numbers
<mj-section>
  <mj-column>
    <mj-button mj-class="button-cta button-cta-subtle" href="javascript:void(0)" align="right">Learn More
    </mj-button>
  </mj-column>
  <mj-column>
    <mj-button mj-class="button-cta button-cta-link" href="javascript:void(0)" align="left">View All<img alt="alt" height="32" src="https://static.basedcdn.com/1ws/src/assets/arrow.png" style="vertical-align: middle; display: inline; margin-left: 6px" width="32"/>
    </mj-button>
  </mj-column>
</mj-section>
```

  </CodeGroupItem>
  <CodeGroupItem title="HTML">

@[code html:no-line-numbers](@inc/button-link-split.html)

  </CodeGroupItem>
</CodeGroup>
:::
