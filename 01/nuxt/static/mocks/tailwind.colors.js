const resolveConfig = require("tailwindcss/resolveConfig");
const tailwindConfig = require("./tailwind.config.js");

const fullConfig = resolveConfig(tailwindConfig);

let output = ":root {\n";
for(const key of Object.keys(fullConfig.theme.colors)) {
  const value = fullConfig.theme.colors[key];
  if(typeof value === "string") {
    output += `  --${key}: ${value};\n`;
  } else {
    for(const colorKey of Object.keys(value)) {
      const colorValue = value[colorKey];
      output += `  --${key}-${colorKey}: ${colorValue};\n`;
    }
  }
}
output += "}\n";

console.log(output);

// node tailwind.colors.js > tailwind.colors.css