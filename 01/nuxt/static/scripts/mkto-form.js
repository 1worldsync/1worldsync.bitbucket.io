MktoForms2.loadForm("//app-sj25.marketo.com", "980-UUZ-493", 3077);
MktoForms2.whenReady(function (form) {
  form.addHiddenFields({
    _mkt_trk: null,
  });
  form.getFormElem()[0].setAttribute("data-mkto-ready", "true");
  form.onValidate(function () {
    var vals = form.vals();
    if (vals.honeyPot == "") {
      form.submittable(true);
    } else {
      alert("Gotcha");
      form.submittable(false);
    }
  });
  form.onSubmit(function (form) {
    form.vals({
      _mkt_trk: "",
    });
  });
  form.onSuccess(function (values, followUpUrl) {
    form.getFormElem().hide();
    document.getElementById("mktoThanks").style.display = "flex";
    return false;
  });
});
