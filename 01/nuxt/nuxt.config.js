export default {
  ssr: true,
  target: "static",
  router: {
    base: "/lib/",
  },
  head: {
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/lib/favicon.ico" },
      // { rel: "dns-prefetch", href: "//app-sj25.marketo.com" },
      // { rel: "dns-prefetch", href: "//munchkin.marketo.net" },
      { rel: "dns-prefetch", href: "//kit.fontawesome.com" },
    ],
    script: [
      { src: "https://kit.fontawesome.com/e8c799d635.js", crossorigin: "anonymous" },
    ],
  },
  module: {
    rules: [
      {
        test: /\.pug$/,
        loader: "vue-pug-loader",
      },
    ],
  },
  components: [
    { path: "@@/components/example", prefix: "lib", extensions: ["vue"] },
    { path: "@@/components/base", extensions: ["vue"] },
  ],
  buildModules: [
    ["@nuxt/postcss8"],
    ["@nuxtjs/moment", { defaultLocale: ["en"] }],
    ["@nuxtjs/svg"],
    [
      "@nuxtjs/tailwindcss",
      {
        cssPath: "@@/tailwind.css",
        configPath: "@@/tailwind.config.js",
        viewer: true,
        exposeConfig: true,
      },
    ],
    ["faker-nuxt", { locale: "en_US" }],
    ["nuxt-clipboard"],
    // [
    //   "nuxt-font-loader",
    //   {
    //     url: { local: "/lib/fonts/index.css" },
    //     prefetch: true,
    //     preconnect: true,
    //     stylesheet: true,
    //   },
    // ],
    ["nuxt-highlightjs", { style: "shades-of-purple" }],
  ],
  build: {
    loaders: { vue: { prettify: false } },
    postcss: {
      plugins: {
        "postcss-import": true,
        "postcss-nested": false,
        "postcss-pxtorem": {
          propList: [
            "background*",
            "border*",
            "bottom",
            "font*",
            "height",
            "left",
            "letter-spacing",
            "margin*",
            "max-*",
            "min-*",
            "padding*",
            "right",
            "text*",
            "top",
            "width",
          ],
        },
        cssnano: false,
      },
      order: [
        "tailwindcss/nesting",
        "postcss-pxtorem",
        "tailwindcss",
        "autoprefixer",
      ],
    },
    html: { minify: false },
    extractCSS: true,
    filenames: {
      css: ({ isDev }) => (isDev ? "[name].css" : "css/[name].css"),
      img: ({ isDev }) => (isDev ? "[path][name].[ext]" : "img/[name].[ext]"),
      font: ({ isDev }) =>
        isDev ? "[path][name].[ext]" : "fonts/[name].[ext]",
      video: ({ isDev }) =>
        isDev ? "[path][name].[ext]" : "videos/[name].[ext]",
    },
  },
  generate: {
    dir: "lib",
  },
};
