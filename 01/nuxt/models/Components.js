export default [
  {
    label: "Buttons",
    name: "Button",
    new: false,
    components: [
      { name: "Overview" },
      { name: "Colors" },
      { name: "Icons" },
      { name: "Sizes" },
      { name: "Styles" },
    ],
  },
  {
    label: "Inputs",
    name: "Input",
    new: false,
    components: [
      { name: "Overview" },
      { name: "Select" },
      { name: "Text" },
      { name: "Type" },
    ],
  },
  {
    label: "Blocks",
    name: "Block",
    new: false,
    components: [
      { name: "Pricing" },
      { name: "RichContent" },
      // { name: "Forms" },
    ],
  },
  {
    label: "Menus",
    name: "Nav",
    new: true,
    components: [
      { name: "Overview" },
      // { name: "MegaMenu" }
    ],
  },
];
