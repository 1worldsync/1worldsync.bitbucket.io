/**
 *	https://github.com/nathanheffley/tailwindcss-card -- plugin uses to build cards
 *  _______
 * 	CONFIG:
 *  card: theme => ({
 *    maxWidth: '24rem',
 *    borderRadius: '.25rem',
 *    boxShadow: '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
 *    padding: '1rem 1.5rem',
 *  })
 *  _______
 * 	MARKUP:
 *  <div class="card">
 *    <img class="card-image" src="https://example.com/example.jpg">
 *    <div class="card-content">
 *      <!-- Card Content -->
 *    </div>
 *  </div>
*/

module.exports = function({ addComponents, e, theme }) {
  const defaultTheme = {
    maxWidth: theme('maxWidth.sm', '24rem'),
    borderRadius: theme('borderRadius.default', '.25rem'),
    boxShadow: theme('boxShadow.lg', '0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)'),
    padding: `${theme('padding.4', '1rem')} ${theme('padding.6', '1.5rem')}`,
  }

  const userTheme = theme('card', {})
  const cardTheme = {...defaultTheme, ...userTheme}

  addComponents({
    '.card': {
      maxWidth: cardTheme.maxWidth,
      borderRadius: cardTheme.borderRadius,
      boxShadow: cardTheme.boxShadow,
      overflow: 'hidden',
    },

    '.card-image': {
      display: 'block',
      width: '100%',
    },

    '.card-content': {
      padding: cardTheme.padding,
    },
  })
}
