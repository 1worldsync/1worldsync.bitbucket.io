const plugin = require("tailwindcss/plugin");
const defaultTheme = require("tailwindcss/defaultTheme");
const colors = require("tailwindcss/colors");

module.exports = plugin(function ({ addBase, theme }) {
  addBase({
    [`body`]: {
      color: `rgb(var(--tw-color-dark) / var(--tw-text-opacity))`,
      "line-height": theme("lineHeight.relaxed"),
      "font-family": theme("fontFamily.sans"),
      "font-size": theme("baseFontSize"),
      "text-rendering": `optimizeLegibility`,
      "text-decoration-skip-ink": `auto`,
      "font-feature-settings": `'cv03' 0, 'cv04' 0, 'cv08' 1, 'cv09' 1, 'cv11' 1, 'cpsp' 0`,
      "font-synthesis": `weight`,
      "font-kerning": `normal`,
    },
    [`.text-link`]: {
      "font-weight": `theme('fontWeight.medium')`,
      color: `rgb(var(--tw-color-primary-dark) / var(--tw-text-opacity))`,
      "text-decoration-line": `underline`,
      "text-decoration-color": `rgb(var(--tw-color-primary) / .5)`,
      "text-decoration-thickness": `0.09375em`,
      "text-underline-offset": `0.09375em`,
      "&:hover": {
        "--tw-text-opacity": `1`,
        color: `rgb(var(--tw-color-primary-dark) / var(--tw-text-opacity))`,
        "text-decoration-color": `rgb(var(--tw-color-primary-dark) / var(--tw-text-opacity))`,
        // 'font-weight':               `theme('fontWeight.semibold')`,
      },
      "&:active": {
        "--tw-text-opacity": `1`,
        color: `rgb(var(--tw-color-primary-dark) / var(--tw-text-opacity))`,
        "text-decoration-color": `rgb(var(--tw-color-primary-dark) / var(--tw-text-opacity))`,
      },
    },
    // [`type='button', type='reset', type='submit'`]: {
    //   "background-color": `inherit`,
    //   background: `inherit`,
    //   color: `inherit`,
    // },
  });
});
