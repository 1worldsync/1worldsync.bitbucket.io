const plugin = require('tailwindcss/plugin')
const defaultTheme = require('tailwindcss/defaultTheme')
const colors = require('tailwindcss/colors')
const { spacing, borderWidth, borderRadius, fontWeight, lineHeight } = defaultTheme

const forms = plugin.withOptions(function(options = { strategy: undefined }) {
  return function({ addBase, addComponents, theme }) {
    const strategy = options.strategy === undefined ? ['base', 'class'] : [options.strategy]

    const rules = [
      {
        base: [
          "[type='text']",
          "[type='email']",
          "[type='url']",
          "[type='password']",
          "[type='number']",
          "[type='date']",
          "[type='datetime-local']",
          "[type='month']",
          "[type='search']",
          "[type='tel']",
          "[type='time']",
          "[type='week']",
          '[multiple]',
          'textarea',
          'select',
        ],
        class: ['.form-input', '.form-textarea', '.form-select', '.form-multiselect'],
        styles: {
          'appearance': `none`,
          // 'font-size': `var(--font-scale)`,
          'font-weight': theme('fontWeight.medium'),
          'line-height': theme('lineHeight.relaxed'),
          'color': `rgb(var(--tw-color-dark) / var(--tw-text-opacity))`,
          'background-color': `var(--tw-input-background)`,
          'border-color': `var(--tw-input-border)`,
          'border-radius': theme('borderRadius.lg'),
          'border-width': `var(--tw-input-border-width, 1.5px)`,
          'border-style': `solid`,
          'padding-bottom': theme('spacing[2.5]'),
          'padding-left': theme('spacing[3]'),
          'padding-right': theme('spacing[3]'),
          'padding-top': theme('spacing[2.5]'),
          // 'width':  '100%',
          '--tw-shadow': `0 0 transparent`,
          '--tw-border-opacity': `1`,
          '--tw-ring-inset': `var(--tw-empty,/*!*/ /*!*/)`,
          '--tw-ring-offset-width': `var(--tw-input-offset-size)`,
          '--tw-ring-offset-color': `rgb(var(--tw-color-light) / 10%)`,
          '--tw-ring-color': `rgb(var(--tw-color-primary-light) / 40%)`,
          '--tw-ring-offset-shadow': `var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color)`,
          '--tw-ring-shadow': `var(--tw-ring-inset) 0 0 0 calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color)`,
          '&:focus': {
            'outline': `var(--tw-ring-offset-width) solid transparent`,
            'outline-offset': `var(--tw-ring-offset-width)`,
            'box-shadow': `var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow)`,
            'border-color': `rgb(var(--tw-color-primary) / var(--tw-border-opacity))`,
            'background-color': `var(--tw-color-primary-light)`,
            'color': `rgb(var(--tw-color-accent))`,
          },
        },
      },
      {
        base: ['input::placeholder', 'textarea::placeholder'],
        class: ['.form-input::placeholder', '.form-textarea::placeholder'],
        styles: {
          '--tw-text-opacity': `.5`,
          'color': `rgb(var(--tw-color-dark) / var(--tw-text-opacity))`,
          'opacity': `1`,
          '&:active': {
            'color': `rgb(var(--tw-color-primary) / var(--tw-text-opacity))`,
          }
        },
      },
      {
        base: ['::-webkit-datetime-edit-fields-wrapper'],
        class: ['.form-input::-webkit-datetime-edit-fields-wrapper'],
        styles: {
          'padding': `0`,
        },
      },
      {
        // Unfortunate hack until https://bugs.webkit.org/show_bug.cgi?id=198959 is fixed.
        // This sucks because users can't change line-height with a utility on date inputs now.
        // Reference: https://github.com/twbs/bootstrap/pull/31993
        base: ['::-webkit-date-and-time-value'],
        class: ['.form-input::-webkit-date-and-time-value'],
        styles: {
          'min-height': `1.5em`,
        },
      },
      {
        // In Safari on macOS date time inputs are 4px taller than normal inputs
        // This is because there is extra padding on the datetime-edit and datetime-edit-{part}-field pseudo elements
        // See https://github.com/tailwindlabs/tailwindcss-forms/issues/95
        base: [
          '::-webkit-datetime-edit',
          '::-webkit-datetime-edit-year-field',
          '::-webkit-datetime-edit-month-field',
          '::-webkit-datetime-edit-day-field',
          '::-webkit-datetime-edit-hour-field',
          '::-webkit-datetime-edit-minute-field',
          '::-webkit-datetime-edit-second-field',
          '::-webkit-datetime-edit-millisecond-field',
          '::-webkit-datetime-edit-meridiem-field',
        ],
        class: [
          '.form-input::-webkit-datetime-edit',
          '.form-input::-webkit-datetime-edit-year-field',
          '.form-input::-webkit-datetime-edit-month-field',
          '.form-input::-webkit-datetime-edit-day-field',
          '.form-input::-webkit-datetime-edit-hour-field',
          '.form-input::-webkit-datetime-edit-minute-field',
          '.form-input::-webkit-datetime-edit-second-field',
          '.form-input::-webkit-datetime-edit-millisecond-field',
          '.form-input::-webkit-datetime-edit-meridiem-field',
        ],
        styles: {
          'padding-top': 0,
          'padding-bottom': 0,
        },
      },
      {
        base: ['select'],
        class: ['.form-select'],
        styles: {
          'background-image': `url("data:image/svg+xml;utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='none' opacity='0.5' viewBox='0 0 20 20'%3E%3Cpath stroke='currentColor' stroke-linecap='round' stroke-linejoin='round' stroke-width='1.5' d='M6 8l4 4 4-4'/%3E%3C/svg%3E")`,
          'background-position': `right ${spacing[2]} center`,
          'background-repeat': `no-repeat`,
          'background-size': `1.5em 1.5em`,
          'padding-right': theme('spacing[10]'),
          'color-adjust': `exact`,
          'cursor': `pointer`,
        },
      },
      {
        base: ['[multiple]'],
        class: null,
        styles: {
          'background-image': 'initial',
          'background-position': 'initial',
          'background-repeat': 'unset',
          'background-size': 'initial',
          'padding-right': theme('spacing[3]'),
          'color-adjust': 'unset',
        },
      },
      {
        base: [`[type='checkbox']`, `[type='radio']`],
        class: ['.form-checkbox', '.form-radio'],
        styles: {
          'appearance': 'none',
          'background-color': `var(--tw-input-background)`,
          'background-origin': 'border-box',
          'border-color': `var(--tw-input-border)`,
          'border-width': `var(--tw-input-border-width, 1.5px)`,
          'color': 'rgb(var(--tw-color-primary) / var(--tw-text-opacity))',
          'color-adjust': 'exact',
          'cursor': `pointer`,
          'display': 'inline-block',
          'flex-shrink': '0',
          'height': theme('spacing[5]'),
          'padding': '0',
          'user-select': 'none',
          'vertical-align': 'middle',
          'width': theme('spacing[5]'),
          '--tw-shadow': '0 0 transparent',
          '--tw-ring-offset-width': `var(--tw-input-offset-size)`,
          '--tw-ring-offset-color': 'rgb(var(--tw-color-light) / .1)',
          '--tw-ring-color': 'rgb(var(--tw-color-primary-light) / .4)',
        },
      },
      {
        base: [`[type='checkbox']`],
        class: ['.form-checkbox'],
        styles: {
          'border-radius': theme('borderRadius.sm'),
        },
      },
      {
        base: [`[type='radio']`],
        class: ['.form-radio'],
        styles: {
          'border-radius': '100%',
        },
      },
      {
        base: [`[type='checkbox']:focus`, `[type='radio']:focus`],
        class: ['.form-checkbox:focus', '.form-radio:focus'],
        styles: {
          'outline': 'var(--tw-ring-offset-width) solid transparent',
          'border-color': 'rgb(var(--tw-color-primary) / var(--tw-border-opacity))',
          'box-shadow': `var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow)`,
          'outline-offset': 'var(--tw-ring-offset-width)',
          '--tw-ring-inset': 'var(--tw-empty,/*!*/ /*!*/)',
          '--tw-ring-offset-shadow': `var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color)`,
          '--tw-ring-shadow': `var(--tw-ring-inset) 0 0 0 calc(.5px + var(--tw-ring-offset-width)) var(--tw-ring-color)`,
        },
      },
      {
        base: [`[type='checkbox']:checked`, `[type='radio']:checked`],
        class: ['.form-checkbox:checked', '.form-radio:checked'],
        styles: {
          'border-color': `transparent`,
          'background-color': `currentColor`,
          'background-size': `100% 100%`,
          'background-position': `center`,
          'background-repeat': `no-repeat`,
        },
      },
      {
        base: [`[type='checkbox']:checked`],
        class: ['.form-checkbox:checked'],
        styles: {
          'background-image': `url("data:image/svg+xml,%3Csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3E%3C/svg%3E")`,
        },
      },
      {
        base: [`[type='radio']:checked`],
        class: ['.form-radio:checked'],
        styles: {
          'background-image': `url("data:image/svg+xml,%3Csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3E%3Ccircle cx='8' cy='8' r='3'/%3E%3C/svg%3E")`,
        },
      },
      {
        base: [
          `[type='checkbox']:checked:hover`,
          `[type='checkbox']:checked:focus`,
          `[type='radio']:checked:hover`,
          `[type='radio']:checked:focus`,
        ],
        class: [
          '.form-checkbox:checked:hover',
          '.form-checkbox:checked:focus',
          '.form-radio:checked:hover',
          '.form-radio:checked:focus',
        ],
        styles: {
          'border-color': `transparent`,
          'background-color': `currentColor`,
        },
      },
      {
        base: [`[type='checkbox']:indeterminate`],
        class: ['.form-checkbox:indeterminate'],
        styles: {
          'background-image': `url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 16 16'%3E%3Cpath stroke='white' stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M4 8h8'/%3E%3C/svg%3E")`,
          'border-color': `transparent`,
          'background-color': `currentColor`,
          'background-size': `100% 100%`,
          'background-position': `center`,
          'background-repeat': `no-repeat`,
        },
      },
      {
        base: [`[type='checkbox']:indeterminate:hover`, `[type='checkbox']:indeterminate:focus`],
        class: ['.form-checkbox:indeterminate:hover', '.form-checkbox:indeterminate:focus'],
        styles: {
          'border-color': `transparent`,
          'background-color': `currentColor`,
        },
      },
      {
        base: [`[type='file']`],
        class: null,
        styles: {
          background: `unset`,
          'border-color': `inherit`,
          'border-width': 0,
          'border-radius': 0,
          'padding': 0,
          'font-size': `unset`,
          'line-height': `inherit`,
        },
      },
      {
        base: [`[type='file']:focus`],
        class: null,
        styles: {
          outline: `1px solid ButtonText`,
          outline: `1px auto -webkit-focus-ring-color`,
        },
      },
    ]

    const getStrategyRules = (strategy) => rules
      .map((rule) => {
        if(rule[strategy] === null) return null

        return {
          [rule[strategy]]: rule.styles
        }
      })
      .filter(Boolean)

    if(strategy.includes('base')) {
      addBase(getStrategyRules('base'))
    }

    if(strategy.includes('class')) {
      addComponents(getStrategyRules('class'))
    }
  }
})

module.exports = forms
