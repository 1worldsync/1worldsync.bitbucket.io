const defaultTheme = require("tailwindcss/defaultTheme");
const colors = require("tailwindcss/colors");

// Adds opacity to RGB CCS Varibles
function withOpacity(variableName) {
  return ({
    opacityValue
  }) => {
    if(opacityValue === undefined) {
      return `rgb(var(${variableName}))`;
    }
    return `rgb(var(${variableName}) / ${opacityValue})`;
  };
}

module.exports = {
  // important: "#twind",
  darkMode: "class",
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    fontFamily: {
      sans: ["var(--tw-font-content)", ...defaultTheme.fontFamily.sans],
      serif: ["var(--tw-font-serif)", ...defaultTheme.fontFamily.serif],
      mono: ["var(--tw-font-mono)", ...defaultTheme.fontFamily.mono],
    },
    extend: {
      fontFamily: {
        display: ["var(--tw-font-title)", ...defaultTheme.fontFamily.sans],
        icon: ["var(--tw-font-icon)", ...defaultTheme.fontFamily.mono],
      },
      colors: {
        "primary": withOpacity("--tw-color-primary"),
        "primary-dark": withOpacity("--tw-color-primary-dark"),
        "primary-light": withOpacity("--tw-color-primary-light"),
        "accent": withOpacity("--tw-color-accent"),
        "accent-dark": withOpacity("--tw-color-accent-dark"),
        "accent-light": withOpacity("--tw-color-accent-light"),
        "light": withOpacity("--tw-color-light"),
        "dark": withOpacity("--tw-color-dark"),
      },
    },
    container: {
      center: true,
      padding: "1em",
    },
  },
  plugins: [
    require("@tailwindcss/aspect-ratio"),
    require('@tailwindcss/line-clamp'),
    require("@tailwindcss/typography"),
    require("tailwindcss-debug-screens"),
    require("./assets/theme/base.js"),
    require("./assets/theme/inputs.js"),
  ],
};
