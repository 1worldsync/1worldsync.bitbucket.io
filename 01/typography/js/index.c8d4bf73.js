let $fb38370e8b5aa3c7$var$button = document.querySelector('.hamburger-button');
let $fb38370e8b5aa3c7$var$nav = document.querySelector('.docs-navigation');
let $fb38370e8b5aa3c7$var$main = document.querySelector('main');
let $fb38370e8b5aa3c7$var$header = document.querySelector('.docs-header');
// Hide everything except the button element.
let $fb38370e8b5aa3c7$var$ariaHide = (element, isHidden)=>{
    for (let child of element.children){
        if (!child.contains($fb38370e8b5aa3c7$var$button)) {
            if (isHidden) child.setAttribute('aria-hidden', 'true');
            else child.removeAttribute('aria-hidden');
        } else if (child !== $fb38370e8b5aa3c7$var$button) $fb38370e8b5aa3c7$var$ariaHide(child, isHidden);
    }
};
let $fb38370e8b5aa3c7$var$hide = ()=>{
    $fb38370e8b5aa3c7$var$nav.classList.remove('visible');
    $fb38370e8b5aa3c7$var$main.removeAttribute('aria-hidden');
    $fb38370e8b5aa3c7$var$nav.removeAttribute('tabindex');
    $fb38370e8b5aa3c7$var$button.setAttribute('aria-pressed', 'false');
    $fb38370e8b5aa3c7$var$ariaHide($fb38370e8b5aa3c7$var$header, false);
    if ($fb38370e8b5aa3c7$var$nav.contains(document.activeElement)) $fb38370e8b5aa3c7$var$button.focus();
};
$fb38370e8b5aa3c7$var$button.onclick = ()=>{
    $fb38370e8b5aa3c7$var$nav.classList.toggle('visible');
    if ($fb38370e8b5aa3c7$var$nav.classList.contains('visible')) {
        $fb38370e8b5aa3c7$var$main.setAttribute('aria-hidden', 'true');
        $fb38370e8b5aa3c7$var$button.setAttribute('aria-pressed', 'true');
        $fb38370e8b5aa3c7$var$ariaHide($fb38370e8b5aa3c7$var$header, true);
        $fb38370e8b5aa3c7$var$nav.tabIndex = -1;
        $fb38370e8b5aa3c7$var$nav.focus();
    } else $fb38370e8b5aa3c7$var$hide();
};
document.body.onclick = (e)=>{
    if (!$fb38370e8b5aa3c7$var$nav.contains(e.target) && !$fb38370e8b5aa3c7$var$button.contains(e.target)) $fb38370e8b5aa3c7$var$hide();
};
$fb38370e8b5aa3c7$var$nav.onkeydown = (e)=>{
    if (e.key === 'Escape') $fb38370e8b5aa3c7$var$hide();
    // Trap focus when nav is open.
    if (e.key === 'Tab' && $fb38370e8b5aa3c7$var$nav.classList.contains('visible')) {
        let tabbables = $fb38370e8b5aa3c7$var$nav.querySelectorAll('button, a[href]');
        let first = tabbables[0];
        let last = tabbables[tabbables.length - 1];
        if (event.shiftKey && event.target === first) {
            event.preventDefault();
            last.focus();
        } else if (!event.shiftKey && event.target === last) {
            event.preventDefault();
            first.focus();
        }
    }
};
let $fb38370e8b5aa3c7$var$mq = window.matchMedia('(max-width: 960px)');
$fb38370e8b5aa3c7$var$mq.addListener((e)=>{
    if (!e.matches) $fb38370e8b5aa3c7$var$hide();
});


