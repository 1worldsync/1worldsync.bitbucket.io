(function () {
var $54c36b42b005ed7c$var$button = document.querySelector('.hamburger-button');
var $54c36b42b005ed7c$var$nav = document.querySelector('.docs-navigation');
var $54c36b42b005ed7c$var$main = document.querySelector('main');
var $54c36b42b005ed7c$var$header = document.querySelector('.docs-header');
// Hide everything except the button element.
var $54c36b42b005ed7c$var$ariaHide = function(element, isHidden) {
    var _iteratorNormalCompletion = true, _didIteratorError = false, _iteratorError = undefined;
    try {
        for(var _iterator = element.children[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true){
            var child = _step.value;
            if (!child.contains($54c36b42b005ed7c$var$button)) {
                if (isHidden) child.setAttribute('aria-hidden', 'true');
                else child.removeAttribute('aria-hidden');
            } else if (child !== $54c36b42b005ed7c$var$button) $54c36b42b005ed7c$var$ariaHide(child, isHidden);
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally{
        try {
            if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
            }
        } finally{
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }
};
var $54c36b42b005ed7c$var$hide = function() {
    $54c36b42b005ed7c$var$nav.classList.remove('visible');
    $54c36b42b005ed7c$var$main.removeAttribute('aria-hidden');
    $54c36b42b005ed7c$var$nav.removeAttribute('tabindex');
    $54c36b42b005ed7c$var$button.setAttribute('aria-pressed', 'false');
    $54c36b42b005ed7c$var$ariaHide($54c36b42b005ed7c$var$header, false);
    if ($54c36b42b005ed7c$var$nav.contains(document.activeElement)) $54c36b42b005ed7c$var$button.focus();
};
$54c36b42b005ed7c$var$button.onclick = function() {
    $54c36b42b005ed7c$var$nav.classList.toggle('visible');
    if ($54c36b42b005ed7c$var$nav.classList.contains('visible')) {
        $54c36b42b005ed7c$var$main.setAttribute('aria-hidden', 'true');
        $54c36b42b005ed7c$var$button.setAttribute('aria-pressed', 'true');
        $54c36b42b005ed7c$var$ariaHide($54c36b42b005ed7c$var$header, true);
        $54c36b42b005ed7c$var$nav.tabIndex = -1;
        $54c36b42b005ed7c$var$nav.focus();
    } else $54c36b42b005ed7c$var$hide();
};
document.body.onclick = function(e) {
    if (!$54c36b42b005ed7c$var$nav.contains(e.target) && !$54c36b42b005ed7c$var$button.contains(e.target)) $54c36b42b005ed7c$var$hide();
};
$54c36b42b005ed7c$var$nav.onkeydown = function(e) {
    if (e.key === 'Escape') $54c36b42b005ed7c$var$hide();
    // Trap focus when nav is open.
    if (e.key === 'Tab' && $54c36b42b005ed7c$var$nav.classList.contains('visible')) {
        var tabbables = $54c36b42b005ed7c$var$nav.querySelectorAll('button, a[href]');
        var first = tabbables[0];
        var last = tabbables[tabbables.length - 1];
        if (event.shiftKey && event.target === first) {
            event.preventDefault();
            last.focus();
        } else if (!event.shiftKey && event.target === last) {
            event.preventDefault();
            first.focus();
        }
    }
};
var $54c36b42b005ed7c$var$mq = window.matchMedia('(max-width: 960px)');
$54c36b42b005ed7c$var$mq.addListener(function(e) {
    if (!e.matches) $54c36b42b005ed7c$var$hide();
});

})();
