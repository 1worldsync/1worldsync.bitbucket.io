// packages/alpinejs/src/scheduler.js
var $1df07f6efe5c7d9e$var$flushPending = false;
var $1df07f6efe5c7d9e$var$flushing = false;
var $1df07f6efe5c7d9e$var$queue = [];
function $1df07f6efe5c7d9e$var$scheduler(callback) {
    $1df07f6efe5c7d9e$var$queueJob(callback);
}
function $1df07f6efe5c7d9e$var$queueJob(job) {
    if (!$1df07f6efe5c7d9e$var$queue.includes(job)) $1df07f6efe5c7d9e$var$queue.push(job);
    $1df07f6efe5c7d9e$var$queueFlush();
}
function $1df07f6efe5c7d9e$var$dequeueJob(job) {
    let index = $1df07f6efe5c7d9e$var$queue.indexOf(job);
    if (index !== -1) $1df07f6efe5c7d9e$var$queue.splice(index, 1);
}
function $1df07f6efe5c7d9e$var$queueFlush() {
    if (!$1df07f6efe5c7d9e$var$flushing && !$1df07f6efe5c7d9e$var$flushPending) {
        $1df07f6efe5c7d9e$var$flushPending = true;
        queueMicrotask($1df07f6efe5c7d9e$var$flushJobs);
    }
}
function $1df07f6efe5c7d9e$var$flushJobs() {
    $1df07f6efe5c7d9e$var$flushPending = false;
    $1df07f6efe5c7d9e$var$flushing = true;
    for(let i = 0; i < $1df07f6efe5c7d9e$var$queue.length; i++)$1df07f6efe5c7d9e$var$queue[i]();
    $1df07f6efe5c7d9e$var$queue.length = 0;
    $1df07f6efe5c7d9e$var$flushing = false;
}
// packages/alpinejs/src/reactivity.js
var $1df07f6efe5c7d9e$var$reactive;
var $1df07f6efe5c7d9e$var$effect;
var $1df07f6efe5c7d9e$var$release;
var $1df07f6efe5c7d9e$var$raw;
var $1df07f6efe5c7d9e$var$shouldSchedule = true;
function $1df07f6efe5c7d9e$var$disableEffectScheduling(callback) {
    $1df07f6efe5c7d9e$var$shouldSchedule = false;
    callback();
    $1df07f6efe5c7d9e$var$shouldSchedule = true;
}
function $1df07f6efe5c7d9e$var$setReactivityEngine(engine) {
    $1df07f6efe5c7d9e$var$reactive = engine.reactive;
    $1df07f6efe5c7d9e$var$release = engine.release;
    $1df07f6efe5c7d9e$var$effect = (callback)=>engine.effect(callback, {
            scheduler: (task)=>{
                if ($1df07f6efe5c7d9e$var$shouldSchedule) $1df07f6efe5c7d9e$var$scheduler(task);
                else task();
            }
        })
    ;
    $1df07f6efe5c7d9e$var$raw = engine.raw;
}
function $1df07f6efe5c7d9e$var$overrideEffect(override) {
    $1df07f6efe5c7d9e$var$effect = override;
}
function $1df07f6efe5c7d9e$var$elementBoundEffect(el) {
    let cleanup2 = ()=>{};
    let wrappedEffect = (callback)=>{
        let effectReference = $1df07f6efe5c7d9e$var$effect(callback);
        if (!el._x_effects) {
            el._x_effects = new Set();
            el._x_runEffects = ()=>{
                el._x_effects.forEach((i)=>i()
                );
            };
        }
        el._x_effects.add(effectReference);
        cleanup2 = ()=>{
            if (effectReference === void 0) return;
            el._x_effects.delete(effectReference);
            $1df07f6efe5c7d9e$var$release(effectReference);
        };
        return effectReference;
    };
    return [
        wrappedEffect,
        ()=>{
            cleanup2();
        }
    ];
}
// packages/alpinejs/src/mutation.js
var $1df07f6efe5c7d9e$var$onAttributeAddeds = [];
var $1df07f6efe5c7d9e$var$onElRemoveds = [];
var $1df07f6efe5c7d9e$var$onElAddeds = [];
function $1df07f6efe5c7d9e$var$onElAdded(callback) {
    $1df07f6efe5c7d9e$var$onElAddeds.push(callback);
}
function $1df07f6efe5c7d9e$var$onElRemoved(el, callback) {
    if (typeof callback === "function") {
        if (!el._x_cleanups) el._x_cleanups = [];
        el._x_cleanups.push(callback);
    } else {
        callback = el;
        $1df07f6efe5c7d9e$var$onElRemoveds.push(callback);
    }
}
function $1df07f6efe5c7d9e$var$onAttributesAdded(callback) {
    $1df07f6efe5c7d9e$var$onAttributeAddeds.push(callback);
}
function $1df07f6efe5c7d9e$var$onAttributeRemoved(el, name, callback) {
    if (!el._x_attributeCleanups) el._x_attributeCleanups = {};
    if (!el._x_attributeCleanups[name]) el._x_attributeCleanups[name] = [];
    el._x_attributeCleanups[name].push(callback);
}
function $1df07f6efe5c7d9e$var$cleanupAttributes(el, names) {
    if (!el._x_attributeCleanups) return;
    Object.entries(el._x_attributeCleanups).forEach(([name, value])=>{
        if (names === void 0 || names.includes(name)) {
            value.forEach((i)=>i()
            );
            delete el._x_attributeCleanups[name];
        }
    });
}
var $1df07f6efe5c7d9e$var$observer = new MutationObserver($1df07f6efe5c7d9e$var$onMutate);
var $1df07f6efe5c7d9e$var$currentlyObserving = false;
function $1df07f6efe5c7d9e$var$startObservingMutations() {
    $1df07f6efe5c7d9e$var$observer.observe(document, {
        subtree: true,
        childList: true,
        attributes: true,
        attributeOldValue: true
    });
    $1df07f6efe5c7d9e$var$currentlyObserving = true;
}
function $1df07f6efe5c7d9e$var$stopObservingMutations() {
    $1df07f6efe5c7d9e$var$flushObserver();
    $1df07f6efe5c7d9e$var$observer.disconnect();
    $1df07f6efe5c7d9e$var$currentlyObserving = false;
}
var $1df07f6efe5c7d9e$var$recordQueue = [];
var $1df07f6efe5c7d9e$var$willProcessRecordQueue = false;
function $1df07f6efe5c7d9e$var$flushObserver() {
    $1df07f6efe5c7d9e$var$recordQueue = $1df07f6efe5c7d9e$var$recordQueue.concat($1df07f6efe5c7d9e$var$observer.takeRecords());
    if ($1df07f6efe5c7d9e$var$recordQueue.length && !$1df07f6efe5c7d9e$var$willProcessRecordQueue) {
        $1df07f6efe5c7d9e$var$willProcessRecordQueue = true;
        queueMicrotask(()=>{
            $1df07f6efe5c7d9e$var$processRecordQueue();
            $1df07f6efe5c7d9e$var$willProcessRecordQueue = false;
        });
    }
}
function $1df07f6efe5c7d9e$var$processRecordQueue() {
    $1df07f6efe5c7d9e$var$onMutate($1df07f6efe5c7d9e$var$recordQueue);
    $1df07f6efe5c7d9e$var$recordQueue.length = 0;
}
function $1df07f6efe5c7d9e$var$mutateDom(callback) {
    if (!$1df07f6efe5c7d9e$var$currentlyObserving) return callback();
    $1df07f6efe5c7d9e$var$stopObservingMutations();
    let result = callback();
    $1df07f6efe5c7d9e$var$startObservingMutations();
    return result;
}
var $1df07f6efe5c7d9e$var$isCollecting = false;
var $1df07f6efe5c7d9e$var$deferredMutations = [];
function $1df07f6efe5c7d9e$var$deferMutations() {
    $1df07f6efe5c7d9e$var$isCollecting = true;
}
function $1df07f6efe5c7d9e$var$flushAndStopDeferringMutations() {
    $1df07f6efe5c7d9e$var$isCollecting = false;
    $1df07f6efe5c7d9e$var$onMutate($1df07f6efe5c7d9e$var$deferredMutations);
    $1df07f6efe5c7d9e$var$deferredMutations = [];
}
function $1df07f6efe5c7d9e$var$onMutate(mutations) {
    if ($1df07f6efe5c7d9e$var$isCollecting) {
        $1df07f6efe5c7d9e$var$deferredMutations = $1df07f6efe5c7d9e$var$deferredMutations.concat(mutations);
        return;
    }
    let addedNodes = [];
    let removedNodes = [];
    let addedAttributes = new Map();
    let removedAttributes = new Map();
    for(let i1 = 0; i1 < mutations.length; i1++){
        if (mutations[i1].target._x_ignoreMutationObserver) continue;
        if (mutations[i1].type === "childList") {
            mutations[i1].addedNodes.forEach((node)=>node.nodeType === 1 && addedNodes.push(node)
            );
            mutations[i1].removedNodes.forEach((node)=>node.nodeType === 1 && removedNodes.push(node)
            );
        }
        if (mutations[i1].type === "attributes") {
            let el = mutations[i1].target;
            let name = mutations[i1].attributeName;
            let oldValue = mutations[i1].oldValue;
            let add2 = ()=>{
                if (!addedAttributes.has(el)) addedAttributes.set(el, []);
                addedAttributes.get(el).push({
                    name: name,
                    value: el.getAttribute(name)
                });
            };
            let remove = ()=>{
                if (!removedAttributes.has(el)) removedAttributes.set(el, []);
                removedAttributes.get(el).push(name);
            };
            if (el.hasAttribute(name) && oldValue === null) add2();
            else if (el.hasAttribute(name)) {
                remove();
                add2();
            } else remove();
        }
    }
    removedAttributes.forEach((attrs, el)=>{
        $1df07f6efe5c7d9e$var$cleanupAttributes(el, attrs);
    });
    addedAttributes.forEach((attrs, el)=>{
        $1df07f6efe5c7d9e$var$onAttributeAddeds.forEach((i)=>i(el, attrs)
        );
    });
    for (let node2 of removedNodes){
        if (addedNodes.includes(node2)) continue;
        $1df07f6efe5c7d9e$var$onElRemoveds.forEach((i)=>i(node2)
        );
        if (node2._x_cleanups) while(node2._x_cleanups.length)node2._x_cleanups.pop()();
    }
    addedNodes.forEach((node)=>{
        node._x_ignoreSelf = true;
        node._x_ignore = true;
    });
    for (let node1 of addedNodes){
        if (removedNodes.includes(node1)) continue;
        if (!node1.isConnected) continue;
        delete node1._x_ignoreSelf;
        delete node1._x_ignore;
        $1df07f6efe5c7d9e$var$onElAddeds.forEach((i)=>i(node1)
        );
        node1._x_ignore = true;
        node1._x_ignoreSelf = true;
    }
    addedNodes.forEach((node)=>{
        delete node._x_ignoreSelf;
        delete node._x_ignore;
    });
    addedNodes = null;
    removedNodes = null;
    addedAttributes = null;
    removedAttributes = null;
}
// packages/alpinejs/src/scope.js
function $1df07f6efe5c7d9e$var$scope(node) {
    return $1df07f6efe5c7d9e$var$mergeProxies($1df07f6efe5c7d9e$var$closestDataStack(node));
}
function $1df07f6efe5c7d9e$var$addScopeToNode(node, data2, referenceNode) {
    node._x_dataStack = [
        data2,
        ...$1df07f6efe5c7d9e$var$closestDataStack(referenceNode || node)
    ];
    return ()=>{
        node._x_dataStack = node._x_dataStack.filter((i)=>i !== data2
        );
    };
}
function $1df07f6efe5c7d9e$var$refreshScope(element, scope2) {
    let existingScope = element._x_dataStack[0];
    Object.entries(scope2).forEach(([key, value])=>{
        existingScope[key] = value;
    });
}
function $1df07f6efe5c7d9e$var$closestDataStack(node) {
    if (node._x_dataStack) return node._x_dataStack;
    if (typeof ShadowRoot === "function" && node instanceof ShadowRoot) return $1df07f6efe5c7d9e$var$closestDataStack(node.host);
    if (!node.parentNode) return [];
    return $1df07f6efe5c7d9e$var$closestDataStack(node.parentNode);
}
function $1df07f6efe5c7d9e$var$mergeProxies(objects) {
    let thisProxy = new Proxy({}, {
        ownKeys: ()=>{
            return Array.from(new Set(objects.flatMap((i)=>Object.keys(i)
            )));
        },
        has: (target, name)=>{
            return objects.some((obj)=>obj.hasOwnProperty(name)
            );
        },
        get: (target, name)=>{
            return (objects.find((obj)=>{
                if (obj.hasOwnProperty(name)) {
                    let descriptor = Object.getOwnPropertyDescriptor(obj, name);
                    if (descriptor.get && descriptor.get._x_alreadyBound || descriptor.set && descriptor.set._x_alreadyBound) return true;
                    if ((descriptor.get || descriptor.set) && descriptor.enumerable) {
                        let getter = descriptor.get;
                        let setter = descriptor.set;
                        let property = descriptor;
                        getter = getter && getter.bind(thisProxy);
                        setter = setter && setter.bind(thisProxy);
                        if (getter) getter._x_alreadyBound = true;
                        if (setter) setter._x_alreadyBound = true;
                        Object.defineProperty(obj, name, {
                            ...property,
                            get: getter,
                            set: setter
                        });
                    }
                    return true;
                }
                return false;
            }) || {})[name];
        },
        set: (target, name, value)=>{
            let closestObjectWithKey = objects.find((obj)=>obj.hasOwnProperty(name)
            );
            if (closestObjectWithKey) closestObjectWithKey[name] = value;
            else objects[objects.length - 1][name] = value;
            return true;
        }
    });
    return thisProxy;
}
// packages/alpinejs/src/interceptor.js
function $1df07f6efe5c7d9e$var$initInterceptors(data2) {
    let isObject2 = (val)=>typeof val === "object" && !Array.isArray(val) && val !== null
    ;
    let recurse = (obj, basePath = "")=>{
        Object.entries(Object.getOwnPropertyDescriptors(obj)).forEach(([key, { value: value , enumerable: enumerable  }])=>{
            if (enumerable === false || value === void 0) return;
            let path = basePath === "" ? key : `${basePath}.${key}`;
            if (typeof value === "object" && value !== null && value._x_interceptor) obj[key] = value.initialize(data2, path, key);
            else if (isObject2(value) && value !== obj && !(value instanceof Element)) recurse(value, path);
        });
    };
    return recurse(data2);
}
function $1df07f6efe5c7d9e$var$interceptor(callback, mutateObj = ()=>{}) {
    let obj = {
        initialValue: void 0,
        _x_interceptor: true,
        initialize (data2, path, key) {
            return callback(this.initialValue, ()=>$1df07f6efe5c7d9e$var$get(data2, path)
            , (value)=>$1df07f6efe5c7d9e$var$set(data2, path, value)
            , path, key);
        }
    };
    mutateObj(obj);
    return (initialValue)=>{
        if (typeof initialValue === "object" && initialValue !== null && initialValue._x_interceptor) {
            let initialize = obj.initialize.bind(obj);
            obj.initialize = (data2, path, key)=>{
                let innerValue = initialValue.initialize(data2, path, key);
                obj.initialValue = innerValue;
                return initialize(data2, path, key);
            };
        } else obj.initialValue = initialValue;
        return obj;
    };
}
function $1df07f6efe5c7d9e$var$get(obj, path) {
    return path.split(".").reduce((carry, segment)=>carry[segment]
    , obj);
}
function $1df07f6efe5c7d9e$var$set(obj, path, value) {
    if (typeof path === "string") path = path.split(".");
    if (path.length === 1) obj[path[0]] = value;
    else if (path.length === 0) throw error;
    else {
        if (obj[path[0]]) return $1df07f6efe5c7d9e$var$set(obj[path[0]], path.slice(1), value);
        else {
            obj[path[0]] = {};
            return $1df07f6efe5c7d9e$var$set(obj[path[0]], path.slice(1), value);
        }
    }
}
// packages/alpinejs/src/magics.js
var $1df07f6efe5c7d9e$var$magics = {};
function $1df07f6efe5c7d9e$var$magic(name, callback) {
    $1df07f6efe5c7d9e$var$magics[name] = callback;
}
function $1df07f6efe5c7d9e$var$injectMagics(obj, el) {
    Object.entries($1df07f6efe5c7d9e$var$magics).forEach(([name, callback])=>{
        Object.defineProperty(obj, `$${name}`, {
            get () {
                let [utilities, cleanup2] = $1df07f6efe5c7d9e$var$getElementBoundUtilities(el);
                utilities = {
                    interceptor: $1df07f6efe5c7d9e$var$interceptor,
                    ...utilities
                };
                $1df07f6efe5c7d9e$var$onElRemoved(el, cleanup2);
                return callback(el, utilities);
            },
            enumerable: false
        });
    });
    return obj;
}
// packages/alpinejs/src/utils/error.js
function $1df07f6efe5c7d9e$var$tryCatch(el, expression, callback, ...args) {
    try {
        return callback(...args);
    } catch (e) {
        $1df07f6efe5c7d9e$var$handleError(e, el, expression);
    }
}
function $1df07f6efe5c7d9e$var$handleError(error2, el, expression) {
    Object.assign(error2, {
        el: el,
        expression: expression
    });
    console.warn(`Alpine Expression Error: ${error2.message}

${expression ? 'Expression: "' + expression + '"\n\n' : ""}`, el);
    setTimeout(()=>{
        throw error2;
    }, 0);
}
// packages/alpinejs/src/evaluator.js
function $1df07f6efe5c7d9e$var$evaluate(el, expression, extras = {}) {
    let result;
    $1df07f6efe5c7d9e$var$evaluateLater(el, expression)((value)=>result = value
    , extras);
    return result;
}
function $1df07f6efe5c7d9e$var$evaluateLater(...args) {
    return $1df07f6efe5c7d9e$var$theEvaluatorFunction(...args);
}
var $1df07f6efe5c7d9e$var$theEvaluatorFunction = $1df07f6efe5c7d9e$var$normalEvaluator;
function $1df07f6efe5c7d9e$var$setEvaluator(newEvaluator) {
    $1df07f6efe5c7d9e$var$theEvaluatorFunction = newEvaluator;
}
function $1df07f6efe5c7d9e$var$normalEvaluator(el, expression) {
    let overriddenMagics = {};
    $1df07f6efe5c7d9e$var$injectMagics(overriddenMagics, el);
    let dataStack = [
        overriddenMagics,
        ...$1df07f6efe5c7d9e$var$closestDataStack(el)
    ];
    if (typeof expression === "function") return $1df07f6efe5c7d9e$var$generateEvaluatorFromFunction(dataStack, expression);
    let evaluator = $1df07f6efe5c7d9e$var$generateEvaluatorFromString(dataStack, expression, el);
    return $1df07f6efe5c7d9e$var$tryCatch.bind(null, el, expression, evaluator);
}
function $1df07f6efe5c7d9e$var$generateEvaluatorFromFunction(dataStack, func) {
    return (receiver = ()=>{}, { scope: scope2 = {} , params: params = []  } = {})=>{
        let result = func.apply($1df07f6efe5c7d9e$var$mergeProxies([
            scope2,
            ...dataStack
        ]), params);
        $1df07f6efe5c7d9e$var$runIfTypeOfFunction(receiver, result);
    };
}
var $1df07f6efe5c7d9e$var$evaluatorMemo = {};
function $1df07f6efe5c7d9e$var$generateFunctionFromString(expression, el) {
    if ($1df07f6efe5c7d9e$var$evaluatorMemo[expression]) return $1df07f6efe5c7d9e$var$evaluatorMemo[expression];
    let AsyncFunction = Object.getPrototypeOf(async function() {}).constructor;
    let rightSideSafeExpression = /^[\n\s]*if.*\(.*\)/.test(expression) || /^(let|const)\s/.test(expression) ? `(() => { ${expression} })()` : expression;
    const safeAsyncFunction = ()=>{
        try {
            return new AsyncFunction([
                "__self",
                "scope"
            ], `with (scope) { __self.result = ${rightSideSafeExpression} }; __self.finished = true; return __self.result;`);
        } catch (error2) {
            $1df07f6efe5c7d9e$var$handleError(error2, el, expression);
            return Promise.resolve();
        }
    };
    let func = safeAsyncFunction();
    $1df07f6efe5c7d9e$var$evaluatorMemo[expression] = func;
    return func;
}
function $1df07f6efe5c7d9e$var$generateEvaluatorFromString(dataStack, expression, el) {
    let func = $1df07f6efe5c7d9e$var$generateFunctionFromString(expression, el);
    return (receiver = ()=>{}, { scope: scope2 = {} , params: params = []  } = {})=>{
        func.result = void 0;
        func.finished = false;
        let completeScope = $1df07f6efe5c7d9e$var$mergeProxies([
            scope2,
            ...dataStack
        ]);
        if (typeof func === "function") {
            let promise = func(func, completeScope).catch((error2)=>$1df07f6efe5c7d9e$var$handleError(error2, el, expression)
            );
            if (func.finished) {
                $1df07f6efe5c7d9e$var$runIfTypeOfFunction(receiver, func.result, completeScope, params, el);
                func.result = void 0;
            } else promise.then((result)=>{
                $1df07f6efe5c7d9e$var$runIfTypeOfFunction(receiver, result, completeScope, params, el);
            }).catch((error2)=>$1df07f6efe5c7d9e$var$handleError(error2, el, expression)
            ).finally(()=>func.result = void 0
            );
        }
    };
}
function $1df07f6efe5c7d9e$var$runIfTypeOfFunction(receiver, value, scope2, params, el) {
    if (typeof value === "function") {
        let result = value.apply(scope2, params);
        if (result instanceof Promise) result.then((i)=>$1df07f6efe5c7d9e$var$runIfTypeOfFunction(receiver, i, scope2, params)
        ).catch((error2)=>$1df07f6efe5c7d9e$var$handleError(error2, el, value)
        );
        else receiver(result);
    } else receiver(value);
}
// packages/alpinejs/src/directives.js
var $1df07f6efe5c7d9e$var$prefixAsString = "x-";
function $1df07f6efe5c7d9e$var$prefix(subject = "") {
    return $1df07f6efe5c7d9e$var$prefixAsString + subject;
}
function $1df07f6efe5c7d9e$var$setPrefix(newPrefix) {
    $1df07f6efe5c7d9e$var$prefixAsString = newPrefix;
}
var $1df07f6efe5c7d9e$var$directiveHandlers = {};
function $1df07f6efe5c7d9e$var$directive(name, callback) {
    $1df07f6efe5c7d9e$var$directiveHandlers[name] = callback;
}
function $1df07f6efe5c7d9e$var$directives(el, attributes, originalAttributeOverride) {
    let transformedAttributeMap = {};
    let directives2 = Array.from(attributes).map($1df07f6efe5c7d9e$var$toTransformedAttributes((newName, oldName)=>transformedAttributeMap[newName] = oldName
    )).filter($1df07f6efe5c7d9e$var$outNonAlpineAttributes).map($1df07f6efe5c7d9e$var$toParsedDirectives(transformedAttributeMap, originalAttributeOverride)).sort($1df07f6efe5c7d9e$var$byPriority);
    return directives2.map((directive2)=>{
        return $1df07f6efe5c7d9e$var$getDirectiveHandler(el, directive2);
    });
}
function $1df07f6efe5c7d9e$var$attributesOnly(attributes) {
    return Array.from(attributes).map($1df07f6efe5c7d9e$var$toTransformedAttributes()).filter((attr)=>!$1df07f6efe5c7d9e$var$outNonAlpineAttributes(attr)
    );
}
var $1df07f6efe5c7d9e$var$isDeferringHandlers = false;
var $1df07f6efe5c7d9e$var$directiveHandlerStacks = new Map();
var $1df07f6efe5c7d9e$var$currentHandlerStackKey = Symbol();
function $1df07f6efe5c7d9e$var$deferHandlingDirectives(callback) {
    $1df07f6efe5c7d9e$var$isDeferringHandlers = true;
    let key = Symbol();
    $1df07f6efe5c7d9e$var$currentHandlerStackKey = key;
    $1df07f6efe5c7d9e$var$directiveHandlerStacks.set(key, []);
    let flushHandlers = ()=>{
        while($1df07f6efe5c7d9e$var$directiveHandlerStacks.get(key).length)$1df07f6efe5c7d9e$var$directiveHandlerStacks.get(key).shift()();
        $1df07f6efe5c7d9e$var$directiveHandlerStacks.delete(key);
    };
    let stopDeferring = ()=>{
        $1df07f6efe5c7d9e$var$isDeferringHandlers = false;
        flushHandlers();
    };
    callback(flushHandlers);
    stopDeferring();
}
function $1df07f6efe5c7d9e$var$getElementBoundUtilities(el) {
    let cleanups = [];
    let cleanup2 = (callback)=>cleanups.push(callback)
    ;
    let [effect3, cleanupEffect] = $1df07f6efe5c7d9e$var$elementBoundEffect(el);
    cleanups.push(cleanupEffect);
    let utilities = {
        Alpine: $1df07f6efe5c7d9e$var$alpine_default,
        effect: effect3,
        cleanup: cleanup2,
        evaluateLater: $1df07f6efe5c7d9e$var$evaluateLater.bind($1df07f6efe5c7d9e$var$evaluateLater, el),
        evaluate: $1df07f6efe5c7d9e$var$evaluate.bind($1df07f6efe5c7d9e$var$evaluate, el)
    };
    let doCleanup = ()=>cleanups.forEach((i)=>i()
        )
    ;
    return [
        utilities,
        doCleanup
    ];
}
function $1df07f6efe5c7d9e$var$getDirectiveHandler(el, directive2) {
    let noop = ()=>{};
    let handler3 = $1df07f6efe5c7d9e$var$directiveHandlers[directive2.type] || noop;
    let [utilities, cleanup2] = $1df07f6efe5c7d9e$var$getElementBoundUtilities(el);
    $1df07f6efe5c7d9e$var$onAttributeRemoved(el, directive2.original, cleanup2);
    let fullHandler = ()=>{
        if (el._x_ignore || el._x_ignoreSelf) return;
        handler3.inline && handler3.inline(el, directive2, utilities);
        handler3 = handler3.bind(handler3, el, directive2, utilities);
        $1df07f6efe5c7d9e$var$isDeferringHandlers ? $1df07f6efe5c7d9e$var$directiveHandlerStacks.get($1df07f6efe5c7d9e$var$currentHandlerStackKey).push(handler3) : handler3();
    };
    fullHandler.runCleanups = cleanup2;
    return fullHandler;
}
var $1df07f6efe5c7d9e$var$startingWith = (subject, replacement)=>({ name: name , value: value  })=>{
        if (name.startsWith(subject)) name = name.replace(subject, replacement);
        return {
            name: name,
            value: value
        };
    }
;
var $1df07f6efe5c7d9e$var$into = (i)=>i
;
function $1df07f6efe5c7d9e$var$toTransformedAttributes(callback = ()=>{}) {
    return ({ name: name , value: value  })=>{
        let { name: newName , value: newValue  } = $1df07f6efe5c7d9e$var$attributeTransformers.reduce((carry, transform)=>{
            return transform(carry);
        }, {
            name: name,
            value: value
        });
        if (newName !== name) callback(newName, name);
        return {
            name: newName,
            value: newValue
        };
    };
}
var $1df07f6efe5c7d9e$var$attributeTransformers = [];
function $1df07f6efe5c7d9e$var$mapAttributes(callback) {
    $1df07f6efe5c7d9e$var$attributeTransformers.push(callback);
}
function $1df07f6efe5c7d9e$var$outNonAlpineAttributes({ name: name  }) {
    return $1df07f6efe5c7d9e$var$alpineAttributeRegex().test(name);
}
var $1df07f6efe5c7d9e$var$alpineAttributeRegex = ()=>new RegExp(`^${$1df07f6efe5c7d9e$var$prefixAsString}([^:^.]+)\\b`)
;
function $1df07f6efe5c7d9e$var$toParsedDirectives(transformedAttributeMap, originalAttributeOverride) {
    return ({ name: name , value: value  })=>{
        let typeMatch = name.match($1df07f6efe5c7d9e$var$alpineAttributeRegex());
        let valueMatch = name.match(/:([a-zA-Z0-9\-:]+)/);
        let modifiers = name.match(/\.[^.\]]+(?=[^\]]*$)/g) || [];
        let original = originalAttributeOverride || transformedAttributeMap[name] || name;
        return {
            type: typeMatch ? typeMatch[1] : null,
            value: valueMatch ? valueMatch[1] : null,
            modifiers: modifiers.map((i)=>i.replace(".", "")
            ),
            expression: value,
            original: original
        };
    };
}
var $1df07f6efe5c7d9e$var$DEFAULT = "DEFAULT";
var $1df07f6efe5c7d9e$var$directiveOrder = [
    "ignore",
    "ref",
    "data",
    "id",
    "bind",
    "init",
    "for",
    "model",
    "modelable",
    "transition",
    "show",
    "if",
    $1df07f6efe5c7d9e$var$DEFAULT,
    "teleport",
    "element"
];
function $1df07f6efe5c7d9e$var$byPriority(a, b) {
    let typeA = $1df07f6efe5c7d9e$var$directiveOrder.indexOf(a.type) === -1 ? $1df07f6efe5c7d9e$var$DEFAULT : a.type;
    let typeB = $1df07f6efe5c7d9e$var$directiveOrder.indexOf(b.type) === -1 ? $1df07f6efe5c7d9e$var$DEFAULT : b.type;
    return $1df07f6efe5c7d9e$var$directiveOrder.indexOf(typeA) - $1df07f6efe5c7d9e$var$directiveOrder.indexOf(typeB);
}
// packages/alpinejs/src/utils/dispatch.js
function $1df07f6efe5c7d9e$var$dispatch(el, name, detail = {}) {
    el.dispatchEvent(new CustomEvent(name, {
        detail: detail,
        bubbles: true,
        composed: true,
        cancelable: true
    }));
}
// packages/alpinejs/src/nextTick.js
var $1df07f6efe5c7d9e$var$tickStack = [];
var $1df07f6efe5c7d9e$var$isHolding = false;
function $1df07f6efe5c7d9e$var$nextTick(callback) {
    $1df07f6efe5c7d9e$var$tickStack.push(callback);
    queueMicrotask(()=>{
        $1df07f6efe5c7d9e$var$isHolding || setTimeout(()=>{
            $1df07f6efe5c7d9e$var$releaseNextTicks();
        });
    });
}
function $1df07f6efe5c7d9e$var$releaseNextTicks() {
    $1df07f6efe5c7d9e$var$isHolding = false;
    while($1df07f6efe5c7d9e$var$tickStack.length)$1df07f6efe5c7d9e$var$tickStack.shift()();
}
function $1df07f6efe5c7d9e$var$holdNextTicks() {
    $1df07f6efe5c7d9e$var$isHolding = true;
}
// packages/alpinejs/src/utils/walk.js
function $1df07f6efe5c7d9e$var$walk(el, callback) {
    if (typeof ShadowRoot === "function" && el instanceof ShadowRoot) {
        Array.from(el.children).forEach((el2)=>$1df07f6efe5c7d9e$var$walk(el2, callback)
        );
        return;
    }
    let skip = false;
    callback(el, ()=>skip = true
    );
    if (skip) return;
    let node = el.firstElementChild;
    while(node){
        $1df07f6efe5c7d9e$var$walk(node, callback, false);
        node = node.nextElementSibling;
    }
}
// packages/alpinejs/src/utils/warn.js
function $1df07f6efe5c7d9e$var$warn(message, ...args) {
    console.warn(`Alpine Warning: ${message}`, ...args);
}
// packages/alpinejs/src/lifecycle.js
function $1df07f6efe5c7d9e$var$start() {
    if (!document.body) $1df07f6efe5c7d9e$var$warn("Unable to initialize. Trying to load Alpine before `<body>` is available. Did you forget to add `defer` in Alpine's `<script>` tag?");
    $1df07f6efe5c7d9e$var$dispatch(document, "alpine:init");
    $1df07f6efe5c7d9e$var$dispatch(document, "alpine:initializing");
    $1df07f6efe5c7d9e$var$startObservingMutations();
    $1df07f6efe5c7d9e$var$onElAdded((el)=>$1df07f6efe5c7d9e$var$initTree(el, $1df07f6efe5c7d9e$var$walk)
    );
    $1df07f6efe5c7d9e$var$onElRemoved((el)=>$1df07f6efe5c7d9e$var$destroyTree(el)
    );
    $1df07f6efe5c7d9e$var$onAttributesAdded((el, attrs)=>{
        $1df07f6efe5c7d9e$var$directives(el, attrs).forEach((handle)=>handle()
        );
    });
    let outNestedComponents = (el)=>!$1df07f6efe5c7d9e$var$closestRoot(el.parentElement, true)
    ;
    Array.from(document.querySelectorAll($1df07f6efe5c7d9e$var$allSelectors())).filter(outNestedComponents).forEach((el)=>{
        $1df07f6efe5c7d9e$var$initTree(el);
    });
    $1df07f6efe5c7d9e$var$dispatch(document, "alpine:initialized");
}
var $1df07f6efe5c7d9e$var$rootSelectorCallbacks = [];
var $1df07f6efe5c7d9e$var$initSelectorCallbacks = [];
function $1df07f6efe5c7d9e$var$rootSelectors() {
    return $1df07f6efe5c7d9e$var$rootSelectorCallbacks.map((fn)=>fn()
    );
}
function $1df07f6efe5c7d9e$var$allSelectors() {
    return $1df07f6efe5c7d9e$var$rootSelectorCallbacks.concat($1df07f6efe5c7d9e$var$initSelectorCallbacks).map((fn)=>fn()
    );
}
function $1df07f6efe5c7d9e$var$addRootSelector(selectorCallback) {
    $1df07f6efe5c7d9e$var$rootSelectorCallbacks.push(selectorCallback);
}
function $1df07f6efe5c7d9e$var$addInitSelector(selectorCallback) {
    $1df07f6efe5c7d9e$var$initSelectorCallbacks.push(selectorCallback);
}
function $1df07f6efe5c7d9e$var$closestRoot(el, includeInitSelectors = false) {
    return $1df07f6efe5c7d9e$var$findClosest(el, (element)=>{
        const selectors = includeInitSelectors ? $1df07f6efe5c7d9e$var$allSelectors() : $1df07f6efe5c7d9e$var$rootSelectors();
        if (selectors.some((selector)=>element.matches(selector)
        )) return true;
    });
}
function $1df07f6efe5c7d9e$var$findClosest(el, callback) {
    if (!el) return;
    if (callback(el)) return el;
    if (el._x_teleportBack) el = el._x_teleportBack;
    if (!el.parentElement) return;
    return $1df07f6efe5c7d9e$var$findClosest(el.parentElement, callback);
}
function $1df07f6efe5c7d9e$var$isRoot(el) {
    return $1df07f6efe5c7d9e$var$rootSelectors().some((selector)=>el.matches(selector)
    );
}
function $1df07f6efe5c7d9e$var$initTree(el, walker = $1df07f6efe5c7d9e$var$walk) {
    $1df07f6efe5c7d9e$var$deferHandlingDirectives(()=>{
        walker(el, (el2, skip)=>{
            $1df07f6efe5c7d9e$var$directives(el2, el2.attributes).forEach((handle)=>handle()
            );
            el2._x_ignore && skip();
        });
    });
}
function $1df07f6efe5c7d9e$var$destroyTree(root) {
    $1df07f6efe5c7d9e$var$walk(root, (el)=>$1df07f6efe5c7d9e$var$cleanupAttributes(el)
    );
}
// packages/alpinejs/src/utils/classes.js
function $1df07f6efe5c7d9e$var$setClasses(el, value) {
    if (Array.isArray(value)) return $1df07f6efe5c7d9e$var$setClassesFromString(el, value.join(" "));
    else if (typeof value === "object" && value !== null) return $1df07f6efe5c7d9e$var$setClassesFromObject(el, value);
    else if (typeof value === "function") return $1df07f6efe5c7d9e$var$setClasses(el, value());
    return $1df07f6efe5c7d9e$var$setClassesFromString(el, value);
}
function $1df07f6efe5c7d9e$var$setClassesFromString(el, classString) {
    let split = (classString2)=>classString2.split(" ").filter(Boolean)
    ;
    let missingClasses = (classString2)=>classString2.split(" ").filter((i)=>!el.classList.contains(i)
        ).filter(Boolean)
    ;
    let addClassesAndReturnUndo = (classes)=>{
        el.classList.add(...classes);
        return ()=>{
            el.classList.remove(...classes);
        };
    };
    classString = classString === true ? classString = "" : classString || "";
    return addClassesAndReturnUndo(missingClasses(classString));
}
function $1df07f6efe5c7d9e$var$setClassesFromObject(el, classObject) {
    let split = (classString)=>classString.split(" ").filter(Boolean)
    ;
    let forAdd = Object.entries(classObject).flatMap(([classString, bool])=>bool ? split(classString) : false
    ).filter(Boolean);
    let forRemove = Object.entries(classObject).flatMap(([classString, bool])=>!bool ? split(classString) : false
    ).filter(Boolean);
    let added = [];
    let removed = [];
    forRemove.forEach((i)=>{
        if (el.classList.contains(i)) {
            el.classList.remove(i);
            removed.push(i);
        }
    });
    forAdd.forEach((i)=>{
        if (!el.classList.contains(i)) {
            el.classList.add(i);
            added.push(i);
        }
    });
    return ()=>{
        removed.forEach((i)=>el.classList.add(i)
        );
        added.forEach((i)=>el.classList.remove(i)
        );
    };
}
// packages/alpinejs/src/utils/styles.js
function $1df07f6efe5c7d9e$var$setStyles(el, value) {
    if (typeof value === "object" && value !== null) return $1df07f6efe5c7d9e$var$setStylesFromObject(el, value);
    return $1df07f6efe5c7d9e$var$setStylesFromString(el, value);
}
function $1df07f6efe5c7d9e$var$setStylesFromObject(el, value) {
    let previousStyles = {};
    Object.entries(value).forEach(([key, value2])=>{
        previousStyles[key] = el.style[key];
        if (!key.startsWith("--")) key = $1df07f6efe5c7d9e$var$kebabCase(key);
        el.style.setProperty(key, value2);
    });
    setTimeout(()=>{
        if (el.style.length === 0) el.removeAttribute("style");
    });
    return ()=>{
        $1df07f6efe5c7d9e$var$setStyles(el, previousStyles);
    };
}
function $1df07f6efe5c7d9e$var$setStylesFromString(el, value) {
    let cache = el.getAttribute("style", value);
    el.setAttribute("style", value);
    return ()=>{
        el.setAttribute("style", cache || "");
    };
}
function $1df07f6efe5c7d9e$var$kebabCase(subject) {
    return subject.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase();
}
// packages/alpinejs/src/utils/once.js
function $1df07f6efe5c7d9e$var$once(callback, fallback = ()=>{}) {
    let called = false;
    return function() {
        if (!called) {
            called = true;
            callback.apply(this, arguments);
        } else fallback.apply(this, arguments);
    };
}
// packages/alpinejs/src/directives/x-transition.js
$1df07f6efe5c7d9e$var$directive("transition", (el, { value: value , modifiers: modifiers , expression: expression  }, { evaluate: evaluate2  })=>{
    if (typeof expression === "function") expression = evaluate2(expression);
    if (!expression) $1df07f6efe5c7d9e$var$registerTransitionsFromHelper(el, modifiers, value);
    else $1df07f6efe5c7d9e$var$registerTransitionsFromClassString(el, expression, value);
});
function $1df07f6efe5c7d9e$var$registerTransitionsFromClassString(el, classString, stage) {
    $1df07f6efe5c7d9e$var$registerTransitionObject(el, $1df07f6efe5c7d9e$var$setClasses, "");
    let directiveStorageMap = {
        enter: (classes)=>{
            el._x_transition.enter.during = classes;
        },
        "enter-start": (classes)=>{
            el._x_transition.enter.start = classes;
        },
        "enter-end": (classes)=>{
            el._x_transition.enter.end = classes;
        },
        leave: (classes)=>{
            el._x_transition.leave.during = classes;
        },
        "leave-start": (classes)=>{
            el._x_transition.leave.start = classes;
        },
        "leave-end": (classes)=>{
            el._x_transition.leave.end = classes;
        }
    };
    directiveStorageMap[stage](classString);
}
function $1df07f6efe5c7d9e$var$registerTransitionsFromHelper(el, modifiers, stage) {
    $1df07f6efe5c7d9e$var$registerTransitionObject(el, $1df07f6efe5c7d9e$var$setStyles);
    let doesntSpecify = !modifiers.includes("in") && !modifiers.includes("out") && !stage;
    let transitioningIn = doesntSpecify || modifiers.includes("in") || [
        "enter"
    ].includes(stage);
    let transitioningOut = doesntSpecify || modifiers.includes("out") || [
        "leave"
    ].includes(stage);
    if (modifiers.includes("in") && !doesntSpecify) modifiers = modifiers.filter((i, index)=>index < modifiers.indexOf("out")
    );
    if (modifiers.includes("out") && !doesntSpecify) modifiers = modifiers.filter((i, index)=>index > modifiers.indexOf("out")
    );
    let wantsAll = !modifiers.includes("opacity") && !modifiers.includes("scale");
    let wantsOpacity = wantsAll || modifiers.includes("opacity");
    let wantsScale = wantsAll || modifiers.includes("scale");
    let opacityValue = wantsOpacity ? 0 : 1;
    let scaleValue = wantsScale ? $1df07f6efe5c7d9e$var$modifierValue(modifiers, "scale", 95) / 100 : 1;
    let delay = $1df07f6efe5c7d9e$var$modifierValue(modifiers, "delay", 0);
    let origin = $1df07f6efe5c7d9e$var$modifierValue(modifiers, "origin", "center");
    let property = "opacity, transform";
    let durationIn = $1df07f6efe5c7d9e$var$modifierValue(modifiers, "duration", 150) / 1000;
    let durationOut = $1df07f6efe5c7d9e$var$modifierValue(modifiers, "duration", 75) / 1000;
    let easing = `cubic-bezier(0.4, 0.0, 0.2, 1)`;
    if (transitioningIn) {
        el._x_transition.enter.during = {
            transformOrigin: origin,
            transitionDelay: delay,
            transitionProperty: property,
            transitionDuration: `${durationIn}s`,
            transitionTimingFunction: easing
        };
        el._x_transition.enter.start = {
            opacity: opacityValue,
            transform: `scale(${scaleValue})`
        };
        el._x_transition.enter.end = {
            opacity: 1,
            transform: `scale(1)`
        };
    }
    if (transitioningOut) {
        el._x_transition.leave.during = {
            transformOrigin: origin,
            transitionDelay: delay,
            transitionProperty: property,
            transitionDuration: `${durationOut}s`,
            transitionTimingFunction: easing
        };
        el._x_transition.leave.start = {
            opacity: 1,
            transform: `scale(1)`
        };
        el._x_transition.leave.end = {
            opacity: opacityValue,
            transform: `scale(${scaleValue})`
        };
    }
}
function $1df07f6efe5c7d9e$var$registerTransitionObject(el, setFunction, defaultValue = {}) {
    if (!el._x_transition) el._x_transition = {
        enter: {
            during: defaultValue,
            start: defaultValue,
            end: defaultValue
        },
        leave: {
            during: defaultValue,
            start: defaultValue,
            end: defaultValue
        },
        in (before = ()=>{}, after = ()=>{}) {
            $1df07f6efe5c7d9e$var$transition(el, setFunction, {
                during: this.enter.during,
                start: this.enter.start,
                end: this.enter.end
            }, before, after);
        },
        out (before = ()=>{}, after = ()=>{}) {
            $1df07f6efe5c7d9e$var$transition(el, setFunction, {
                during: this.leave.during,
                start: this.leave.start,
                end: this.leave.end
            }, before, after);
        }
    };
}
window.Element.prototype._x_toggleAndCascadeWithTransitions = function(el, value, show, hide) {
    let clickAwayCompatibleShow = ()=>{
        document.visibilityState === "visible" ? requestAnimationFrame(show) : setTimeout(show);
    };
    if (value) {
        if (el._x_transition && (el._x_transition.enter || el._x_transition.leave)) el._x_transition.enter && (Object.entries(el._x_transition.enter.during).length || Object.entries(el._x_transition.enter.start).length || Object.entries(el._x_transition.enter.end).length) ? el._x_transition.in(show) : clickAwayCompatibleShow();
        else el._x_transition ? el._x_transition.in(show) : clickAwayCompatibleShow();
        return;
    }
    el._x_hidePromise = el._x_transition ? new Promise((resolve, reject)=>{
        el._x_transition.out(()=>{}, ()=>resolve(hide)
        );
        el._x_transitioning.beforeCancel(()=>reject({
                isFromCancelledTransition: true
            })
        );
    }) : Promise.resolve(hide);
    queueMicrotask(()=>{
        let closest = $1df07f6efe5c7d9e$var$closestHide(el);
        if (closest) {
            if (!closest._x_hideChildren) closest._x_hideChildren = [];
            closest._x_hideChildren.push(el);
        } else queueMicrotask(()=>{
            let hideAfterChildren = (el2)=>{
                let carry = Promise.all([
                    el2._x_hidePromise,
                    ...(el2._x_hideChildren || []).map(hideAfterChildren)
                ]).then(([i])=>i()
                );
                delete el2._x_hidePromise;
                delete el2._x_hideChildren;
                return carry;
            };
            hideAfterChildren(el).catch((e)=>{
                if (!e.isFromCancelledTransition) throw e;
            });
        });
    });
};
function $1df07f6efe5c7d9e$var$closestHide(el) {
    let parent = el.parentNode;
    if (!parent) return;
    return parent._x_hidePromise ? parent : $1df07f6efe5c7d9e$var$closestHide(parent);
}
function $1df07f6efe5c7d9e$var$transition(el, setFunction, { during: during , start: start2 , end: end  } = {}, before = ()=>{}, after = ()=>{}) {
    if (el._x_transitioning) el._x_transitioning.cancel();
    if (Object.keys(during).length === 0 && Object.keys(start2).length === 0 && Object.keys(end).length === 0) {
        before();
        after();
        return;
    }
    let undoStart, undoDuring, undoEnd;
    $1df07f6efe5c7d9e$var$performTransition(el, {
        start () {
            undoStart = setFunction(el, start2);
        },
        during () {
            undoDuring = setFunction(el, during);
        },
        before: before,
        end () {
            undoStart();
            undoEnd = setFunction(el, end);
        },
        after: after,
        cleanup () {
            undoDuring();
            undoEnd();
        }
    });
}
function $1df07f6efe5c7d9e$var$performTransition(el, stages) {
    let interrupted, reachedBefore, reachedEnd;
    let finish = $1df07f6efe5c7d9e$var$once(()=>{
        $1df07f6efe5c7d9e$var$mutateDom(()=>{
            interrupted = true;
            if (!reachedBefore) stages.before();
            if (!reachedEnd) {
                stages.end();
                $1df07f6efe5c7d9e$var$releaseNextTicks();
            }
            stages.after();
            if (el.isConnected) stages.cleanup();
            delete el._x_transitioning;
        });
    });
    el._x_transitioning = {
        beforeCancels: [],
        beforeCancel (callback) {
            this.beforeCancels.push(callback);
        },
        cancel: $1df07f6efe5c7d9e$var$once(function() {
            while(this.beforeCancels.length)this.beforeCancels.shift()();
            finish();
        }),
        finish: finish
    };
    $1df07f6efe5c7d9e$var$mutateDom(()=>{
        stages.start();
        stages.during();
    });
    $1df07f6efe5c7d9e$var$holdNextTicks();
    requestAnimationFrame(()=>{
        if (interrupted) return;
        let duration = Number(getComputedStyle(el).transitionDuration.replace(/,.*/, "").replace("s", "")) * 1000;
        let delay = Number(getComputedStyle(el).transitionDelay.replace(/,.*/, "").replace("s", "")) * 1000;
        if (duration === 0) duration = Number(getComputedStyle(el).animationDuration.replace("s", "")) * 1000;
        $1df07f6efe5c7d9e$var$mutateDom(()=>{
            stages.before();
        });
        reachedBefore = true;
        requestAnimationFrame(()=>{
            if (interrupted) return;
            $1df07f6efe5c7d9e$var$mutateDom(()=>{
                stages.end();
            });
            $1df07f6efe5c7d9e$var$releaseNextTicks();
            setTimeout(el._x_transitioning.finish, duration + delay);
            reachedEnd = true;
        });
    });
}
function $1df07f6efe5c7d9e$var$modifierValue(modifiers, key, fallback) {
    if (modifiers.indexOf(key) === -1) return fallback;
    const rawValue = modifiers[modifiers.indexOf(key) + 1];
    if (!rawValue) return fallback;
    if (key === "scale") {
        if (isNaN(rawValue)) return fallback;
    }
    if (key === "duration") {
        let match = rawValue.match(/([0-9]+)ms/);
        if (match) return match[1];
    }
    if (key === "origin") {
        if ([
            "top",
            "right",
            "left",
            "center",
            "bottom"
        ].includes(modifiers[modifiers.indexOf(key) + 2])) return [
            rawValue,
            modifiers[modifiers.indexOf(key) + 2]
        ].join(" ");
    }
    return rawValue;
}
// packages/alpinejs/src/clone.js
var $1df07f6efe5c7d9e$var$isCloning = false;
function $1df07f6efe5c7d9e$var$skipDuringClone(callback, fallback = ()=>{}) {
    return (...args)=>$1df07f6efe5c7d9e$var$isCloning ? fallback(...args) : callback(...args)
    ;
}
function $1df07f6efe5c7d9e$var$clone(oldEl, newEl) {
    if (!newEl._x_dataStack) newEl._x_dataStack = oldEl._x_dataStack;
    $1df07f6efe5c7d9e$var$isCloning = true;
    $1df07f6efe5c7d9e$var$dontRegisterReactiveSideEffects(()=>{
        $1df07f6efe5c7d9e$var$cloneTree(newEl);
    });
    $1df07f6efe5c7d9e$var$isCloning = false;
}
function $1df07f6efe5c7d9e$var$cloneTree(el) {
    let hasRunThroughFirstEl = false;
    let shallowWalker = (el2, callback)=>{
        $1df07f6efe5c7d9e$var$walk(el2, (el3, skip)=>{
            if (hasRunThroughFirstEl && $1df07f6efe5c7d9e$var$isRoot(el3)) return skip();
            hasRunThroughFirstEl = true;
            callback(el3, skip);
        });
    };
    $1df07f6efe5c7d9e$var$initTree(el, shallowWalker);
}
function $1df07f6efe5c7d9e$var$dontRegisterReactiveSideEffects(callback) {
    let cache = $1df07f6efe5c7d9e$var$effect;
    $1df07f6efe5c7d9e$var$overrideEffect((callback2, el)=>{
        let storedEffect = cache(callback2);
        $1df07f6efe5c7d9e$var$release(storedEffect);
        return ()=>{};
    });
    callback();
    $1df07f6efe5c7d9e$var$overrideEffect(cache);
}
// packages/alpinejs/src/utils/bind.js
function $1df07f6efe5c7d9e$var$bind(el, name, value, modifiers = []) {
    if (!el._x_bindings) el._x_bindings = $1df07f6efe5c7d9e$var$reactive({});
    el._x_bindings[name] = value;
    name = modifiers.includes("camel") ? $1df07f6efe5c7d9e$var$camelCase(name) : name;
    switch(name){
        case "value":
            $1df07f6efe5c7d9e$var$bindInputValue(el, value);
            break;
        case "style":
            $1df07f6efe5c7d9e$var$bindStyles(el, value);
            break;
        case "class":
            $1df07f6efe5c7d9e$var$bindClasses(el, value);
            break;
        default:
            $1df07f6efe5c7d9e$var$bindAttribute(el, name, value);
            break;
    }
}
function $1df07f6efe5c7d9e$var$bindInputValue(el, value) {
    if (el.type === "radio") {
        if (el.attributes.value === void 0) el.value = value;
        if (window.fromModel) el.checked = $1df07f6efe5c7d9e$var$checkedAttrLooseCompare(el.value, value);
    } else if (el.type === "checkbox") {
        if (Number.isInteger(value)) el.value = value;
        else if (!Number.isInteger(value) && !Array.isArray(value) && typeof value !== "boolean" && ![
            null,
            void 0
        ].includes(value)) el.value = String(value);
        else if (Array.isArray(value)) el.checked = value.some((val)=>$1df07f6efe5c7d9e$var$checkedAttrLooseCompare(val, el.value)
        );
        else el.checked = !!value;
    } else if (el.tagName === "SELECT") $1df07f6efe5c7d9e$var$updateSelect(el, value);
    else {
        if (el.value === value) return;
        el.value = value;
    }
}
function $1df07f6efe5c7d9e$var$bindClasses(el, value) {
    if (el._x_undoAddedClasses) el._x_undoAddedClasses();
    el._x_undoAddedClasses = $1df07f6efe5c7d9e$var$setClasses(el, value);
}
function $1df07f6efe5c7d9e$var$bindStyles(el, value) {
    if (el._x_undoAddedStyles) el._x_undoAddedStyles();
    el._x_undoAddedStyles = $1df07f6efe5c7d9e$var$setStyles(el, value);
}
function $1df07f6efe5c7d9e$var$bindAttribute(el, name, value) {
    if ([
        null,
        void 0,
        false
    ].includes(value) && $1df07f6efe5c7d9e$var$attributeShouldntBePreservedIfFalsy(name)) el.removeAttribute(name);
    else {
        if ($1df07f6efe5c7d9e$var$isBooleanAttr(name)) value = name;
        $1df07f6efe5c7d9e$var$setIfChanged(el, name, value);
    }
}
function $1df07f6efe5c7d9e$var$setIfChanged(el, attrName, value) {
    if (el.getAttribute(attrName) != value) el.setAttribute(attrName, value);
}
function $1df07f6efe5c7d9e$var$updateSelect(el, value) {
    const arrayWrappedValue = [].concat(value).map((value2)=>{
        return value2 + "";
    });
    Array.from(el.options).forEach((option)=>{
        option.selected = arrayWrappedValue.includes(option.value);
    });
}
function $1df07f6efe5c7d9e$var$camelCase(subject) {
    return subject.toLowerCase().replace(/-(\w)/g, (match, char)=>char.toUpperCase()
    );
}
function $1df07f6efe5c7d9e$var$checkedAttrLooseCompare(valueA, valueB) {
    return valueA == valueB;
}
function $1df07f6efe5c7d9e$var$isBooleanAttr(attrName) {
    const booleanAttributes = [
        "disabled",
        "checked",
        "required",
        "readonly",
        "hidden",
        "open",
        "selected",
        "autofocus",
        "itemscope",
        "multiple",
        "novalidate",
        "allowfullscreen",
        "allowpaymentrequest",
        "formnovalidate",
        "autoplay",
        "controls",
        "loop",
        "muted",
        "playsinline",
        "default",
        "ismap",
        "reversed",
        "async",
        "defer",
        "nomodule"
    ];
    return booleanAttributes.includes(attrName);
}
function $1df07f6efe5c7d9e$var$attributeShouldntBePreservedIfFalsy(name) {
    return ![
        "aria-pressed",
        "aria-checked",
        "aria-expanded",
        "aria-selected"
    ].includes(name);
}
function $1df07f6efe5c7d9e$var$getBinding(el, name, fallback) {
    if (el._x_bindings && el._x_bindings[name] !== void 0) return el._x_bindings[name];
    let attr = el.getAttribute(name);
    if (attr === null) return typeof fallback === "function" ? fallback() : fallback;
    if ($1df07f6efe5c7d9e$var$isBooleanAttr(name)) return !![
        name,
        "true"
    ].includes(attr);
    if (attr === "") return true;
    return attr;
}
// packages/alpinejs/src/utils/debounce.js
function $1df07f6efe5c7d9e$var$debounce(func, wait) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            func.apply(context, args);
        };
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
    };
}
// packages/alpinejs/src/utils/throttle.js
function $1df07f6efe5c7d9e$var$throttle(func, limit) {
    let inThrottle;
    return function() {
        let context = this, args = arguments;
        if (!inThrottle) {
            func.apply(context, args);
            inThrottle = true;
            setTimeout(()=>inThrottle = false
            , limit);
        }
    };
}
// packages/alpinejs/src/plugin.js
function $1df07f6efe5c7d9e$var$plugin(callback) {
    callback($1df07f6efe5c7d9e$var$alpine_default);
}
// packages/alpinejs/src/store.js
var $1df07f6efe5c7d9e$var$stores = {};
var $1df07f6efe5c7d9e$var$isReactive = false;
function $1df07f6efe5c7d9e$var$store(name, value) {
    if (!$1df07f6efe5c7d9e$var$isReactive) {
        $1df07f6efe5c7d9e$var$stores = $1df07f6efe5c7d9e$var$reactive($1df07f6efe5c7d9e$var$stores);
        $1df07f6efe5c7d9e$var$isReactive = true;
    }
    if (value === void 0) return $1df07f6efe5c7d9e$var$stores[name];
    $1df07f6efe5c7d9e$var$stores[name] = value;
    if (typeof value === "object" && value !== null && value.hasOwnProperty("init") && typeof value.init === "function") $1df07f6efe5c7d9e$var$stores[name].init();
    $1df07f6efe5c7d9e$var$initInterceptors($1df07f6efe5c7d9e$var$stores[name]);
}
function $1df07f6efe5c7d9e$var$getStores() {
    return $1df07f6efe5c7d9e$var$stores;
}
// packages/alpinejs/src/binds.js
var $1df07f6efe5c7d9e$var$binds = {};
function $1df07f6efe5c7d9e$var$bind2(name, object) {
    $1df07f6efe5c7d9e$var$binds[name] = typeof object !== "function" ? ()=>object
     : object;
}
function $1df07f6efe5c7d9e$var$injectBindingProviders(obj) {
    Object.entries($1df07f6efe5c7d9e$var$binds).forEach(([name, callback])=>{
        Object.defineProperty(obj, name, {
            get () {
                return (...args)=>{
                    return callback(...args);
                };
            }
        });
    });
    return obj;
}
// packages/alpinejs/src/datas.js
var $1df07f6efe5c7d9e$var$datas = {};
function $1df07f6efe5c7d9e$var$data(name, callback) {
    $1df07f6efe5c7d9e$var$datas[name] = callback;
}
function $1df07f6efe5c7d9e$var$injectDataProviders(obj, context) {
    Object.entries($1df07f6efe5c7d9e$var$datas).forEach(([name, callback])=>{
        Object.defineProperty(obj, name, {
            get () {
                return (...args)=>{
                    return callback.bind(context)(...args);
                };
            },
            enumerable: false
        });
    });
    return obj;
}
// packages/alpinejs/src/alpine.js
var $1df07f6efe5c7d9e$var$Alpine = {
    get reactive () {
        return $1df07f6efe5c7d9e$var$reactive;
    },
    get release () {
        return $1df07f6efe5c7d9e$var$release;
    },
    get effect () {
        return $1df07f6efe5c7d9e$var$effect;
    },
    get raw () {
        return $1df07f6efe5c7d9e$var$raw;
    },
    version: "3.9.5",
    flushAndStopDeferringMutations: $1df07f6efe5c7d9e$var$flushAndStopDeferringMutations,
    disableEffectScheduling: $1df07f6efe5c7d9e$var$disableEffectScheduling,
    setReactivityEngine: $1df07f6efe5c7d9e$var$setReactivityEngine,
    closestDataStack: $1df07f6efe5c7d9e$var$closestDataStack,
    skipDuringClone: $1df07f6efe5c7d9e$var$skipDuringClone,
    addRootSelector: $1df07f6efe5c7d9e$var$addRootSelector,
    addInitSelector: $1df07f6efe5c7d9e$var$addInitSelector,
    addScopeToNode: $1df07f6efe5c7d9e$var$addScopeToNode,
    deferMutations: $1df07f6efe5c7d9e$var$deferMutations,
    mapAttributes: $1df07f6efe5c7d9e$var$mapAttributes,
    evaluateLater: $1df07f6efe5c7d9e$var$evaluateLater,
    setEvaluator: $1df07f6efe5c7d9e$var$setEvaluator,
    mergeProxies: $1df07f6efe5c7d9e$var$mergeProxies,
    findClosest: $1df07f6efe5c7d9e$var$findClosest,
    closestRoot: $1df07f6efe5c7d9e$var$closestRoot,
    interceptor: $1df07f6efe5c7d9e$var$interceptor,
    transition: $1df07f6efe5c7d9e$var$transition,
    setStyles: $1df07f6efe5c7d9e$var$setStyles,
    mutateDom: $1df07f6efe5c7d9e$var$mutateDom,
    directive: $1df07f6efe5c7d9e$var$directive,
    throttle: $1df07f6efe5c7d9e$var$throttle,
    debounce: $1df07f6efe5c7d9e$var$debounce,
    evaluate: $1df07f6efe5c7d9e$var$evaluate,
    initTree: $1df07f6efe5c7d9e$var$initTree,
    nextTick: $1df07f6efe5c7d9e$var$nextTick,
    prefixed: $1df07f6efe5c7d9e$var$prefix,
    prefix: $1df07f6efe5c7d9e$var$setPrefix,
    plugin: $1df07f6efe5c7d9e$var$plugin,
    magic: $1df07f6efe5c7d9e$var$magic,
    store: $1df07f6efe5c7d9e$var$store,
    start: $1df07f6efe5c7d9e$var$start,
    clone: $1df07f6efe5c7d9e$var$clone,
    bound: $1df07f6efe5c7d9e$var$getBinding,
    $data: $1df07f6efe5c7d9e$var$scope,
    data: $1df07f6efe5c7d9e$var$data,
    bind: $1df07f6efe5c7d9e$var$bind2
};
var $1df07f6efe5c7d9e$var$alpine_default = $1df07f6efe5c7d9e$var$Alpine;
// node_modules/@vue/shared/dist/shared.esm-bundler.js
function $1df07f6efe5c7d9e$var$makeMap(str, expectsLowerCase) {
    const map = Object.create(null);
    const list = str.split(",");
    for(let i = 0; i < list.length; i++)map[list[i]] = true;
    return expectsLowerCase ? (val)=>!!map[val.toLowerCase()]
     : (val)=>!!map[val]
    ;
}
var $1df07f6efe5c7d9e$var$PatchFlagNames = {
    [1]: `TEXT`,
    [2]: `CLASS`,
    [4]: `STYLE`,
    [8]: `PROPS`,
    [16]: `FULL_PROPS`,
    [32]: `HYDRATE_EVENTS`,
    [64]: `STABLE_FRAGMENT`,
    [128]: `KEYED_FRAGMENT`,
    [256]: `UNKEYED_FRAGMENT`,
    [512]: `NEED_PATCH`,
    [1024]: `DYNAMIC_SLOTS`,
    [2048]: `DEV_ROOT_FRAGMENT`,
    [-1]: `HOISTED`,
    [-2]: `BAIL`
};
var $1df07f6efe5c7d9e$var$slotFlagsText = {
    [1]: "STABLE",
    [2]: "DYNAMIC",
    [3]: "FORWARDED"
};
var $1df07f6efe5c7d9e$var$specialBooleanAttrs = `itemscope,allowfullscreen,formnovalidate,ismap,nomodule,novalidate,readonly`;
var $1df07f6efe5c7d9e$var$isBooleanAttr2 = /* @__PURE__ */ $1df07f6efe5c7d9e$var$makeMap($1df07f6efe5c7d9e$var$specialBooleanAttrs + `,async,autofocus,autoplay,controls,default,defer,disabled,hidden,loop,open,required,reversed,scoped,seamless,checked,muted,multiple,selected`);
var $1df07f6efe5c7d9e$var$EMPTY_OBJ = Object.freeze({});
var $1df07f6efe5c7d9e$var$EMPTY_ARR = Object.freeze([]);
var $1df07f6efe5c7d9e$var$extend = Object.assign;
var $1df07f6efe5c7d9e$var$hasOwnProperty = Object.prototype.hasOwnProperty;
var $1df07f6efe5c7d9e$var$hasOwn = (val, key)=>$1df07f6efe5c7d9e$var$hasOwnProperty.call(val, key)
;
var $1df07f6efe5c7d9e$var$isArray = Array.isArray;
var $1df07f6efe5c7d9e$var$isMap = (val)=>$1df07f6efe5c7d9e$var$toTypeString(val) === "[object Map]"
;
var $1df07f6efe5c7d9e$var$isString = (val)=>typeof val === "string"
;
var $1df07f6efe5c7d9e$var$isSymbol = (val)=>typeof val === "symbol"
;
var $1df07f6efe5c7d9e$var$isObject = (val)=>val !== null && typeof val === "object"
;
var $1df07f6efe5c7d9e$var$objectToString = Object.prototype.toString;
var $1df07f6efe5c7d9e$var$toTypeString = (value)=>$1df07f6efe5c7d9e$var$objectToString.call(value)
;
var $1df07f6efe5c7d9e$var$toRawType = (value)=>{
    return $1df07f6efe5c7d9e$var$toTypeString(value).slice(8, -1);
};
var $1df07f6efe5c7d9e$var$isIntegerKey = (key)=>$1df07f6efe5c7d9e$var$isString(key) && key !== "NaN" && key[0] !== "-" && "" + parseInt(key, 10) === key
;
var $1df07f6efe5c7d9e$var$cacheStringFunction = (fn)=>{
    const cache = Object.create(null);
    return (str)=>{
        const hit = cache[str];
        return hit || (cache[str] = fn(str));
    };
};
var $1df07f6efe5c7d9e$var$camelizeRE = /-(\w)/g;
var $1df07f6efe5c7d9e$var$camelize = $1df07f6efe5c7d9e$var$cacheStringFunction((str)=>{
    return str.replace($1df07f6efe5c7d9e$var$camelizeRE, (_, c)=>c ? c.toUpperCase() : ""
    );
});
var $1df07f6efe5c7d9e$var$hyphenateRE = /\B([A-Z])/g;
var $1df07f6efe5c7d9e$var$hyphenate = $1df07f6efe5c7d9e$var$cacheStringFunction((str)=>str.replace($1df07f6efe5c7d9e$var$hyphenateRE, "-$1").toLowerCase()
);
var $1df07f6efe5c7d9e$var$capitalize = $1df07f6efe5c7d9e$var$cacheStringFunction((str)=>str.charAt(0).toUpperCase() + str.slice(1)
);
var $1df07f6efe5c7d9e$var$toHandlerKey = $1df07f6efe5c7d9e$var$cacheStringFunction((str)=>str ? `on${$1df07f6efe5c7d9e$var$capitalize(str)}` : ``
);
var $1df07f6efe5c7d9e$var$hasChanged = (value, oldValue)=>value !== oldValue && (value === value || oldValue === oldValue)
;
// node_modules/@vue/reactivity/dist/reactivity.esm-bundler.js
var $1df07f6efe5c7d9e$var$targetMap = new WeakMap();
var $1df07f6efe5c7d9e$var$effectStack = [];
var $1df07f6efe5c7d9e$var$activeEffect;
var $1df07f6efe5c7d9e$var$ITERATE_KEY = Symbol("iterate");
var $1df07f6efe5c7d9e$var$MAP_KEY_ITERATE_KEY = Symbol("Map key iterate");
function $1df07f6efe5c7d9e$var$isEffect(fn) {
    return fn && fn._isEffect === true;
}
function $1df07f6efe5c7d9e$var$effect2(fn, options = $1df07f6efe5c7d9e$var$EMPTY_OBJ) {
    if ($1df07f6efe5c7d9e$var$isEffect(fn)) fn = fn.raw;
    const effect3 = $1df07f6efe5c7d9e$var$createReactiveEffect(fn, options);
    if (!options.lazy) effect3();
    return effect3;
}
function $1df07f6efe5c7d9e$var$stop(effect3) {
    if (effect3.active) {
        $1df07f6efe5c7d9e$var$cleanup(effect3);
        if (effect3.options.onStop) effect3.options.onStop();
        effect3.active = false;
    }
}
var $1df07f6efe5c7d9e$var$uid = 0;
function $1df07f6efe5c7d9e$var$createReactiveEffect(fn, options) {
    const effect3 = function reactiveEffect() {
        if (!effect3.active) return fn();
        if (!$1df07f6efe5c7d9e$var$effectStack.includes(effect3)) {
            $1df07f6efe5c7d9e$var$cleanup(effect3);
            try {
                $1df07f6efe5c7d9e$var$enableTracking();
                $1df07f6efe5c7d9e$var$effectStack.push(effect3);
                $1df07f6efe5c7d9e$var$activeEffect = effect3;
                return fn();
            } finally{
                $1df07f6efe5c7d9e$var$effectStack.pop();
                $1df07f6efe5c7d9e$var$resetTracking();
                $1df07f6efe5c7d9e$var$activeEffect = $1df07f6efe5c7d9e$var$effectStack[$1df07f6efe5c7d9e$var$effectStack.length - 1];
            }
        }
    };
    effect3.id = $1df07f6efe5c7d9e$var$uid++;
    effect3.allowRecurse = !!options.allowRecurse;
    effect3._isEffect = true;
    effect3.active = true;
    effect3.raw = fn;
    effect3.deps = [];
    effect3.options = options;
    return effect3;
}
function $1df07f6efe5c7d9e$var$cleanup(effect3) {
    const { deps: deps  } = effect3;
    if (deps.length) {
        for(let i = 0; i < deps.length; i++)deps[i].delete(effect3);
        deps.length = 0;
    }
}
var $1df07f6efe5c7d9e$var$shouldTrack = true;
var $1df07f6efe5c7d9e$var$trackStack = [];
function $1df07f6efe5c7d9e$var$pauseTracking() {
    $1df07f6efe5c7d9e$var$trackStack.push($1df07f6efe5c7d9e$var$shouldTrack);
    $1df07f6efe5c7d9e$var$shouldTrack = false;
}
function $1df07f6efe5c7d9e$var$enableTracking() {
    $1df07f6efe5c7d9e$var$trackStack.push($1df07f6efe5c7d9e$var$shouldTrack);
    $1df07f6efe5c7d9e$var$shouldTrack = true;
}
function $1df07f6efe5c7d9e$var$resetTracking() {
    const last = $1df07f6efe5c7d9e$var$trackStack.pop();
    $1df07f6efe5c7d9e$var$shouldTrack = last === void 0 ? true : last;
}
function $1df07f6efe5c7d9e$var$track(target, type, key) {
    if (!$1df07f6efe5c7d9e$var$shouldTrack || $1df07f6efe5c7d9e$var$activeEffect === void 0) return;
    let depsMap = $1df07f6efe5c7d9e$var$targetMap.get(target);
    if (!depsMap) $1df07f6efe5c7d9e$var$targetMap.set(target, depsMap = new Map());
    let dep = depsMap.get(key);
    if (!dep) depsMap.set(key, dep = new Set());
    if (!dep.has($1df07f6efe5c7d9e$var$activeEffect)) {
        dep.add($1df07f6efe5c7d9e$var$activeEffect);
        $1df07f6efe5c7d9e$var$activeEffect.deps.push(dep);
        if ($1df07f6efe5c7d9e$var$activeEffect.options.onTrack) $1df07f6efe5c7d9e$var$activeEffect.options.onTrack({
            effect: $1df07f6efe5c7d9e$var$activeEffect,
            target: target,
            type: type,
            key: key
        });
    }
}
function $1df07f6efe5c7d9e$var$trigger(target, type, key, newValue, oldValue, oldTarget) {
    const depsMap = $1df07f6efe5c7d9e$var$targetMap.get(target);
    if (!depsMap) return;
    const effects = new Set();
    const add2 = (effectsToAdd)=>{
        if (effectsToAdd) effectsToAdd.forEach((effect3)=>{
            if (effect3 !== $1df07f6efe5c7d9e$var$activeEffect || effect3.allowRecurse) effects.add(effect3);
        });
    };
    if (type === "clear") depsMap.forEach(add2);
    else if (key === "length" && $1df07f6efe5c7d9e$var$isArray(target)) depsMap.forEach((dep, key2)=>{
        if (key2 === "length" || key2 >= newValue) add2(dep);
    });
    else {
        if (key !== void 0) add2(depsMap.get(key));
        switch(type){
            case "add":
                if (!$1df07f6efe5c7d9e$var$isArray(target)) {
                    add2(depsMap.get($1df07f6efe5c7d9e$var$ITERATE_KEY));
                    if ($1df07f6efe5c7d9e$var$isMap(target)) add2(depsMap.get($1df07f6efe5c7d9e$var$MAP_KEY_ITERATE_KEY));
                } else if ($1df07f6efe5c7d9e$var$isIntegerKey(key)) add2(depsMap.get("length"));
                break;
            case "delete":
                if (!$1df07f6efe5c7d9e$var$isArray(target)) {
                    add2(depsMap.get($1df07f6efe5c7d9e$var$ITERATE_KEY));
                    if ($1df07f6efe5c7d9e$var$isMap(target)) add2(depsMap.get($1df07f6efe5c7d9e$var$MAP_KEY_ITERATE_KEY));
                }
                break;
            case "set":
                if ($1df07f6efe5c7d9e$var$isMap(target)) add2(depsMap.get($1df07f6efe5c7d9e$var$ITERATE_KEY));
                break;
        }
    }
    const run = (effect3)=>{
        if (effect3.options.onTrigger) effect3.options.onTrigger({
            effect: effect3,
            target: target,
            key: key,
            type: type,
            newValue: newValue,
            oldValue: oldValue,
            oldTarget: oldTarget
        });
        if (effect3.options.scheduler) effect3.options.scheduler(effect3);
        else effect3();
    };
    effects.forEach(run);
}
var $1df07f6efe5c7d9e$var$isNonTrackableKeys = /* @__PURE__ */ $1df07f6efe5c7d9e$var$makeMap(`__proto__,__v_isRef,__isVue`);
var $1df07f6efe5c7d9e$var$builtInSymbols = new Set(Object.getOwnPropertyNames(Symbol).map((key)=>Symbol[key]
).filter($1df07f6efe5c7d9e$var$isSymbol));
var $1df07f6efe5c7d9e$var$get2 = /* @__PURE__ */ $1df07f6efe5c7d9e$var$createGetter();
var $1df07f6efe5c7d9e$var$shallowGet = /* @__PURE__ */ $1df07f6efe5c7d9e$var$createGetter(false, true);
var $1df07f6efe5c7d9e$var$readonlyGet = /* @__PURE__ */ $1df07f6efe5c7d9e$var$createGetter(true);
var $1df07f6efe5c7d9e$var$shallowReadonlyGet = /* @__PURE__ */ $1df07f6efe5c7d9e$var$createGetter(true, true);
var $1df07f6efe5c7d9e$var$arrayInstrumentations = {};
[
    "includes",
    "indexOf",
    "lastIndexOf"
].forEach((key)=>{
    const method = Array.prototype[key];
    $1df07f6efe5c7d9e$var$arrayInstrumentations[key] = function(...args) {
        const arr = $1df07f6efe5c7d9e$var$toRaw(this);
        for(let i = 0, l = this.length; i < l; i++)$1df07f6efe5c7d9e$var$track(arr, "get", i + "");
        const res = method.apply(arr, args);
        if (res === -1 || res === false) return method.apply(arr, args.map($1df07f6efe5c7d9e$var$toRaw));
        else return res;
    };
});
[
    "push",
    "pop",
    "shift",
    "unshift",
    "splice"
].forEach((key)=>{
    const method = Array.prototype[key];
    $1df07f6efe5c7d9e$var$arrayInstrumentations[key] = function(...args) {
        $1df07f6efe5c7d9e$var$pauseTracking();
        const res = method.apply(this, args);
        $1df07f6efe5c7d9e$var$resetTracking();
        return res;
    };
});
function $1df07f6efe5c7d9e$var$createGetter(isReadonly = false, shallow = false) {
    return function get3(target, key, receiver) {
        if (key === "__v_isReactive") return !isReadonly;
        else if (key === "__v_isReadonly") return isReadonly;
        else if (key === "__v_raw" && receiver === (isReadonly ? shallow ? $1df07f6efe5c7d9e$var$shallowReadonlyMap : $1df07f6efe5c7d9e$var$readonlyMap : shallow ? $1df07f6efe5c7d9e$var$shallowReactiveMap : $1df07f6efe5c7d9e$var$reactiveMap).get(target)) return target;
        const targetIsArray = $1df07f6efe5c7d9e$var$isArray(target);
        if (!isReadonly && targetIsArray && $1df07f6efe5c7d9e$var$hasOwn($1df07f6efe5c7d9e$var$arrayInstrumentations, key)) return Reflect.get($1df07f6efe5c7d9e$var$arrayInstrumentations, key, receiver);
        const res = Reflect.get(target, key, receiver);
        if ($1df07f6efe5c7d9e$var$isSymbol(key) ? $1df07f6efe5c7d9e$var$builtInSymbols.has(key) : $1df07f6efe5c7d9e$var$isNonTrackableKeys(key)) return res;
        if (!isReadonly) $1df07f6efe5c7d9e$var$track(target, "get", key);
        if (shallow) return res;
        if ($1df07f6efe5c7d9e$var$isRef(res)) {
            const shouldUnwrap = !targetIsArray || !$1df07f6efe5c7d9e$var$isIntegerKey(key);
            return shouldUnwrap ? res.value : res;
        }
        if ($1df07f6efe5c7d9e$var$isObject(res)) return isReadonly ? $1df07f6efe5c7d9e$var$readonly(res) : $1df07f6efe5c7d9e$var$reactive2(res);
        return res;
    };
}
var $1df07f6efe5c7d9e$var$set2 = /* @__PURE__ */ $1df07f6efe5c7d9e$var$createSetter();
var $1df07f6efe5c7d9e$var$shallowSet = /* @__PURE__ */ $1df07f6efe5c7d9e$var$createSetter(true);
function $1df07f6efe5c7d9e$var$createSetter(shallow = false) {
    return function set3(target, key, value, receiver) {
        let oldValue = target[key];
        if (!shallow) {
            value = $1df07f6efe5c7d9e$var$toRaw(value);
            oldValue = $1df07f6efe5c7d9e$var$toRaw(oldValue);
            if (!$1df07f6efe5c7d9e$var$isArray(target) && $1df07f6efe5c7d9e$var$isRef(oldValue) && !$1df07f6efe5c7d9e$var$isRef(value)) {
                oldValue.value = value;
                return true;
            }
        }
        const hadKey = $1df07f6efe5c7d9e$var$isArray(target) && $1df07f6efe5c7d9e$var$isIntegerKey(key) ? Number(key) < target.length : $1df07f6efe5c7d9e$var$hasOwn(target, key);
        const result = Reflect.set(target, key, value, receiver);
        if (target === $1df07f6efe5c7d9e$var$toRaw(receiver)) {
            if (!hadKey) $1df07f6efe5c7d9e$var$trigger(target, "add", key, value);
            else if ($1df07f6efe5c7d9e$var$hasChanged(value, oldValue)) $1df07f6efe5c7d9e$var$trigger(target, "set", key, value, oldValue);
        }
        return result;
    };
}
function $1df07f6efe5c7d9e$var$deleteProperty(target, key) {
    const hadKey = $1df07f6efe5c7d9e$var$hasOwn(target, key);
    const oldValue = target[key];
    const result = Reflect.deleteProperty(target, key);
    if (result && hadKey) $1df07f6efe5c7d9e$var$trigger(target, "delete", key, void 0, oldValue);
    return result;
}
function $1df07f6efe5c7d9e$var$has(target, key) {
    const result = Reflect.has(target, key);
    if (!$1df07f6efe5c7d9e$var$isSymbol(key) || !$1df07f6efe5c7d9e$var$builtInSymbols.has(key)) $1df07f6efe5c7d9e$var$track(target, "has", key);
    return result;
}
function $1df07f6efe5c7d9e$var$ownKeys(target) {
    $1df07f6efe5c7d9e$var$track(target, "iterate", $1df07f6efe5c7d9e$var$isArray(target) ? "length" : $1df07f6efe5c7d9e$var$ITERATE_KEY);
    return Reflect.ownKeys(target);
}
var $1df07f6efe5c7d9e$var$mutableHandlers = {
    get: $1df07f6efe5c7d9e$var$get2,
    set: $1df07f6efe5c7d9e$var$set2,
    deleteProperty: $1df07f6efe5c7d9e$var$deleteProperty,
    has: $1df07f6efe5c7d9e$var$has,
    ownKeys: $1df07f6efe5c7d9e$var$ownKeys
};
var $1df07f6efe5c7d9e$var$readonlyHandlers = {
    get: $1df07f6efe5c7d9e$var$readonlyGet,
    set (target, key) {
        console.warn(`Set operation on key "${String(key)}" failed: target is readonly.`, target);
        return true;
    },
    deleteProperty (target, key) {
        console.warn(`Delete operation on key "${String(key)}" failed: target is readonly.`, target);
        return true;
    }
};
var $1df07f6efe5c7d9e$var$shallowReactiveHandlers = $1df07f6efe5c7d9e$var$extend({}, $1df07f6efe5c7d9e$var$mutableHandlers, {
    get: $1df07f6efe5c7d9e$var$shallowGet,
    set: $1df07f6efe5c7d9e$var$shallowSet
});
var $1df07f6efe5c7d9e$var$shallowReadonlyHandlers = $1df07f6efe5c7d9e$var$extend({}, $1df07f6efe5c7d9e$var$readonlyHandlers, {
    get: $1df07f6efe5c7d9e$var$shallowReadonlyGet
});
var $1df07f6efe5c7d9e$var$toReactive = (value)=>$1df07f6efe5c7d9e$var$isObject(value) ? $1df07f6efe5c7d9e$var$reactive2(value) : value
;
var $1df07f6efe5c7d9e$var$toReadonly = (value)=>$1df07f6efe5c7d9e$var$isObject(value) ? $1df07f6efe5c7d9e$var$readonly(value) : value
;
var $1df07f6efe5c7d9e$var$toShallow = (value)=>value
;
var $1df07f6efe5c7d9e$var$getProto = (v)=>Reflect.getPrototypeOf(v)
;
function $1df07f6efe5c7d9e$var$get$1(target, key, isReadonly = false, isShallow = false) {
    target = target["__v_raw"];
    const rawTarget = $1df07f6efe5c7d9e$var$toRaw(target);
    const rawKey = $1df07f6efe5c7d9e$var$toRaw(key);
    if (key !== rawKey) !isReadonly && $1df07f6efe5c7d9e$var$track(rawTarget, "get", key);
    !isReadonly && $1df07f6efe5c7d9e$var$track(rawTarget, "get", rawKey);
    const { has: has2  } = $1df07f6efe5c7d9e$var$getProto(rawTarget);
    const wrap = isShallow ? $1df07f6efe5c7d9e$var$toShallow : isReadonly ? $1df07f6efe5c7d9e$var$toReadonly : $1df07f6efe5c7d9e$var$toReactive;
    if (has2.call(rawTarget, key)) return wrap(target.get(key));
    else if (has2.call(rawTarget, rawKey)) return wrap(target.get(rawKey));
    else if (target !== rawTarget) target.get(key);
}
function $1df07f6efe5c7d9e$var$has$1(key, isReadonly = false) {
    const target = this["__v_raw"];
    const rawTarget = $1df07f6efe5c7d9e$var$toRaw(target);
    const rawKey = $1df07f6efe5c7d9e$var$toRaw(key);
    if (key !== rawKey) !isReadonly && $1df07f6efe5c7d9e$var$track(rawTarget, "has", key);
    !isReadonly && $1df07f6efe5c7d9e$var$track(rawTarget, "has", rawKey);
    return key === rawKey ? target.has(key) : target.has(key) || target.has(rawKey);
}
function $1df07f6efe5c7d9e$var$size(target, isReadonly = false) {
    target = target["__v_raw"];
    !isReadonly && $1df07f6efe5c7d9e$var$track($1df07f6efe5c7d9e$var$toRaw(target), "iterate", $1df07f6efe5c7d9e$var$ITERATE_KEY);
    return Reflect.get(target, "size", target);
}
function $1df07f6efe5c7d9e$var$add(value) {
    value = $1df07f6efe5c7d9e$var$toRaw(value);
    const target = $1df07f6efe5c7d9e$var$toRaw(this);
    const proto = $1df07f6efe5c7d9e$var$getProto(target);
    const hadKey = proto.has.call(target, value);
    if (!hadKey) {
        target.add(value);
        $1df07f6efe5c7d9e$var$trigger(target, "add", value, value);
    }
    return this;
}
function $1df07f6efe5c7d9e$var$set$1(key, value) {
    value = $1df07f6efe5c7d9e$var$toRaw(value);
    const target = $1df07f6efe5c7d9e$var$toRaw(this);
    const { has: has2 , get: get3  } = $1df07f6efe5c7d9e$var$getProto(target);
    let hadKey = has2.call(target, key);
    if (!hadKey) {
        key = $1df07f6efe5c7d9e$var$toRaw(key);
        hadKey = has2.call(target, key);
    } else $1df07f6efe5c7d9e$var$checkIdentityKeys(target, has2, key);
    const oldValue = get3.call(target, key);
    target.set(key, value);
    if (!hadKey) $1df07f6efe5c7d9e$var$trigger(target, "add", key, value);
    else if ($1df07f6efe5c7d9e$var$hasChanged(value, oldValue)) $1df07f6efe5c7d9e$var$trigger(target, "set", key, value, oldValue);
    return this;
}
function $1df07f6efe5c7d9e$var$deleteEntry(key) {
    const target = $1df07f6efe5c7d9e$var$toRaw(this);
    const { has: has2 , get: get3  } = $1df07f6efe5c7d9e$var$getProto(target);
    let hadKey = has2.call(target, key);
    if (!hadKey) {
        key = $1df07f6efe5c7d9e$var$toRaw(key);
        hadKey = has2.call(target, key);
    } else $1df07f6efe5c7d9e$var$checkIdentityKeys(target, has2, key);
    const oldValue = get3 ? get3.call(target, key) : void 0;
    const result = target.delete(key);
    if (hadKey) $1df07f6efe5c7d9e$var$trigger(target, "delete", key, void 0, oldValue);
    return result;
}
function $1df07f6efe5c7d9e$var$clear() {
    const target = $1df07f6efe5c7d9e$var$toRaw(this);
    const hadItems = target.size !== 0;
    const oldTarget = $1df07f6efe5c7d9e$var$isMap(target) ? new Map(target) : new Set(target);
    const result = target.clear();
    if (hadItems) $1df07f6efe5c7d9e$var$trigger(target, "clear", void 0, void 0, oldTarget);
    return result;
}
function $1df07f6efe5c7d9e$var$createForEach(isReadonly, isShallow) {
    return function forEach(callback, thisArg) {
        const observed = this;
        const target = observed["__v_raw"];
        const rawTarget = $1df07f6efe5c7d9e$var$toRaw(target);
        const wrap = isShallow ? $1df07f6efe5c7d9e$var$toShallow : isReadonly ? $1df07f6efe5c7d9e$var$toReadonly : $1df07f6efe5c7d9e$var$toReactive;
        !isReadonly && $1df07f6efe5c7d9e$var$track(rawTarget, "iterate", $1df07f6efe5c7d9e$var$ITERATE_KEY);
        return target.forEach((value, key)=>{
            return callback.call(thisArg, wrap(value), wrap(key), observed);
        });
    };
}
function $1df07f6efe5c7d9e$var$createIterableMethod(method, isReadonly, isShallow) {
    return function(...args) {
        const target = this["__v_raw"];
        const rawTarget = $1df07f6efe5c7d9e$var$toRaw(target);
        const targetIsMap = $1df07f6efe5c7d9e$var$isMap(rawTarget);
        const isPair = method === "entries" || method === Symbol.iterator && targetIsMap;
        const isKeyOnly = method === "keys" && targetIsMap;
        const innerIterator = target[method](...args);
        const wrap = isShallow ? $1df07f6efe5c7d9e$var$toShallow : isReadonly ? $1df07f6efe5c7d9e$var$toReadonly : $1df07f6efe5c7d9e$var$toReactive;
        !isReadonly && $1df07f6efe5c7d9e$var$track(rawTarget, "iterate", isKeyOnly ? $1df07f6efe5c7d9e$var$MAP_KEY_ITERATE_KEY : $1df07f6efe5c7d9e$var$ITERATE_KEY);
        return {
            next () {
                const { value: value , done: done  } = innerIterator.next();
                return done ? {
                    value: value,
                    done: done
                } : {
                    value: isPair ? [
                        wrap(value[0]),
                        wrap(value[1])
                    ] : wrap(value),
                    done: done
                };
            },
            [Symbol.iterator] () {
                return this;
            }
        };
    };
}
function $1df07f6efe5c7d9e$var$createReadonlyMethod(type) {
    return function(...args) {
        {
            const key = args[0] ? `on key "${args[0]}" ` : ``;
            console.warn(`${$1df07f6efe5c7d9e$var$capitalize(type)} operation ${key}failed: target is readonly.`, $1df07f6efe5c7d9e$var$toRaw(this));
        }
        return type === "delete" ? false : this;
    };
}
var $1df07f6efe5c7d9e$var$mutableInstrumentations = {
    get (key) {
        return $1df07f6efe5c7d9e$var$get$1(this, key);
    },
    get size () {
        return $1df07f6efe5c7d9e$var$size(this);
    },
    has: $1df07f6efe5c7d9e$var$has$1,
    add: $1df07f6efe5c7d9e$var$add,
    set: $1df07f6efe5c7d9e$var$set$1,
    delete: $1df07f6efe5c7d9e$var$deleteEntry,
    clear: $1df07f6efe5c7d9e$var$clear,
    forEach: $1df07f6efe5c7d9e$var$createForEach(false, false)
};
var $1df07f6efe5c7d9e$var$shallowInstrumentations = {
    get (key) {
        return $1df07f6efe5c7d9e$var$get$1(this, key, false, true);
    },
    get size () {
        return $1df07f6efe5c7d9e$var$size(this);
    },
    has: $1df07f6efe5c7d9e$var$has$1,
    add: $1df07f6efe5c7d9e$var$add,
    set: $1df07f6efe5c7d9e$var$set$1,
    delete: $1df07f6efe5c7d9e$var$deleteEntry,
    clear: $1df07f6efe5c7d9e$var$clear,
    forEach: $1df07f6efe5c7d9e$var$createForEach(false, true)
};
var $1df07f6efe5c7d9e$var$readonlyInstrumentations = {
    get (key) {
        return $1df07f6efe5c7d9e$var$get$1(this, key, true);
    },
    get size () {
        return $1df07f6efe5c7d9e$var$size(this, true);
    },
    has (key) {
        return $1df07f6efe5c7d9e$var$has$1.call(this, key, true);
    },
    add: $1df07f6efe5c7d9e$var$createReadonlyMethod("add"),
    set: $1df07f6efe5c7d9e$var$createReadonlyMethod("set"),
    delete: $1df07f6efe5c7d9e$var$createReadonlyMethod("delete"),
    clear: $1df07f6efe5c7d9e$var$createReadonlyMethod("clear"),
    forEach: $1df07f6efe5c7d9e$var$createForEach(true, false)
};
var $1df07f6efe5c7d9e$var$shallowReadonlyInstrumentations = {
    get (key) {
        return $1df07f6efe5c7d9e$var$get$1(this, key, true, true);
    },
    get size () {
        return $1df07f6efe5c7d9e$var$size(this, true);
    },
    has (key) {
        return $1df07f6efe5c7d9e$var$has$1.call(this, key, true);
    },
    add: $1df07f6efe5c7d9e$var$createReadonlyMethod("add"),
    set: $1df07f6efe5c7d9e$var$createReadonlyMethod("set"),
    delete: $1df07f6efe5c7d9e$var$createReadonlyMethod("delete"),
    clear: $1df07f6efe5c7d9e$var$createReadonlyMethod("clear"),
    forEach: $1df07f6efe5c7d9e$var$createForEach(true, true)
};
var $1df07f6efe5c7d9e$var$iteratorMethods = [
    "keys",
    "values",
    "entries",
    Symbol.iterator
];
$1df07f6efe5c7d9e$var$iteratorMethods.forEach((method)=>{
    $1df07f6efe5c7d9e$var$mutableInstrumentations[method] = $1df07f6efe5c7d9e$var$createIterableMethod(method, false, false);
    $1df07f6efe5c7d9e$var$readonlyInstrumentations[method] = $1df07f6efe5c7d9e$var$createIterableMethod(method, true, false);
    $1df07f6efe5c7d9e$var$shallowInstrumentations[method] = $1df07f6efe5c7d9e$var$createIterableMethod(method, false, true);
    $1df07f6efe5c7d9e$var$shallowReadonlyInstrumentations[method] = $1df07f6efe5c7d9e$var$createIterableMethod(method, true, true);
});
function $1df07f6efe5c7d9e$var$createInstrumentationGetter(isReadonly, shallow) {
    const instrumentations = shallow ? isReadonly ? $1df07f6efe5c7d9e$var$shallowReadonlyInstrumentations : $1df07f6efe5c7d9e$var$shallowInstrumentations : isReadonly ? $1df07f6efe5c7d9e$var$readonlyInstrumentations : $1df07f6efe5c7d9e$var$mutableInstrumentations;
    return (target, key, receiver)=>{
        if (key === "__v_isReactive") return !isReadonly;
        else if (key === "__v_isReadonly") return isReadonly;
        else if (key === "__v_raw") return target;
        return Reflect.get($1df07f6efe5c7d9e$var$hasOwn(instrumentations, key) && key in target ? instrumentations : target, key, receiver);
    };
}
var $1df07f6efe5c7d9e$var$mutableCollectionHandlers = {
    get: $1df07f6efe5c7d9e$var$createInstrumentationGetter(false, false)
};
var $1df07f6efe5c7d9e$var$shallowCollectionHandlers = {
    get: $1df07f6efe5c7d9e$var$createInstrumentationGetter(false, true)
};
var $1df07f6efe5c7d9e$var$readonlyCollectionHandlers = {
    get: $1df07f6efe5c7d9e$var$createInstrumentationGetter(true, false)
};
var $1df07f6efe5c7d9e$var$shallowReadonlyCollectionHandlers = {
    get: $1df07f6efe5c7d9e$var$createInstrumentationGetter(true, true)
};
function $1df07f6efe5c7d9e$var$checkIdentityKeys(target, has2, key) {
    const rawKey = $1df07f6efe5c7d9e$var$toRaw(key);
    if (rawKey !== key && has2.call(target, rawKey)) {
        const type = $1df07f6efe5c7d9e$var$toRawType(target);
        console.warn(`Reactive ${type} contains both the raw and reactive versions of the same object${type === `Map` ? ` as keys` : ``}, which can lead to inconsistencies. Avoid differentiating between the raw and reactive versions of an object and only use the reactive version if possible.`);
    }
}
var $1df07f6efe5c7d9e$var$reactiveMap = new WeakMap();
var $1df07f6efe5c7d9e$var$shallowReactiveMap = new WeakMap();
var $1df07f6efe5c7d9e$var$readonlyMap = new WeakMap();
var $1df07f6efe5c7d9e$var$shallowReadonlyMap = new WeakMap();
function $1df07f6efe5c7d9e$var$targetTypeMap(rawType) {
    switch(rawType){
        case "Object":
        case "Array":
            return 1;
        case "Map":
        case "Set":
        case "WeakMap":
        case "WeakSet":
            return 2;
        default:
            return 0;
    }
}
function $1df07f6efe5c7d9e$var$getTargetType(value) {
    return value["__v_skip"] || !Object.isExtensible(value) ? 0 : $1df07f6efe5c7d9e$var$targetTypeMap($1df07f6efe5c7d9e$var$toRawType(value));
}
function $1df07f6efe5c7d9e$var$reactive2(target) {
    if (target && target["__v_isReadonly"]) return target;
    return $1df07f6efe5c7d9e$var$createReactiveObject(target, false, $1df07f6efe5c7d9e$var$mutableHandlers, $1df07f6efe5c7d9e$var$mutableCollectionHandlers, $1df07f6efe5c7d9e$var$reactiveMap);
}
function $1df07f6efe5c7d9e$var$readonly(target) {
    return $1df07f6efe5c7d9e$var$createReactiveObject(target, true, $1df07f6efe5c7d9e$var$readonlyHandlers, $1df07f6efe5c7d9e$var$readonlyCollectionHandlers, $1df07f6efe5c7d9e$var$readonlyMap);
}
function $1df07f6efe5c7d9e$var$createReactiveObject(target, isReadonly, baseHandlers, collectionHandlers, proxyMap) {
    if (!$1df07f6efe5c7d9e$var$isObject(target)) {
        console.warn(`value cannot be made reactive: ${String(target)}`);
        return target;
    }
    if (target["__v_raw"] && !(isReadonly && target["__v_isReactive"])) return target;
    const existingProxy = proxyMap.get(target);
    if (existingProxy) return existingProxy;
    const targetType = $1df07f6efe5c7d9e$var$getTargetType(target);
    if (targetType === 0) return target;
    const proxy = new Proxy(target, targetType === 2 ? collectionHandlers : baseHandlers);
    proxyMap.set(target, proxy);
    return proxy;
}
function $1df07f6efe5c7d9e$var$toRaw(observed) {
    return observed && $1df07f6efe5c7d9e$var$toRaw(observed["__v_raw"]) || observed;
}
function $1df07f6efe5c7d9e$var$isRef(r) {
    return Boolean(r && r.__v_isRef === true);
}
// packages/alpinejs/src/magics/$nextTick.js
$1df07f6efe5c7d9e$var$magic("nextTick", ()=>$1df07f6efe5c7d9e$var$nextTick
);
// packages/alpinejs/src/magics/$dispatch.js
$1df07f6efe5c7d9e$var$magic("dispatch", (el)=>$1df07f6efe5c7d9e$var$dispatch.bind($1df07f6efe5c7d9e$var$dispatch, el)
);
// packages/alpinejs/src/magics/$watch.js
$1df07f6efe5c7d9e$var$magic("watch", (el, { evaluateLater: evaluateLater2 , effect: effect3  })=>(key, callback)=>{
        let evaluate2 = evaluateLater2(key);
        let firstTime = true;
        let oldValue;
        let effectReference = effect3(()=>evaluate2((value)=>{
                JSON.stringify(value);
                if (!firstTime) queueMicrotask(()=>{
                    callback(value, oldValue);
                    oldValue = value;
                });
                else oldValue = value;
                firstTime = false;
            })
        );
        el._x_effects.delete(effectReference);
    }
);
// packages/alpinejs/src/magics/$store.js
$1df07f6efe5c7d9e$var$magic("store", $1df07f6efe5c7d9e$var$getStores);
// packages/alpinejs/src/magics/$data.js
$1df07f6efe5c7d9e$var$magic("data", (el)=>$1df07f6efe5c7d9e$var$scope(el)
);
// packages/alpinejs/src/magics/$root.js
$1df07f6efe5c7d9e$var$magic("root", (el)=>$1df07f6efe5c7d9e$var$closestRoot(el)
);
// packages/alpinejs/src/magics/$refs.js
$1df07f6efe5c7d9e$var$magic("refs", (el)=>{
    if (el._x_refs_proxy) return el._x_refs_proxy;
    el._x_refs_proxy = $1df07f6efe5c7d9e$var$mergeProxies($1df07f6efe5c7d9e$var$getArrayOfRefObject(el));
    return el._x_refs_proxy;
});
function $1df07f6efe5c7d9e$var$getArrayOfRefObject(el) {
    let refObjects = [];
    let currentEl = el;
    while(currentEl){
        if (currentEl._x_refs) refObjects.push(currentEl._x_refs);
        currentEl = currentEl.parentNode;
    }
    return refObjects;
}
// packages/alpinejs/src/ids.js
var $1df07f6efe5c7d9e$var$globalIdMemo = {};
function $1df07f6efe5c7d9e$var$findAndIncrementId(name) {
    if (!$1df07f6efe5c7d9e$var$globalIdMemo[name]) $1df07f6efe5c7d9e$var$globalIdMemo[name] = 0;
    return ++$1df07f6efe5c7d9e$var$globalIdMemo[name];
}
function $1df07f6efe5c7d9e$var$closestIdRoot(el, name) {
    return $1df07f6efe5c7d9e$var$findClosest(el, (element)=>{
        if (element._x_ids && element._x_ids[name]) return true;
    });
}
function $1df07f6efe5c7d9e$var$setIdRoot(el, name) {
    if (!el._x_ids) el._x_ids = {};
    if (!el._x_ids[name]) el._x_ids[name] = $1df07f6efe5c7d9e$var$findAndIncrementId(name);
}
// packages/alpinejs/src/magics/$id.js
$1df07f6efe5c7d9e$var$magic("id", (el)=>(name, key = null)=>{
        let root = $1df07f6efe5c7d9e$var$closestIdRoot(el, name);
        let id = root ? root._x_ids[name] : $1df07f6efe5c7d9e$var$findAndIncrementId(name);
        return key ? `${name}-${id}-${key}` : `${name}-${id}`;
    }
);
// packages/alpinejs/src/magics/$el.js
$1df07f6efe5c7d9e$var$magic("el", (el)=>el
);
// packages/alpinejs/src/directives/x-modelable.js
$1df07f6efe5c7d9e$var$directive("modelable", (el, { expression: expression  }, { effect: effect3 , evaluateLater: evaluateLater2  })=>{
    let func = evaluateLater2(expression);
    let innerGet = ()=>{
        let result;
        func((i)=>result = i
        );
        return result;
    };
    let evaluateInnerSet = evaluateLater2(`${expression} = __placeholder`);
    let innerSet = (val)=>evaluateInnerSet(()=>{}, {
            scope: {
                __placeholder: val
            }
        })
    ;
    let initialValue = innerGet();
    innerSet(initialValue);
    queueMicrotask(()=>{
        if (!el._x_model) return;
        el._x_removeModelListeners["default"]();
        let outerGet = el._x_model.get;
        let outerSet = el._x_model.set;
        effect3(()=>innerSet(outerGet())
        );
        effect3(()=>outerSet(innerGet())
        );
    });
});
// packages/alpinejs/src/directives/x-teleport.js
$1df07f6efe5c7d9e$var$directive("teleport", (el, { expression: expression  }, { cleanup: cleanup2  })=>{
    if (el.tagName.toLowerCase() !== "template") $1df07f6efe5c7d9e$var$warn("x-teleport can only be used on a <template> tag", el);
    let target = document.querySelector(expression);
    if (!target) $1df07f6efe5c7d9e$var$warn(`Cannot find x-teleport element for selector: "${expression}"`);
    let clone2 = el.content.cloneNode(true).firstElementChild;
    el._x_teleport = clone2;
    clone2._x_teleportBack = el;
    if (el._x_forwardEvents) el._x_forwardEvents.forEach((eventName)=>{
        clone2.addEventListener(eventName, (e)=>{
            e.stopPropagation();
            el.dispatchEvent(new e.constructor(e.type, e));
        });
    });
    $1df07f6efe5c7d9e$var$addScopeToNode(clone2, {}, el);
    $1df07f6efe5c7d9e$var$mutateDom(()=>{
        target.appendChild(clone2);
        $1df07f6efe5c7d9e$var$initTree(clone2);
        clone2._x_ignore = true;
    });
    cleanup2(()=>clone2.remove()
    );
});
// packages/alpinejs/src/directives/x-ignore.js
var $1df07f6efe5c7d9e$var$handler = ()=>{};
$1df07f6efe5c7d9e$var$handler.inline = (el, { modifiers: modifiers  }, { cleanup: cleanup2  })=>{
    modifiers.includes("self") ? el._x_ignoreSelf = true : el._x_ignore = true;
    cleanup2(()=>{
        modifiers.includes("self") ? delete el._x_ignoreSelf : delete el._x_ignore;
    });
};
$1df07f6efe5c7d9e$var$directive("ignore", $1df07f6efe5c7d9e$var$handler);
// packages/alpinejs/src/directives/x-effect.js
$1df07f6efe5c7d9e$var$directive("effect", (el, { expression: expression  }, { effect: effect3  })=>effect3($1df07f6efe5c7d9e$var$evaluateLater(el, expression))
);
// packages/alpinejs/src/utils/on.js
function $1df07f6efe5c7d9e$var$on(el, event, modifiers, callback) {
    let listenerTarget = el;
    let handler3 = (e)=>callback(e)
    ;
    let options = {};
    let wrapHandler = (callback2, wrapper)=>(e)=>wrapper(callback2, e)
    ;
    if (modifiers.includes("dot")) event = $1df07f6efe5c7d9e$var$dotSyntax(event);
    if (modifiers.includes("camel")) event = $1df07f6efe5c7d9e$var$camelCase2(event);
    if (modifiers.includes("passive")) options.passive = true;
    if (modifiers.includes("capture")) options.capture = true;
    if (modifiers.includes("window")) listenerTarget = window;
    if (modifiers.includes("document")) listenerTarget = document;
    if (modifiers.includes("prevent")) handler3 = wrapHandler(handler3, (next, e)=>{
        e.preventDefault();
        next(e);
    });
    if (modifiers.includes("stop")) handler3 = wrapHandler(handler3, (next, e)=>{
        e.stopPropagation();
        next(e);
    });
    if (modifiers.includes("self")) handler3 = wrapHandler(handler3, (next, e)=>{
        e.target === el && next(e);
    });
    if (modifiers.includes("away") || modifiers.includes("outside")) {
        listenerTarget = document;
        handler3 = wrapHandler(handler3, (next, e)=>{
            if (el.contains(e.target)) return;
            if (e.target.isConnected === false) return;
            if (el.offsetWidth < 1 && el.offsetHeight < 1) return;
            if (el._x_isShown === false) return;
            next(e);
        });
    }
    if (modifiers.includes("once")) handler3 = wrapHandler(handler3, (next, e)=>{
        next(e);
        listenerTarget.removeEventListener(event, handler3, options);
    });
    handler3 = wrapHandler(handler3, (next, e)=>{
        if ($1df07f6efe5c7d9e$var$isKeyEvent(event)) {
            if ($1df07f6efe5c7d9e$var$isListeningForASpecificKeyThatHasntBeenPressed(e, modifiers)) return;
        }
        next(e);
    });
    if (modifiers.includes("debounce")) {
        let nextModifier = modifiers[modifiers.indexOf("debounce") + 1] || "invalid-wait";
        let wait = $1df07f6efe5c7d9e$var$isNumeric(nextModifier.split("ms")[0]) ? Number(nextModifier.split("ms")[0]) : 250;
        handler3 = $1df07f6efe5c7d9e$var$debounce(handler3, wait);
    }
    if (modifiers.includes("throttle")) {
        let nextModifier = modifiers[modifiers.indexOf("throttle") + 1] || "invalid-wait";
        let wait = $1df07f6efe5c7d9e$var$isNumeric(nextModifier.split("ms")[0]) ? Number(nextModifier.split("ms")[0]) : 250;
        handler3 = $1df07f6efe5c7d9e$var$throttle(handler3, wait);
    }
    listenerTarget.addEventListener(event, handler3, options);
    return ()=>{
        listenerTarget.removeEventListener(event, handler3, options);
    };
}
function $1df07f6efe5c7d9e$var$dotSyntax(subject) {
    return subject.replace(/-/g, ".");
}
function $1df07f6efe5c7d9e$var$camelCase2(subject) {
    return subject.toLowerCase().replace(/-(\w)/g, (match, char)=>char.toUpperCase()
    );
}
function $1df07f6efe5c7d9e$var$isNumeric(subject) {
    return !Array.isArray(subject) && !isNaN(subject);
}
function $1df07f6efe5c7d9e$var$kebabCase2(subject) {
    return subject.replace(/([a-z])([A-Z])/g, "$1-$2").replace(/[_\s]/, "-").toLowerCase();
}
function $1df07f6efe5c7d9e$var$isKeyEvent(event) {
    return [
        "keydown",
        "keyup"
    ].includes(event);
}
function $1df07f6efe5c7d9e$var$isListeningForASpecificKeyThatHasntBeenPressed(e, modifiers) {
    let keyModifiers = modifiers.filter((i)=>{
        return ![
            "window",
            "document",
            "prevent",
            "stop",
            "once"
        ].includes(i);
    });
    if (keyModifiers.includes("debounce")) {
        let debounceIndex = keyModifiers.indexOf("debounce");
        keyModifiers.splice(debounceIndex, $1df07f6efe5c7d9e$var$isNumeric((keyModifiers[debounceIndex + 1] || "invalid-wait").split("ms")[0]) ? 2 : 1);
    }
    if (keyModifiers.length === 0) return false;
    if (keyModifiers.length === 1 && $1df07f6efe5c7d9e$var$keyToModifiers(e.key).includes(keyModifiers[0])) return false;
    const systemKeyModifiers = [
        "ctrl",
        "shift",
        "alt",
        "meta",
        "cmd",
        "super"
    ];
    const selectedSystemKeyModifiers = systemKeyModifiers.filter((modifier)=>keyModifiers.includes(modifier)
    );
    keyModifiers = keyModifiers.filter((i)=>!selectedSystemKeyModifiers.includes(i)
    );
    if (selectedSystemKeyModifiers.length > 0) {
        const activelyPressedKeyModifiers = selectedSystemKeyModifiers.filter((modifier)=>{
            if (modifier === "cmd" || modifier === "super") modifier = "meta";
            return e[`${modifier}Key`];
        });
        if (activelyPressedKeyModifiers.length === selectedSystemKeyModifiers.length) {
            if ($1df07f6efe5c7d9e$var$keyToModifiers(e.key).includes(keyModifiers[0])) return false;
        }
    }
    return true;
}
function $1df07f6efe5c7d9e$var$keyToModifiers(key) {
    if (!key) return [];
    key = $1df07f6efe5c7d9e$var$kebabCase2(key);
    let modifierToKeyMap = {
        ctrl: "control",
        slash: "/",
        space: "-",
        spacebar: "-",
        cmd: "meta",
        esc: "escape",
        up: "arrow-up",
        down: "arrow-down",
        left: "arrow-left",
        right: "arrow-right",
        period: ".",
        equal: "="
    };
    modifierToKeyMap[key] = key;
    return Object.keys(modifierToKeyMap).map((modifier)=>{
        if (modifierToKeyMap[modifier] === key) return modifier;
    }).filter((modifier)=>modifier
    );
}
// packages/alpinejs/src/directives/x-model.js
$1df07f6efe5c7d9e$var$directive("model", (el, { modifiers: modifiers , expression: expression  }, { effect: effect3 , cleanup: cleanup2  })=>{
    let evaluate2 = $1df07f6efe5c7d9e$var$evaluateLater(el, expression);
    let assignmentExpression = `${expression} = rightSideOfExpression($event, ${expression})`;
    let evaluateAssignment = $1df07f6efe5c7d9e$var$evaluateLater(el, assignmentExpression);
    var event = el.tagName.toLowerCase() === "select" || [
        "checkbox",
        "radio"
    ].includes(el.type) || modifiers.includes("lazy") ? "change" : "input";
    let assigmentFunction = $1df07f6efe5c7d9e$var$generateAssignmentFunction(el, modifiers, expression);
    let removeListener = $1df07f6efe5c7d9e$var$on(el, event, modifiers, (e)=>{
        evaluateAssignment(()=>{}, {
            scope: {
                $event: e,
                rightSideOfExpression: assigmentFunction
            }
        });
    });
    if (!el._x_removeModelListeners) el._x_removeModelListeners = {};
    el._x_removeModelListeners["default"] = removeListener;
    cleanup2(()=>el._x_removeModelListeners["default"]()
    );
    let evaluateSetModel = $1df07f6efe5c7d9e$var$evaluateLater(el, `${expression} = __placeholder`);
    el._x_model = {
        get () {
            let result;
            evaluate2((value)=>result = value
            );
            return result;
        },
        set (value) {
            evaluateSetModel(()=>{}, {
                scope: {
                    __placeholder: value
                }
            });
        }
    };
    el._x_forceModelUpdate = ()=>{
        evaluate2((value)=>{
            if (value === void 0 && expression.match(/\./)) value = "";
            window.fromModel = true;
            $1df07f6efe5c7d9e$var$mutateDom(()=>$1df07f6efe5c7d9e$var$bind(el, "value", value)
            );
            delete window.fromModel;
        });
    };
    effect3(()=>{
        if (modifiers.includes("unintrusive") && document.activeElement.isSameNode(el)) return;
        el._x_forceModelUpdate();
    });
});
function $1df07f6efe5c7d9e$var$generateAssignmentFunction(el, modifiers, expression) {
    if (el.type === "radio") $1df07f6efe5c7d9e$var$mutateDom(()=>{
        if (!el.hasAttribute("name")) el.setAttribute("name", expression);
    });
    return (event, currentValue)=>{
        return $1df07f6efe5c7d9e$var$mutateDom(()=>{
            if (event instanceof CustomEvent && event.detail !== void 0) return event.detail || event.target.value;
            else if (el.type === "checkbox") {
                if (Array.isArray(currentValue)) {
                    let newValue = modifiers.includes("number") ? $1df07f6efe5c7d9e$var$safeParseNumber(event.target.value) : event.target.value;
                    return event.target.checked ? currentValue.concat([
                        newValue
                    ]) : currentValue.filter((el2)=>!$1df07f6efe5c7d9e$var$checkedAttrLooseCompare2(el2, newValue)
                    );
                } else return event.target.checked;
            } else if (el.tagName.toLowerCase() === "select" && el.multiple) return modifiers.includes("number") ? Array.from(event.target.selectedOptions).map((option)=>{
                let rawValue = option.value || option.text;
                return $1df07f6efe5c7d9e$var$safeParseNumber(rawValue);
            }) : Array.from(event.target.selectedOptions).map((option)=>{
                return option.value || option.text;
            });
            else {
                let rawValue = event.target.value;
                return modifiers.includes("number") ? $1df07f6efe5c7d9e$var$safeParseNumber(rawValue) : modifiers.includes("trim") ? rawValue.trim() : rawValue;
            }
        });
    };
}
function $1df07f6efe5c7d9e$var$safeParseNumber(rawValue) {
    let number = rawValue ? parseFloat(rawValue) : null;
    return $1df07f6efe5c7d9e$var$isNumeric2(number) ? number : rawValue;
}
function $1df07f6efe5c7d9e$var$checkedAttrLooseCompare2(valueA, valueB) {
    return valueA == valueB;
}
function $1df07f6efe5c7d9e$var$isNumeric2(subject) {
    return !Array.isArray(subject) && !isNaN(subject);
}
// packages/alpinejs/src/directives/x-cloak.js
$1df07f6efe5c7d9e$var$directive("cloak", (el)=>queueMicrotask(()=>$1df07f6efe5c7d9e$var$mutateDom(()=>el.removeAttribute($1df07f6efe5c7d9e$var$prefix("cloak"))
        )
    )
);
// packages/alpinejs/src/directives/x-init.js
$1df07f6efe5c7d9e$var$addInitSelector(()=>`[${$1df07f6efe5c7d9e$var$prefix("init")}]`
);
$1df07f6efe5c7d9e$var$directive("init", $1df07f6efe5c7d9e$var$skipDuringClone((el, { expression: expression  }, { evaluate: evaluate2  })=>{
    if (typeof expression === "string") return !!expression.trim() && evaluate2(expression, {}, false);
    return evaluate2(expression, {}, false);
}));
// packages/alpinejs/src/directives/x-text.js
$1df07f6efe5c7d9e$var$directive("text", (el, { expression: expression  }, { effect: effect3 , evaluateLater: evaluateLater2  })=>{
    let evaluate2 = evaluateLater2(expression);
    effect3(()=>{
        evaluate2((value)=>{
            $1df07f6efe5c7d9e$var$mutateDom(()=>{
                el.textContent = value;
            });
        });
    });
});
// packages/alpinejs/src/directives/x-html.js
$1df07f6efe5c7d9e$var$directive("html", (el, { expression: expression  }, { effect: effect3 , evaluateLater: evaluateLater2  })=>{
    let evaluate2 = evaluateLater2(expression);
    effect3(()=>{
        evaluate2((value)=>{
            $1df07f6efe5c7d9e$var$mutateDom(()=>{
                el.innerHTML = value;
                el._x_ignoreSelf = true;
                $1df07f6efe5c7d9e$var$initTree(el);
                delete el._x_ignoreSelf;
            });
        });
    });
});
// packages/alpinejs/src/directives/x-bind.js
$1df07f6efe5c7d9e$var$mapAttributes($1df07f6efe5c7d9e$var$startingWith(":", $1df07f6efe5c7d9e$var$into($1df07f6efe5c7d9e$var$prefix("bind:"))));
$1df07f6efe5c7d9e$var$directive("bind", (el, { value: value , modifiers: modifiers , expression: expression , original: original  }, { effect: effect3  })=>{
    if (!value) return $1df07f6efe5c7d9e$var$applyBindingsObject(el, expression, original, effect3);
    if (value === "key") return $1df07f6efe5c7d9e$var$storeKeyForXFor(el, expression);
    let evaluate2 = $1df07f6efe5c7d9e$var$evaluateLater(el, expression);
    effect3(()=>evaluate2((result)=>{
            if (result === void 0 && expression.match(/\./)) result = "";
            $1df07f6efe5c7d9e$var$mutateDom(()=>$1df07f6efe5c7d9e$var$bind(el, value, result, modifiers)
            );
        })
    );
});
function $1df07f6efe5c7d9e$var$applyBindingsObject(el, expression, original, effect3) {
    let bindingProviders = {};
    $1df07f6efe5c7d9e$var$injectBindingProviders(bindingProviders);
    let getBindings = $1df07f6efe5c7d9e$var$evaluateLater(el, expression);
    let cleanupRunners = [];
    while(cleanupRunners.length)cleanupRunners.pop()();
    getBindings((bindings)=>{
        let attributes = Object.entries(bindings).map(([name, value])=>({
                name: name,
                value: value
            })
        );
        let staticAttributes = $1df07f6efe5c7d9e$var$attributesOnly(attributes);
        attributes = attributes.map((attribute)=>{
            if (staticAttributes.find((attr)=>attr.name === attribute.name
            )) return {
                name: `x-bind:${attribute.name}`,
                value: `"${attribute.value}"`
            };
            return attribute;
        });
        $1df07f6efe5c7d9e$var$directives(el, attributes, original).map((handle)=>{
            cleanupRunners.push(handle.runCleanups);
            handle();
        });
    }, {
        scope: bindingProviders
    });
}
function $1df07f6efe5c7d9e$var$storeKeyForXFor(el, expression) {
    el._x_keyExpression = expression;
}
// packages/alpinejs/src/directives/x-data.js
$1df07f6efe5c7d9e$var$addRootSelector(()=>`[${$1df07f6efe5c7d9e$var$prefix("data")}]`
);
$1df07f6efe5c7d9e$var$directive("data", $1df07f6efe5c7d9e$var$skipDuringClone((el, { expression: expression  }, { cleanup: cleanup2  })=>{
    expression = expression === "" ? "{}" : expression;
    let magicContext = {};
    $1df07f6efe5c7d9e$var$injectMagics(magicContext, el);
    let dataProviderContext = {};
    $1df07f6efe5c7d9e$var$injectDataProviders(dataProviderContext, magicContext);
    let data2 = $1df07f6efe5c7d9e$var$evaluate(el, expression, {
        scope: dataProviderContext
    });
    if (data2 === void 0) data2 = {};
    $1df07f6efe5c7d9e$var$injectMagics(data2, el);
    let reactiveData = $1df07f6efe5c7d9e$var$reactive(data2);
    $1df07f6efe5c7d9e$var$initInterceptors(reactiveData);
    let undo = $1df07f6efe5c7d9e$var$addScopeToNode(el, reactiveData);
    reactiveData["init"] && $1df07f6efe5c7d9e$var$evaluate(el, reactiveData["init"]);
    cleanup2(()=>{
        reactiveData["destroy"] && $1df07f6efe5c7d9e$var$evaluate(el, reactiveData["destroy"]);
        undo();
    });
}));
// packages/alpinejs/src/directives/x-show.js
$1df07f6efe5c7d9e$var$directive("show", (el, { modifiers: modifiers , expression: expression  }, { effect: effect3  })=>{
    let evaluate2 = $1df07f6efe5c7d9e$var$evaluateLater(el, expression);
    let hide = ()=>$1df07f6efe5c7d9e$var$mutateDom(()=>{
            el.style.display = "none";
            el._x_isShown = false;
        })
    ;
    let show = ()=>$1df07f6efe5c7d9e$var$mutateDom(()=>{
            if (el.style.length === 1 && el.style.display === "none") el.removeAttribute("style");
            else el.style.removeProperty("display");
            el._x_isShown = true;
        })
    ;
    let clickAwayCompatibleShow = ()=>setTimeout(show)
    ;
    let toggle = $1df07f6efe5c7d9e$var$once((value)=>value ? show() : hide()
    , (value)=>{
        if (typeof el._x_toggleAndCascadeWithTransitions === "function") el._x_toggleAndCascadeWithTransitions(el, value, show, hide);
        else value ? clickAwayCompatibleShow() : hide();
    });
    let oldValue;
    let firstTime = true;
    effect3(()=>evaluate2((value)=>{
            if (!firstTime && value === oldValue) return;
            if (modifiers.includes("immediate")) value ? clickAwayCompatibleShow() : hide();
            toggle(value);
            oldValue = value;
            firstTime = false;
        })
    );
});
// packages/alpinejs/src/directives/x-for.js
$1df07f6efe5c7d9e$var$directive("for", (el, { expression: expression  }, { effect: effect3 , cleanup: cleanup2  })=>{
    let iteratorNames = $1df07f6efe5c7d9e$var$parseForExpression(expression);
    let evaluateItems = $1df07f6efe5c7d9e$var$evaluateLater(el, iteratorNames.items);
    let evaluateKey = $1df07f6efe5c7d9e$var$evaluateLater(el, el._x_keyExpression || "index");
    el._x_prevKeys = [];
    el._x_lookup = {};
    effect3(()=>$1df07f6efe5c7d9e$var$loop(el, iteratorNames, evaluateItems, evaluateKey)
    );
    cleanup2(()=>{
        Object.values(el._x_lookup).forEach((el2)=>el2.remove()
        );
        delete el._x_prevKeys;
        delete el._x_lookup;
    });
});
function $1df07f6efe5c7d9e$var$loop(el, iteratorNames, evaluateItems, evaluateKey) {
    let isObject2 = (i)=>typeof i === "object" && !Array.isArray(i)
    ;
    let templateEl = el;
    evaluateItems((items)=>{
        if ($1df07f6efe5c7d9e$var$isNumeric3(items) && items >= 0) items = Array.from(Array(items).keys(), (i)=>i + 1
        );
        if (items === void 0) items = [];
        let lookup = el._x_lookup;
        let prevKeys = el._x_prevKeys;
        let scopes = [];
        let keys = [];
        if (isObject2(items)) items = Object.entries(items).map(([key, value])=>{
            let scope2 = $1df07f6efe5c7d9e$var$getIterationScopeVariables(iteratorNames, value, key, items);
            evaluateKey((value2)=>keys.push(value2)
            , {
                scope: {
                    index: key,
                    ...scope2
                }
            });
            scopes.push(scope2);
        });
        else for(let i8 = 0; i8 < items.length; i8++){
            let scope2 = $1df07f6efe5c7d9e$var$getIterationScopeVariables(iteratorNames, items[i8], i8, items);
            evaluateKey((value)=>keys.push(value)
            , {
                scope: {
                    index: i8,
                    ...scope2
                }
            });
            scopes.push(scope2);
        }
        let adds = [];
        let moves = [];
        let removes = [];
        let sames = [];
        for(let i2 = 0; i2 < prevKeys.length; i2++){
            let key = prevKeys[i2];
            if (keys.indexOf(key) === -1) removes.push(key);
        }
        prevKeys = prevKeys.filter((key)=>!removes.includes(key)
        );
        let lastKey = "template";
        for(let i3 = 0; i3 < keys.length; i3++){
            let key = keys[i3];
            let prevIndex = prevKeys.indexOf(key);
            if (prevIndex === -1) {
                prevKeys.splice(i3, 0, key);
                adds.push([
                    lastKey,
                    i3
                ]);
            } else if (prevIndex !== i3) {
                let keyInSpot = prevKeys.splice(i3, 1)[0];
                let keyForSpot = prevKeys.splice(prevIndex - 1, 1)[0];
                prevKeys.splice(i3, 0, keyForSpot);
                prevKeys.splice(prevIndex, 0, keyInSpot);
                moves.push([
                    keyInSpot,
                    keyForSpot
                ]);
            } else sames.push(key);
            lastKey = key;
        }
        for(let i4 = 0; i4 < removes.length; i4++){
            let key = removes[i4];
            if (!!lookup[key]._x_effects) lookup[key]._x_effects.forEach($1df07f6efe5c7d9e$var$dequeueJob);
            lookup[key].remove();
            lookup[key] = null;
            delete lookup[key];
        }
        for(let i5 = 0; i5 < moves.length; i5++){
            let [keyInSpot, keyForSpot] = moves[i5];
            let elInSpot = lookup[keyInSpot];
            let elForSpot = lookup[keyForSpot];
            let marker = document.createElement("div");
            $1df07f6efe5c7d9e$var$mutateDom(()=>{
                elForSpot.after(marker);
                elInSpot.after(elForSpot);
                elForSpot._x_currentIfEl && elForSpot.after(elForSpot._x_currentIfEl);
                marker.before(elInSpot);
                elInSpot._x_currentIfEl && elInSpot.after(elInSpot._x_currentIfEl);
                marker.remove();
            });
            $1df07f6efe5c7d9e$var$refreshScope(elForSpot, scopes[keys.indexOf(keyForSpot)]);
        }
        for(let i6 = 0; i6 < adds.length; i6++){
            let [lastKey2, index] = adds[i6];
            let lastEl = lastKey2 === "template" ? templateEl : lookup[lastKey2];
            if (lastEl._x_currentIfEl) lastEl = lastEl._x_currentIfEl;
            let scope2 = scopes[index];
            let key = keys[index];
            let clone2 = document.importNode(templateEl.content, true).firstElementChild;
            $1df07f6efe5c7d9e$var$addScopeToNode(clone2, $1df07f6efe5c7d9e$var$reactive(scope2), templateEl);
            $1df07f6efe5c7d9e$var$mutateDom(()=>{
                lastEl.after(clone2);
                $1df07f6efe5c7d9e$var$initTree(clone2);
            });
            if (typeof key === "object") $1df07f6efe5c7d9e$var$warn("x-for key cannot be an object, it must be a string or an integer", templateEl);
            lookup[key] = clone2;
        }
        for(let i7 = 0; i7 < sames.length; i7++)$1df07f6efe5c7d9e$var$refreshScope(lookup[sames[i7]], scopes[keys.indexOf(sames[i7])]);
        templateEl._x_prevKeys = keys;
    });
}
function $1df07f6efe5c7d9e$var$parseForExpression(expression) {
    let forIteratorRE = /,([^,\}\]]*)(?:,([^,\}\]]*))?$/;
    let stripParensRE = /^\s*\(|\)\s*$/g;
    let forAliasRE = /([\s\S]*?)\s+(?:in|of)\s+([\s\S]*)/;
    let inMatch = expression.match(forAliasRE);
    if (!inMatch) return;
    let res = {};
    res.items = inMatch[2].trim();
    let item = inMatch[1].replace(stripParensRE, "").trim();
    let iteratorMatch = item.match(forIteratorRE);
    if (iteratorMatch) {
        res.item = item.replace(forIteratorRE, "").trim();
        res.index = iteratorMatch[1].trim();
        if (iteratorMatch[2]) res.collection = iteratorMatch[2].trim();
    } else res.item = item;
    return res;
}
function $1df07f6efe5c7d9e$var$getIterationScopeVariables(iteratorNames, item, index, items) {
    let scopeVariables = {};
    if (/^\[.*\]$/.test(iteratorNames.item) && Array.isArray(item)) {
        let names = iteratorNames.item.replace("[", "").replace("]", "").split(",").map((i)=>i.trim()
        );
        names.forEach((name, i)=>{
            scopeVariables[name] = item[i];
        });
    } else if (/^\{.*\}$/.test(iteratorNames.item) && !Array.isArray(item) && typeof item === "object") {
        let names = iteratorNames.item.replace("{", "").replace("}", "").split(",").map((i)=>i.trim()
        );
        names.forEach((name)=>{
            scopeVariables[name] = item[name];
        });
    } else scopeVariables[iteratorNames.item] = item;
    if (iteratorNames.index) scopeVariables[iteratorNames.index] = index;
    if (iteratorNames.collection) scopeVariables[iteratorNames.collection] = items;
    return scopeVariables;
}
function $1df07f6efe5c7d9e$var$isNumeric3(subject) {
    return !Array.isArray(subject) && !isNaN(subject);
}
// packages/alpinejs/src/directives/x-ref.js
function $1df07f6efe5c7d9e$var$handler2() {}
$1df07f6efe5c7d9e$var$handler2.inline = (el, { expression: expression  }, { cleanup: cleanup2  })=>{
    let root = $1df07f6efe5c7d9e$var$closestRoot(el);
    if (!root._x_refs) root._x_refs = {};
    root._x_refs[expression] = el;
    cleanup2(()=>delete root._x_refs[expression]
    );
};
$1df07f6efe5c7d9e$var$directive("ref", $1df07f6efe5c7d9e$var$handler2);
// packages/alpinejs/src/directives/x-if.js
$1df07f6efe5c7d9e$var$directive("if", (el, { expression: expression  }, { effect: effect3 , cleanup: cleanup2  })=>{
    let evaluate2 = $1df07f6efe5c7d9e$var$evaluateLater(el, expression);
    let show = ()=>{
        if (el._x_currentIfEl) return el._x_currentIfEl;
        let clone2 = el.content.cloneNode(true).firstElementChild;
        $1df07f6efe5c7d9e$var$addScopeToNode(clone2, {}, el);
        $1df07f6efe5c7d9e$var$mutateDom(()=>{
            el.after(clone2);
            $1df07f6efe5c7d9e$var$initTree(clone2);
        });
        el._x_currentIfEl = clone2;
        el._x_undoIf = ()=>{
            $1df07f6efe5c7d9e$var$walk(clone2, (node)=>{
                if (!!node._x_effects) node._x_effects.forEach($1df07f6efe5c7d9e$var$dequeueJob);
            });
            clone2.remove();
            delete el._x_currentIfEl;
        };
        return clone2;
    };
    let hide = ()=>{
        if (!el._x_undoIf) return;
        el._x_undoIf();
        delete el._x_undoIf;
    };
    effect3(()=>evaluate2((value)=>{
            value ? show() : hide();
        })
    );
    cleanup2(()=>el._x_undoIf && el._x_undoIf()
    );
});
// packages/alpinejs/src/directives/x-id.js
$1df07f6efe5c7d9e$var$directive("id", (el, { expression: expression  }, { evaluate: evaluate2  })=>{
    let names = evaluate2(expression);
    names.forEach((name)=>$1df07f6efe5c7d9e$var$setIdRoot(el, name)
    );
});
// packages/alpinejs/src/directives/x-on.js
$1df07f6efe5c7d9e$var$mapAttributes($1df07f6efe5c7d9e$var$startingWith("@", $1df07f6efe5c7d9e$var$into($1df07f6efe5c7d9e$var$prefix("on:"))));
$1df07f6efe5c7d9e$var$directive("on", $1df07f6efe5c7d9e$var$skipDuringClone((el, { value: value , modifiers: modifiers , expression: expression  }, { cleanup: cleanup2  })=>{
    let evaluate2 = expression ? $1df07f6efe5c7d9e$var$evaluateLater(el, expression) : ()=>{};
    if (el.tagName.toLowerCase() === "template") {
        if (!el._x_forwardEvents) el._x_forwardEvents = [];
        if (!el._x_forwardEvents.includes(value)) el._x_forwardEvents.push(value);
    }
    let removeListener = $1df07f6efe5c7d9e$var$on(el, value, modifiers, (e)=>{
        evaluate2(()=>{}, {
            scope: {
                $event: e
            },
            params: [
                e
            ]
        });
    });
    cleanup2(()=>removeListener()
    );
}));
// packages/alpinejs/src/index.js
$1df07f6efe5c7d9e$var$alpine_default.setEvaluator($1df07f6efe5c7d9e$var$normalEvaluator);
$1df07f6efe5c7d9e$var$alpine_default.setReactivityEngine({
    reactive: $1df07f6efe5c7d9e$var$reactive2,
    effect: $1df07f6efe5c7d9e$var$effect2,
    release: $1df07f6efe5c7d9e$var$stop,
    raw: $1df07f6efe5c7d9e$var$toRaw
});
var $1df07f6efe5c7d9e$var$src_default = $1df07f6efe5c7d9e$var$alpine_default;
// packages/alpinejs/builds/module.js
var $1df07f6efe5c7d9e$export$2e2bcd8739ae039 = $1df07f6efe5c7d9e$var$src_default;


$1df07f6efe5c7d9e$export$2e2bcd8739ae039.directive('uppercase', (el)=>{
    el.textContent = el.textContent.toUpperCase();
});
window.Alpine = $1df07f6efe5c7d9e$export$2e2bcd8739ae039;
window.Alpine.start();
// import { library } from '@fortawesome/fontawesome-svg-core'
// import { faFacebookF } from '@fortawesome/free-brands-svg-icons'
// import { fas } from '@fortawesome/pro-solid-svg-icons'
// import { far } from '@fortawesome/pro-regular-svg-icons'
// library.add(fas, far, faFacebookF)
let $1e8f2cd2fdaa9e0c$var$url = new URL(window.location.href);
let $1e8f2cd2fdaa9e0c$var$success = $1e8f2cd2fdaa9e0c$var$url.searchParams.get("success");
if ($1e8f2cd2fdaa9e0c$var$success) document.getElementById('alert-redirect').classList.add('show');
 // const indicator = document.querySelector(".nav-indicator");
 // const items = document.querySelectorAll(".nav-item");
 // function handleIndicator(el) {
 //   items.forEach((item) => {
 //     item.classList.remove("is-active");
 //     item.removeAttribute("style");
 //   });
 //
 //   indicator.style.width = `${el.offsetWidth}px`;
 //   indicator.style.left = `${el.offsetLeft}px`;
 //   indicator.style.backgroundColor = el.getAttribute("active-color");
 //
 //   el.classList.add("is-active");
 //   el.style.color = el.getAttribute("active-color");
 // }
 //
 // items.forEach((item, index) => {
 //   item.addEventListener("click", (e) => {
 //     handleIndicator(e.target);
 //   });
 //   item.classList.contains("is-active") && handleIndicator(item);
 // });


