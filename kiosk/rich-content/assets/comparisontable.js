﻿import $ from "jquery";
import require from "require";
import { bindResizeEvent } from "./utils";

export default function (options) {
	if (!options.comparisonTable) {
		return;
	}

	const tableId = options.comparisonTable.tableId;
	const $table  = $(`#${tableId}`);

	(function onVisible() {
		if ($table.is(":visible")) {
			updateRowsHeight($table);
			fixImageWidth($table);
		} else {
			setTimeout(onVisible, 100);
		}
	}());

	require("logoProgram").detectFeatures(tableId);

	initCells($table, options.comparisonTable.comparisonTableImage);
}

function updateRowsHeight($table) {
	$table.find("tr").each(function () {
		const $row = $(this);
		const $titleCell = $row.find("td:first");

		const $img = $titleCell.find("img");

		if ($img.length > 0) {
			$img.on("load", function () {
				setRowHeight($titleCell, $row);
			}).each(function () {
				this.complete && $(this).trigger("load");
			});
		} else {
			setRowHeight($titleCell, $row);
		}
	});
}

function setRowHeight($titleCell, $row) {
	const paddingTop = parseInt($titleCell.css("paddingTop"));
	const paddingBottom = parseInt($titleCell.css("paddingBottom"));
	const cellHeight = $titleCell.height();

	const titleCellHeight = cellHeight + paddingTop + paddingBottom;

	$(`<td style="padding: 0; height: ${titleCellHeight}px;"></td>`).appendTo($row);
}

function initCells($table, altText) {
	$table
		.find(".ccs-cc-comparison-table-cell-content")
		.each(function (index) {
			const $cell = $(this);

			if ($cell.find("td div").has("img")) {
				var img = $cell.find("img");
				$(img).attr("alt", `${altText} ${index + 1}`);
			}

			const initScript = $cell.attr("data-cell-init-script");
			if (initScript && initScript.length) {
				new Function(initScript)();
			}
		});
}

// CCSDT-7756
function fixImageWidth($table) {
	let timer;

	function update() {
		if (!$table.is(":visible")) {
			clearTimeout(timer);
			timer = setTimeout(() => update(), 300);
			return;
		}

		const styles = $table[0].style;

		styles.setProperty("--table-img-width", "100%");

		const $imgCells = $table.find(".ccs-cc-comparison-table-header-row td:not(:first) div").has("img");
		const imgWidth = Math.min(...$imgCells.map((_, el) => el.offsetWidth).toArray());

		styles.setProperty("--table-img-width", imgWidth + "px");
	}

	setTimeout(() => update());
	bindResizeEvent(() => update());
}
