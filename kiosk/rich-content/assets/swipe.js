﻿// jQuery plugin
// https://github.com/marcandre/detect_swipe

import $ from "jquery";

$.detectSwipe = {
	version: "2.1.2",
	enabled: "ontouchstart" in document.documentElement,
	preventDefault: true,
	threshold: 100
};

let startX,
	startY,
	isMoving = false;

function onTouchEnd() {
	this.removeEventListener("touchmove", onTouchMove);
	this.removeEventListener("touchend", onTouchEnd);
	isMoving = false;
}

function onTouchMove(e) {
	if ($.detectSwipe.preventDefault) {
		e.preventDefault();
		e.stopImmediatePropagation();
	}
	if (isMoving) {
		const x = e.touches[0].pageX;
		const y = e.touches[0].pageY;
		const dx = startX - x;
		const dy = startY - y;
		let dir;
		if (Math.abs(dx) >= $.detectSwipe.threshold) {
			dir = dx > 0 ? "left" : "right";
		}
		else if (Math.abs(dy) >= $.detectSwipe.threshold) {
			dir = dy > 0 ? "up" : "down";
		}
		if (dir) {
			onTouchEnd.call(this);
			$(this).trigger("swipe", dir).trigger("swipe" + dir);
		}
	}
}

function onTouchStart(e) {
	if (e.touches.length === 1) {
		startX = e.touches[0].pageX;
		startY = e.touches[0].pageY;
		isMoving = true;
		this.addEventListener("touchmove", onTouchMove, false);
		this.addEventListener("touchend", onTouchEnd, false);
	}
}

function setup() {
	this.addEventListener && this.addEventListener("touchstart", onTouchStart, false);
}

function teardown() {
	this.removeEventListener("touchstart", onTouchStart);
}

$.event.special.swipe = {
	setup: setup,
	teardown: teardown
};

$.each(["left", "up", "down", "right"], function () {
	$.event.special["swipe" + this] = {
		setup: function () {
			$(this).on("swipe", $.noop);
		}
	};
});
