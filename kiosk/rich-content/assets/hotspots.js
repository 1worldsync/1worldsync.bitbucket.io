﻿import $ from "jquery";
import require from "require";
import * as utils from "./utils";
import { init as initVideo } from "./video";

const c = {
	rootClass: "ccs-hotspots",
	mobileClass: "ccs-hotspots-mobile",
	noScrollClass: "ccs-noscroll",
	pointClass: "ccs-hotspots-point",
	pointActiveClass: "ccs-hotspots-point-active",
	pointSize: 32,
	tooltipTheme: "qtip-light qtip-shadow",
	tooltipsContainerClass: "ccs-hotspots-tooltips",
	tooltipClass: "ccs-hotspots-tooltip",
	videoPlayerClass: "ccs-hotspots-video",

	productTooltipClass: "ccs-hotspots-tooltip-product",
	productPriceClass: "ccs-hotspots-product-price",
	productCartClass: "ccs-hotspots-product-cart",
	customCartClass: "ccs-custom-cart",
	loadingClass: "ccs-loading"
};

const defaultOptions = {
	event: "click",
	size: "small",
	iconSet: "ccs-hotspots-default",
	video: {
		autoplay: true
	}
};

export default function (ep, options) {
	const $images = ep.find(".ccs-hotspots-image");

	if (!$images.length || !options.hotspots) {
		return;
	}

	require("qtip");

	$.fn.qtip.zindex = $.fn.qtip.modal_zindex = 100000; // fancybox

	const $body = $("body");
	let $container = $body.find(`.${c.tooltipsContainerClass}`);
	if ($container.length === 0) {
		$container = $("<div>")
			.addClass(c.tooltipsContainerClass)
			.addClass(options.containerClass)
			.appendTo($body);
	}

	const o = buildOptions(options, $container);

	$images.each((i, img) => initMainImage(img, o));
}

function buildOptions(options, $container) {
	const o = $.extend(true, {}, defaultOptions, options.hotspots);

	o.video = options.video;
	o.logCall = options.logCall;
	o.$container = $container;

	o.isMobile = utils.isMobile() && !options.lightbox.disabled;

	if (o.isMobile) {
		o.event = "click";
	}

	return o;
}

function initMainImage(img, o) {
	const $image = $(img);

	const hotspots = $image.data("hotspots");
	if (hotspots.length === 0) {
		console.warn("No hotspots data was found");
		return;
	}

	$image
		.removeAttr("data-hotspots")
		.wrap($("<div>")
			.addClass(c.rootClass)
			.addClass(o.iconSet));

	if (o.isMobile) {
		initMobile($image, o.$container, hotspots);
	}

	$image.one("load", () => {
		hotspots.forEach(hotspot => initHotspot($image, hotspot, o));
		$image.trigger("hotspotReady");
	});

	if ($image[0].complete) {
		$image.trigger("load");
	}
}

function initMobile($img, $container, hotspots) {
	$container.addClass(c.mobileClass);
	$img.parent().addClass(c.mobileClass);

	for (let i = 0; i < hotspots.length; i++) {
		const hotspot = hotspots[i];

		if (hotspot.type === "video" && hotspot.mediaUrl.match(/\.flv$/)) {
			hotspot.type = "image";
			hotspot.mediaUrl = hotspot.previewUrl;
		}
	}
}

function initHotspot($img, hotspot, options) {
	const o = options;

	const position = getPosition(hotspot.position, o.isMobile);
	const isProduct = !!hotspot.product;

	const closeButtonBlockTime = o.event === "click" ? 0 : 500;

	const $closeButton = o.isMobile
		? $("<a class='qtip-close qtip-icon' title='Close' aria-label='Close' role='button'>" +
			"<svg viewBox='0 0 40 40'>" +
			"<path d='M10,10 L30,30 M30,10 L10,30'/>" +
			"</svg>" +
			"</a>")
		: "Close";

	let timer, openTime;

	$("<div>")
		.addClass(c.pointClass)
		.addClass(c.pointClass + "-" + hotspot.type)
		.toggleClass(c.productTooltipClass, isProduct)
		.attr("tabindex", 0)
		.css("top", `calc(${hotspot.y * 100}% - ${c.pointSize / 2}px)`)
		.css("left", `calc(${hotspot.x * 100}% - ${c.pointSize / 2}px)`)
		.insertBefore($img)
		.qtip({
			prerender: true,
			content: {
				text: buildContent(hotspot, o),
				button: o.event === "click" ? $closeButton : false
			},
			show: {
				event: o.event === "click" ? "click" : "mouseenter",
				modal: o.isMobile
			},
			hide: {
				fixed: true,
				delay: o.event === "click" ? 0 : 300,
				event: o.isMobile
					? false
					: o.event === "click" ? "unfocus" : "mouseleave"
			},
			style: {
				classes: c.tooltipTheme + " " + c.tooltipClass
			},
			position: {
				my: position.my,
				at: position.at,
				effect: false,
				container: o.$container,
				viewport: $(window),
				adjust: {
					method: position.method
				}
			},
			events: {
				render(event, api) {
					api.tooltip
						.addClass(c.tooltipClass + "-" + hotspot.type)
						.addClass(c.tooltipClass + "-" + (hotspot.size || o.size))
						.toggleClass(c.productTooltipClass, isProduct)
						.attr("tabindex", 0)
						.on("keydown", e => {
							if (e.keyCode === 27) {
								api.hide();
								api.elements.target.focus();
								e.stopPropagation();
							}
						});

					api.hotspot = hotspot;

					if (o.isMobile) {
						renderMobile(api);
					}

					renderVideo(api, o);
					renderProduct(api, o);
				},
				show(event, api) {
					if (api.tooltip.is(":visible")) {
						return;
					}

					const point = api.elements.target;

					point.addClass(c.pointActiveClass).blur();

					if (o.isMobile) {
						showMobile(api);
					}

					timer = setTimeout(() => {
						point.off("click.show")
							.one("click.show", () => api.hide());
					}, closeButtonBlockTime);

					openTime = new Date();

					dispatchPopupEvent(o.logCall, $img, api.hotspot, "popup-open");
				},
				visible(event, api) {
					api.tooltip.focus();
				},
				hide(event, api) {
					api.elements.target
						.removeClass(c.pointActiveClass)
						.off("click.show")
						.one("click.show", () => api.show());

					if (o.isMobile) {
						hideMobile();
					}

					clearTimeout(timer);

					const qty = Math.round((new Date() - openTime) / 1000);
					dispatchPopupEvent(o.logCall, $img, api.hotspot, "popup-close", qty);
				}
			}
		})
		.keyup(function (event) {
			if (event.keyCode === 13) {
				$(this).qtip("show");

			}
			if (event.keyCode === 27) {
				$(this).qtip("hide");
			}
		});
}

function getPosition(position, isMobile) {

	if (isMobile) {
		return {
			my: "center",
			at: "center",
			method: "none"
		};
	}

	switch (position) {

		case "left":
			return {
				my: "center right",
				at: "center left",
				method: "flip shift"
			};

		case "right":
			return {
				my: "center left",
				at: "center right",
				method: "flip shift"
			};

		case "bottom":
			return {
				my: "top center",
				at: "bottom center",
				method: "shift flip"
			};

		default:
			return {
				my: "bottom center",
				at: "top center",
				method: "shift flip"
			};
	}
}

function dispatchPopupEvent(logCall, $img, hotspot, type, qty) {

	const actionContext = {
		type: type,
		hotspot: {
			mainImage: $img.attr("src"),
			type: hotspot.type,
			x: +hotspot.x.toFixed(4),
			y: +hotspot.y.toFixed(4)
		}
	};

	const clientParams = utils.makeClientParams($img, "feature-click", actionContext);

	if (qty !== undefined) {
		clientParams["Qty"] = qty;
	}

	ccs_cc_log.dispatchEvent(logCall, clientParams);
}

function buildContent(data, o) {
	let $wrap = $("<div>");

	$wrap.append(buildMediaContent(data));

	if (data.title) {
		$wrap.append($("<h4>").html(data.title));
	}

	if (data.description) {
		$wrap.append($("<div>", { html: data.description }));
	}

	if (data.product) {
		$wrap = buildProductContent($wrap, data, o);
	}

	return $wrap;
}

function buildMediaContent(data) {
	if (!data.mediaUrl) {
		return false;
	}

	return data.type === "video"
		? $("<div>").addClass(c.videoPlayerClass)
		: $("<img>", { src: data.mediaUrl, alt: data.altText });
}

function buildProductContent($wrap, data, o) {
	const crossSell = o.crossSell;
	const price = data.product.price;

	const hideLinkWithPrice = crossSell.showLinkWithPrice && (crossSell.parsingJs || !price || !price.value);

	if (!hideLinkWithPrice) {
		wrapIntoProductLink($wrap, data, o);
	}

	return $wrap
		.append(buildProductPrice(data, o))
		.append(buildProductAddToCart(data, o));
}

function wrapIntoProductLink($content, data, o) {
	if (!data.product.url || !$content.is(":parent")) {
		return;
	}

	const $eventTargets = $content.find("img, h4");

	const $span = $("<span>")
		.attr("data-logo-productLink", true)
		.data("log-event", o.logCall)
		.on("click", dispatchConversionEvent);

	const $link = $("<a>")
		.attr("href", data.product.url)
		.attr("target", o.crossSell.target || "_blank");

	$eventTargets.wrap($span).wrap($link);
}

function buildProductPrice(data, o) {
	const price = data.product.price;

	if (!price && !o.crossSell.priceFallbackText) {
		return null;
	}

	const $block = $("<div>").addClass(c.productPriceClass);

	if (!o.crossSell.parsingJs) {
		$block.text(price.value || o.crossSell.priceFallbackText);
	}

	return $block;
}

function buildProductAddToCart(data, o) {
	const addToCart = data.product.addToCart;
	if (!addToCart) {
		return null;
	}

	const crossSell = o.crossSell;
	const price = data.product.price;

	const hideAtcOnInit = crossSell.showAddToCartWithPrice && (crossSell.parsingJs || !price || !price.value);

	return $("<div>")
		.addClass(c.productCartClass)
		.toggleClass(c.customCartClass, addToCart.isCustom === true)
		.html(addToCart.html)
		.toggle(!hideAtcOnInit)
		.children()
		.data("log-event", o.logCall)
		.on("click", dispatchConversionEvent)
		.parent();
}

function dispatchConversionEvent(e) {

	const $el = $(e.currentTarget);
	const qtip = $el.closest(".qtip").data("qtip");

	let context = qtip.hotspot.product.logData;

	if ($el.data("logo-addtocart")) {
		context = JSON.parse(context);
		context.type = "addtocart";
	}

	utils.dispatchEvent(
		$el.data("log-event"),
		qtip.target,
		"conversion",
		context);
}

function renderVideo(api, o) {
	const hotspot = api.hotspot;
	if (hotspot.type !== "video") {
		return;
	}

	api.tooltip
		.toggleClass(c.tooltipClass + "-video-only", !hotspot.title && !hotspot.description)
		.one("tooltipshow", () => {
			const $content = api.elements.content;
			const $player = $content.find(`.${c.videoPlayerClass}`);
			const isMobile = o.isMobile;

			const settings = {
				file: api.hotspot.mediaUrl,
				transcodedFile: api.hotspot.transcodedUrl,
				ccUrl: api.hotspot.ccUrl,
				ccLang: api.hotspot.ccLang,
				height: isMobile ? "100%" : $content.height(),
				width: isMobile ? "100%" : $content.width(),
				playOnInit: o.autoplayVideo,
				pluginPath: o.video.pluginPath,
				disableCC: o.video.disableCC,
				tracking: api.target
			};

			const player = api.player = initVideo($player, o.logCall, settings);

			api.tooltip
				.on("tooltipvisible", () => o.autoplayVideo && player.play())
				.on("tooltiphide", () => player.pause());
		});
}

function renderProduct(api, o) {
	const hotspot = api.hotspot;
	if (!hotspot.product) {
		return;
	}

	const $tooltip = api.tooltip;

	const price = hotspot.product.price;
	const apiUrl = price && price.apiUrl;

	const crossSell = o.crossSell;
	const parsingJs = crossSell && crossSell.parsingJs;

	if (apiUrl && parsingJs) {
		$tooltip.one("tooltipshow", () => {
			const $block = $tooltip
				.find(`.${c.productPriceClass}`)
				.addClass(c.loadingClass);

			CcsLogoProgram.requestPrice(
				response => {
					if (response.result) {
						$block.text(response.result);

						$tooltip.find(`.${c.productCartClass}`).show();

						if (crossSell.showLinkWithPrice) {
							wrapIntoProductLink($tooltip, hotspot, o);
						}

					} else {
						$block.text(crossSell.priceFallbackText);
					}

					$block.removeClass(c.loadingClass);
					$tooltip.qtip("reposition");
				},
				apiUrl,
				parsingJs,
				o.logCall,
				utils.makeClientParams(api.target, "error", null, "Submit"));
		});
	}
}

function renderMobile(api) {
	window
		.matchMedia("(orientation: portrait)")
		.addListener(() => {
			if (api.tooltip.is(":visible")) {
				if (api.hotspot.type === "video" && api.player) {
					const player = api.player.getPlayer();
					player.mejs.exitFullScreen();
				}

				api.hide();
			}
		});
}

function hideMobile() {
	$("body").removeClass(c.noScrollClass);
}

function showMobile(api) {
	$("body").addClass(c.noScrollClass);

	const screenSize = getScreenSize();

	api.set("position.target", getScreenCenter(screenSize));

	if (api.hotspot.type === "video") {
		setMobileVideoSize(api, screenSize);
	} else {
		setMobileDefaultSize(api, screenSize);
	}
}

function setMobileDefaultSize(api, s) {
	const $content = $(api.elements.content);
	const closeButton = 36;

	$content
		.add($content.find("img"))
		.css({
			"max-width": s.innerWidth - closeButton / 2 + "px",
			"max-height": s.innerHeight - closeButton / 2 + "px"
		});
}

function setMobileVideoSize(api, s) {
	const $content = $(api.elements.content);
	const h = Math.min((s.width - s.margin) / (16 / 9), s.innerHeight);

	$content.css({
		"max-width": s.innerWidth + "px",
		"max-height": s.innerHeight + "px"
	});

	$content.find(`.${c.videoPlayerClass}`)
		.css({
			height: h + "px",
			width: s.innerWidth + "px"
		});

	if (api.player) {
		api.player.mejs.resetSize();
	}
}

function getScreenSize() {
	const width = window.innerWidth
		? window.innerWidth
		: document.documentElement.clientWidth
			? document.documentElement.clientWidth
			: screen.width;

	const height = window.innerHeight
		? window.innerHeight
		: document.documentElement.clientHeight
			? document.documentElement.clientHeight
			: screen.height;

	const margin = Math.round(Math.min(width, height) * 0.2);

	return {
		width: width,
		height: height,
		margin: margin,
		innerWidth: width - margin,
		innerHeight: height - margin
	};
}

function getScreenCenter(screen) {
	const $w = $(window);
	return [
		screen.width / 2 + $w.scrollLeft(),
		screen.height / 2 + $w.scrollTop()
	];
}
