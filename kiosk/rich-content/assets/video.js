﻿import $ from "jquery";
import common from "./common";
import * as utils from "./utils";

import "videoPlayer";

const c = {
	cartCookie: "ccs_video_cart",
	playerSelector: ".mejs__container",
	buyRootSelector: ".ccs-video-buy",
	buyVisibleClass: "ccs-video-buy-visible",
	cartButtonSuffix: "cart",
	closeButtonSuffix: "close"
};

$.extend(c, common);

export function init(element, logCall, settings, reinit) {
	const $wrapper = $(element);

	const player = $wrapper.ccsVideoPlayer("getPlayer");
	if (player && !reinit) {
		return player;
	}

	if (settings.tracking instanceof Element || settings.tracking instanceof $) {
		settings.tracking = getTrackingParams(settings.tracking, logCall);
	}

	if (!settings.tracking) {
		settings.tracking = getTrackingParams($wrapper, logCall);
	}

	return $wrapper.ccsVideoPlayer(settings);
}

export function initInline(element, logCall, pluginPath, disableCC, mute, playOnInit, initByAutoplay) {
	const $wrapper = $(element);

	const playerId = $wrapper.data("player");

	const settings = {
		playerId: playerId,
		file: $wrapper.data("video-url"),
		transcodedFile: $wrapper.data("transcoded-video-url"),
		image: $wrapper.data("image-url"),
		ccUrl: $wrapper.data("cc-url"),
		ccLang: $wrapper.data("cc-lang"),
		height: $wrapper.data("media-height") || "100%",
		width: $wrapper.data("media-width") || "100%",
		playOnInit: playOnInit,
		initByAutoplay: !!initByAutoplay,
		mute: mute,
		volume: 80,
		debug: true,
		pluginPath: pluginPath,
		disableCC: disableCC
	};

	return init($wrapper, logCall, settings);
}

export function stop() {
	$.ccsVideoPlayer.stopPlayer(...arguments);
}

export function autoPlayInViewport(ep, logCall, pluginPath, disableCC) {
	const $ep = ep;

	if (!utils.isMobile()) {
		setTimeout(function () {
			$ep.find(c.embeddedVideoSelector)
				.not(c.noAutoplaySelector + " " + c.embeddedVideoSelector)
				.each(function () {

					const $el = $(this);

					const settings = {
						initByAutoplay: true,
						mute: true,
						pluginPath: pluginPath,
						disableCC: disableCC,
						tracking: getTrackingParams($el, logCall)
					};

					$.ccsVideoPlayer.initAutoPlay($el, settings);
				});
		}, 1500);
	}
}

function getTrackingParams($el, logCall) {
	return {
		logCall: videoData => getLogFunc($el, logCall, videoData),
		errorLogCall: videoData => getLogFunc($el, logCall, videoData, "error", "Submit")
	};
}

function getLogFunc($el, logCall, videoData, action, eventType) {
	const clientParams = utils.makeClientParams($el, action, eventType);

	$.extend(clientParams, videoData);

	ccs_cc_log.dispatchEvent(logCall, clientParams);
}

export function initBuyButton(options) {

	const o = options.product;
	if (!o) return;

	const cookieStr = utils.getCookie(c.cartCookie);
	const cookie = cookieStr ? JSON.parse(cookieStr) : {};

	const showOverlay = function (e) {
		$(`#${e.player.playerId}`)
			.closest(c.playerSelector)
			.find(c.buyRootSelector)
			.addClass(c.buyVisibleClass);
	};

	const stopHandler = function (e) {
		if (e.originalEvent.type === "ended") {
			showOverlay(e);
		}
	};

	$(document).on("ccsPlayer",
		function (e) {
			if (e.originalEvent.type !== "canplay") {
				return;
			}

			const $player = $(`#${e.player.playerId}`).closest(c.playerSelector);
			if ($player.find(c.buyRootSelector).length) {
				return;
			}

			const $wrapper = $("<div class='ccs-video-buy'>").appendTo($player);

			const addToCart = $(o.addToCartHtml).children(":first");
			const $cartButton = createButton(addToCart, c.cartButtonSuffix);

			if (!cookie[o.masterId]) {

				const $closeButton = createButton("<a href='javascript:;'>", c.closeButtonSuffix);

				$("<div class='ccs-video-buy-overlay'>")
					.append(
						$("<div class='ccs-video-buy-inner'>").append($cartButton).append($closeButton))
					.appendTo($wrapper);

				$closeButton.add($cartButton)
					.on("click", function () {
						cookie[o.masterId] = 1;
						utils.setCookie(c.cartCookie, JSON.stringify(cookie));

						$(document).off("ccsPlayer", null, stopHandler);

						$wrapper.removeClass(c.buyVisibleClass);
					});

				$(document).on("ccsPlayer", stopHandler);
			}

			const $floatButton = $cartButton.clone().addClass("ccs-video-buy-float").appendTo($wrapper);

			$cartButton.add($floatButton).on("click",
				function () {
					const context = JSON.stringify({
						type: "video-addtocart",
						url: e.player.getSrc(),
						pos: Math.round(e.player.getCurrentPosition())
					});

					utils.dispatchEvent(options.logCall, e.target, "conversion", context);
				});
		});
}

function createButton(html, suffix, text) {
	const buttonClass = "ccs-video-buy-button";

	const $button = $(html);

	if (text) {
		$button.text(text).attr("title", text);
	}

	return $button.addClass(buttonClass + " " + buttonClass + "-" + suffix);
}
