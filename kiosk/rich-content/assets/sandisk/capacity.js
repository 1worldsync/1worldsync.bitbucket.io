﻿import "slick";

import $ from "jquery";
import { ensureStyles }  from "../utils";

const c = {
	rootSelector: ".ccs-cc-inline-capacitychart",
	containerSelector: ".ccs-cc-inline-capacitychart-container",
	contentSelector: ".ccs-cc-inline-acc-content",
	ensureStyleLoaded: ".ccs-cc-inline-ensure-style-loaded",
	widthBoundaryMedium: 570,
	widthBoundaryWide: 680
};

let currentColumnCount;

export default function init($ep, options) {
	const $chart = $ep.find(c.rootSelector);

	if (!$chart.length)
		return;

	if ($chart.is(":visible") && ensureStyles($ep)) {
		initImpl($chart, options);
	} else {
		setTimeout(function () { init($ep, options); }, 300);
	}
}

function initImpl($chart, options) {
	const $container = $chart.find(c.containerSelector);

	const isRtl = options.isRtl;

	$container.ccsSlick({
		infinite: false,
		dots: false,
		slidesToScroll: 1,
		speed: 100,
		rtl: isRtl,
		swipeToSlide: !isRtl
	});

	const slick = $container.ccsSlick("getSlick");

	$(window).on("resize", function () {
		refreshSlick($chart, $container, slick);
	});

	let $chartContent = $chart.find(c.contentSelector);

	if ($chartContent.length === 0) {
		$chartContent = $chart;
	}

	const isVisible = function () {
		if ($chartContent.is(":visible")) {
			refreshSlick($chart, $container, slick);
		} else {
			setTimeout(isVisible, 300);
		}
	};

	setTimeout(isVisible, 100);
}

function refreshSlick($chart, $container, slick) {
	const targetColumnCount = getTargetColumnCount($chart.outerWidth(true));
	if (targetColumnCount === currentColumnCount) {
		return;
	}

	currentColumnCount = targetColumnCount;

	let columnCount = $container.data("columns-count");
	if (!columnCount) {
		columnCount = Number.POSITIVE_INFINITY;
	}

	// Delayed update since IE sometimes picks width of pre-resized container.
	setTimeout(function () {
		const count = Math.min(targetColumnCount, columnCount);
		slick.slickSetOption("slidesToShow", count, true);
		$container.toggleClass("ccs-slick-slider-arrows", count < slick.slideCount);
	}, 50);
}

function getTargetColumnCount(width) {
	if (width <= c.widthBoundaryMedium) {
		return 3;
	}

	if (width <= c.widthBoundaryWide) {
		return 4;
	}

	return 5;
}
