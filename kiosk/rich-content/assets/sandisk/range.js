﻿import $ from "jquery";
import { isMobile } from "../utils";

const c = {
	rootSelector: ".ccs-cc-inline-rangechart",
	tabSelector: ".ccs-cc-inline-rangechart-section-title",
	sectionActiveClass: "ccs-rangechart-section-active",
	contentsSelector: ".ccs-cc-inline-rangechart-section-content"
};

export default function init(ep) {
	const $chart = ep.find(c.rootSelector);

	if (!$chart.length) {
		return;
	}

	$chart.on("click", c.tabSelector, function () {
		$(this).closest("[data-section-id]").toggleClass(c.sectionActiveClass);
	});

	if (isMobile()) {
		$chart.on("click", c.tabSelector, function () {
			const $el = $(this).closest("[data-section-id]");

			returnScrollToTitle($el);
		});
	}
}

function returnScrollToTitle($el) {
	$("html, body").animate({
		scrollTop: $el.offset().top
	}, 500);
}
