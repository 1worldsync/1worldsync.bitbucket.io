﻿import $ from "jquery";
import c from "./consts";
import lightbox from "../lightbox";
import BaseGallery from "./base";
import * as video from "../video";
import * as utils from "../utils";
import "../swipe";

export default class CloudGallery extends BaseGallery {

	beforeInit() {
		this.$slick
			.find(c.slideSelector)
			.each(function () {
				const $link = $(this).find("a.ccs-cc-thumbnail-wrapper");

				$("<span class='ccs-cc-thumbnail-wrapper'>")
					.html($link.html())
					.insertAfter($link);
			});
	}

	afterInit() {
		const _ = this,
			$slick = _.$slick,
			$slides = _.slick.$slides;

		utils.isMobile()
			? _.initMobileBehavior()
			: _.initDesktopBehavior();

		$slick.on("fbClose", e => _.switchSlide(e.target, true, true));

		_.switchSlide($slides[0], true);

		if ($slides.length === 1) {
			_.initSingleSlideBehavior();
		}
	}

	initSingleSlideBehavior() {
		const _ = this,
			$slick = _.$slick,
			$gallery = _.$gallery;

		const $viewportSlideWrapper = $gallery.find(`.${c.viewportWrapperClass}`);
		const $viewportSlidePreview = $gallery.find(`.${c.viewportPreviewClass}`);

		if ($viewportSlideWrapper.data("media-type") === "image") {
			$viewportSlidePreview.removeAttr("tabindex");

			$viewportSlideWrapper
				.attr("href", $viewportSlidePreview.attr("src"))
				.removeAttr("tabindex");

			lightbox($viewportSlideWrapper, _.o, { backFocus: true });
		}

		$slick.ccsSlick("unslick").remove();
	}

	initMobileBehavior() {
		const _ = this,
			$slick = _.$slick,
			$slides = _.slick.$slides,
			$gallery = _.$gallery;

		$slides.find(".ccs-cc-thumbnail-wrapper").on("click", function (e) {
			_.switchSlide(e.target);

			if (e.originalEvent) {
				e.stopImmediatePropagation();
			}
		});

		$gallery.find(c.cloudViewportArrow).remove();

		$gallery.find(".ccs-cc-ig-cloud-viewport")
			.on("swipeleft swiperight", function (e) {
				const $curSlide = $slick.find(`.${c.cloudActiveClass}`);
				const $newSlide = e.type === "swiperight" ? $curSlide.prev() : $curSlide.next();

				if ($newSlide.length) {
					_.submitNavigateViewportEvent($newSlide, $curSlide);
					_.switchSlide($newSlide);
				}
			});
	}

	initDesktopBehavior() {
		const _ = this,
			$slides = _.slick.$slides;

		let navOnHoverTimer;
		$slides
			.on("mouseover", function (e) {
				navOnHoverTimer = setTimeout(function () {
					_.switchSlide(e.target);
				}, 100);
			})
			.on("mouseout", function () {
				clearTimeout(navOnHoverTimer);
			});

		_.$gallery.find(c.cloudViewportArrow).on("click", function (e) {
			const $arrow = $(e.target).closest(c.cloudViewportArrow);

			const $curSlide = $slides.filter(`.${c.cloudActiveClass}`);
			const $newSlide = $arrow.is(".ccs-slick-prev") ? $curSlide.prev() : $curSlide.next();

			_.submitNavigateViewportEvent($newSlide, $curSlide);
			_.switchSlide($newSlide);
		});
	}

	resize() {
		this.changeHeight();
		this.changeSlidesCount();
	}

	switchSlide(slide, suppressLog, backFocus) {
		const _ = this;
		const $slick = _.$slick;
		const slick = _.slick;

		const $slide = $(slide).closest(c.slideSelector);
		const isHidden = _.slideIsHidden($slide);

		if (backFocus) {
			isHidden
				? $slick.one("afterChange", () => $slide.focus())
				: $slide.focus();
		}

		if (isHidden) {
			slick.goTo($slide.data("slick-index"));
		}

		const $prevActive = $slick.find(`.${c.cloudActiveClass}`);

		if ($slide.is($prevActive)) {
			return;
		}

		if (!suppressLog) {
			_.submitHoverEvent($slide);
		}

		$prevActive
			.removeClass(c.cloudActiveClass)
			.find("a").removeAttr("disabled");

		$slide
			.addClass(c.cloudActiveClass)
			.find("a").attr("disabled", "");

		const $viewport = $slick.siblings(c.cloudViewport);

		$viewport
			.toggleClass("first-slide", $slide.is(":first-child"))
			.toggleClass("last-slide", $slide.is(":last-child"));

		if (_.o.isRtl || isHidden) {
			slick.loadImages();
		}

		const $prev = $viewport
			.children(`.${c.cloudActiveClass}`)
			.removeClass(c.cloudActiveClass);

		video.stop($prev.children());

		const $viewportSlide = $viewport.find(`[data-slide-index=${$slide.index()}]`);

		if ($viewportSlide.length !== 0) {
			$viewportSlide.addClass(c.cloudActiveClass);
		} else {
			$viewport.prepend(_.createViewportSlide($slide));
		}
	}

	submitHoverEvent($slide) {
		const context = {
			type: $slide.data("media-type"),
			url: $slide.data("video-url") || $slide.data("image-url")
		};

		utils.dispatchEvent(this.o.logCall, $slide, "hover", context);
	}

	slideIsHidden($slide) {
		const $slick = this.$slick;

		return $slick.offset().left > $slide.offset().left
			|| $slick.offset().left + $slick.width() < $slide.offset().left + $slide.width();
	}

	createViewportSlide($slickSlide) {
		const _ = this;

		const imgUrl = $slickSlide.data("image-url");
		const altText = $slickSlide.find("img").attr("alt").replace(_.o.media.zoomIn, _.o.media.showLargeImg);

		const $viewportSlide = $("<div>")
			.addClass("ccs-cc-ig-viewport-slide loading")
			.attr("data-slide-index", $slickSlide.index());

		const $viewportWrapper = $(`<a class='${c.viewportWrapperClass}' tabindex='-1' href='javascript:;' >`);

		const $viewportImage = $(`<img class='${c.viewportPreviewClass}'>`)
			.attr("tabindex", 0)
			.attr("src", imgUrl)
			.attr("alt", altText)
			.on("load", function () {
				$viewportSlide.removeClass("loading");
			});

		const createConcreteSlide = $slickSlide.data("media-type") === "image"
			? _.createImageViewportSlide
			: _.createVideoViewportSlide;

		createConcreteSlide.call(_, $slickSlide, $viewportWrapper, $viewportImage);

		setTimeout(function () {
			$viewportSlide.addClass(c.cloudActiveClass);	// for animation
		}, 10);

		return $viewportSlide.append($viewportWrapper);
	}

	createImageViewportSlide($slickSlide, $viewportWrapper, $viewportImage) {

		$viewportWrapper
			.attr(c.mediaTypeAttr, "image")
			.on("click", function () {
				$slickSlide.trigger("click");
			})
			.on("keypress", function (e) {
				if (e.keyCode === 13) {
					$slickSlide.trigger("click");
					return false;
				}
			})
			.append($viewportImage);
	}

	createVideoViewportSlide($slickSlide, $viewportWrapper, $viewportImage) {
		const _ = this;

		const playerId = $slickSlide.closest(c.sectionSelector).attr("id") + "_player-" + $slickSlide.index();

		const $player = $("<div>")
			.attr("id", playerId)
			.append($viewportImage)
			.appendTo($viewportWrapper);

		$viewportImage.on("load", function () {
			$player.append($("<span class='ccs-cc-inline-video-play-icon'>"));
		});

		_.copyDataAttributes($slickSlide, $viewportWrapper);

		const initPlayer = function (autoplay) {
			video.initInline(
				$viewportWrapper,
				_.o.logCall,
				_.o.video.pluginPath,
				_.o.video.disableCC,
				false,
				autoplay);
		};

		$viewportWrapper.attr("data-player", playerId);

		if (utils.isMobile()) {
			setTimeout(() => initPlayer(false));
		} else {
			$viewportWrapper
				.on("keydown", function (e) {
					if (e.keyCode === 13) {
						initPlayer(true);
					}
				})
				.one("click", () => initPlayer(true));
		}
	}

	submitNavigateViewportEvent($newSlide, $curSlide) {
		this.submitNavigateEvent($curSlide.index(), $newSlide.index(), "cloud");
	}

	copyDataAttributes($source, $target) {
		const attr = $.grep($source[0].attributes, at => /^data-((?!fancybox).)*$/.test(at.name));
		for (let i = 0; i < attr.length; i++) {
			$target.attr(attr[i].name, attr[i].value).data(attr[i].name, attr[i].value);
		}
	}

	changeHeight() {
		const _ = this,
			$viewport = _.$gallery.find(c.cloudViewport);

		const width = $viewport.width();
		const curHeight = $viewport.height();

		let maxHeight = _.maxHeight;
		if (!maxHeight) {
			maxHeight = _.maxHeight = curHeight;
		}

		const ratio = 16 / 9;

		if (width < maxHeight * ratio) {
			const newHeight = width / ratio;
			if (newHeight <= maxHeight) {
				$viewport.height(newHeight);
			}
		} else if (curHeight !== maxHeight) {
			$viewport.height(maxHeight);
		}
	}
}