﻿import "slick";

import $ from "jquery";
import c from "./consts";
import lightbox from "../lightbox";
import * as utils from "../utils";

export default class BaseGallery {

	constructor($gallery, options) {
		const _ = this;

		_.$gallery = $gallery;
		_.$slick = $gallery.find(c.thumbsContainer);

		_.o = options;
	}

	init() {
		const _ = this,
			$slick = _.$slick,
			$gallery = _.$gallery;

		if (utils.isMobile()) {
			$slick
				.children().has("[data-video-url$='.flv']")
				.remove();
		}

		_.updateAttributes();

		_.beforeInit();
		_.initSlick();
		_.afterInit();

		$gallery.trigger({ type: "ccsGalleryLoad", gallery: _ });
	}

	initSlick() {
		const _ = this,
			$gallery = _.$gallery,
			$slick = _.$slick;

		let resizeTimer;
		const resizeInvoker = function () {
			clearTimeout(resizeTimer);

			if ($gallery.is(":visible")) {
				_.resize();
			} else {
				resizeTimer = setTimeout(resizeInvoker, 500);
			}
		};

		$slick
			.on("init", function (e, slick) {
				_.slick = slick;

				lightbox(slick.$slides, _.o, { backFocus: false });

				$slick.closest(".ccs-cc-inline").on("ccsResize", resizeInvoker);
				utils.bindResizeEvent(resizeInvoker);
				resizeInvoker();
			})
			.on("beforeChange", function (e, slick, previousSlide, nextSlide) {
				_.submitNavigateEvent(previousSlide, nextSlide);
			});

		$(document).on("afterClose.ccs-fb", function (e, fancybox, fbSlide) {
			const $slide = fbSlide.opts.$orig;

			if (!$slick.has($slide).length) {
				return;
			}

			$slide.trigger({ type: "fbClose", originalEvent: e });
		});

		const slickConfig = $.extend({}, c.slickDefaults, { rtl: _.o.isRtl });
		$slick.ccsSlick(slickConfig);
	}

	updateAttributes() {
		const _ = this,
			$slides = _.$slick.children();

		const elementId = _.$gallery.closest(c.sectionSelector).attr("id");

		$slides.each(function (index, el) {
			const $slide = $(el).attr("data-ccs-fancybox", elementId);

			$slide.attr("data-src", $slide.data("video-url") || $slide.data("image-url"));

			const $img = $slide.find("img");

			let alt = $img.attr("alt");
			if (!alt) {
				return;
			}

			alt = utils.stringFormat(alt, index + 1, $slides.length);

			$img.attr("alt", alt.toLowerCase());
		});
	}

	submitNavigateEvent(previousSlide, nextSlide, subelement) {
		const direction = nextSlide - previousSlide;
		if (direction === 0) {
			return;
		}

		const actionContext = {
			scroll: direction > 0 ^ this.o.isRtl ? "right" : "left",
			subelement: subelement
		};

		utils.dispatchEvent(this.o.logCall, this.$gallery, "navigate-content", actionContext);
	}

	changeSlidesCount() {
		const _ = this;
		const slick = this.slick;

		const slickSize = slick.$list.width();

		const $slide = slick.$slides.find(".ccs-cc-thumbnail-wrapper:visible");
		const slideSize = $slide.outerWidth(true);

		const slidesCount = _.calcCount(slickSize, slideSize);

		if (slick.options.slidesToShow !== slidesCount || slick.listWidth === 0) {
			_.slickSetSlidesCount(slidesCount);
		}
	}

	calcCount(wrapSize, slideSize) {
		return Math.max(Math.floor(wrapSize / slideSize), 1);
	}

	slickSetSlidesCount(count) {
		const slick = this.slick;

		slick.slickSetOption("slidesToShow", count);
		slick.slickSetOption("slidesToScroll", count);

		slick.refresh();
	}

	resize() { }
	beforeInit() { }
	afterInit() { }
	switchSlide(slide) { }
}
