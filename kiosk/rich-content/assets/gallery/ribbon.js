﻿import $ from "jquery";
import BaseGallery from "./base";

export default class RibbonGallery extends BaseGallery {

	afterInit() {
		const _ = this;

		if (_.o.isRtl) {
			_.slick.loadImages();
		}

		_.$slick.on("fbClose", e => _.switchSlide(e.target));
	}

	resize() {
		this.changeSlidesCount();
	}

	switchSlide(slide) {

		const slick = this.slick,
			$slick = this.$slick,
			$slide = $(slide);

		$slick.one("afterChange", () => {
			$slide.children().focus();
			slick.loadImages();
		});

		slick.goTo($slide.data("slick-index"));
	}
}