﻿import $ from "jquery";
import common from "../common";

const c = {
	slickDefaults: { speed: 200, draggable: false, infinite: false },
	rootGallerySelector: ".ccs-cc-ig",
	thumbsContainer: ".ccs-slick-slider",
	slideSelector: ".ccs-slick-slide",
	viewportWrapperClass: "ccs-cc-viewport-slide-wrapper",
	viewportPreviewClass: "ccs-cc-ig-viewport-preview",
	cloudRoot: ".ccs-cc-ig-cloud",
	cloudViewport: ".ccs-cc-ig-cloud-viewport",
	cloudViewportArrow: ".ccs-ig-viewport-arrow",
	cloudActiveClass: "ccs-ig-cloud-active"
};

export default $.extend(c, common);
