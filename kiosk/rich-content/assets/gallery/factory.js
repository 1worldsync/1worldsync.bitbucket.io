﻿import Ribbon from "./ribbon";
import Cloud from "./cloud";

export default class GalleryFactory {

	create($gallery, o) {

		let Gallery = Ribbon;

		if ($gallery.hasClass("ccs-cc-ig-cloud")) {
			Gallery = Cloud;
		}

		return new Gallery($gallery, o);
	}
}
