﻿import $ from "jquery";
import { init as initAccordion } from "./accordion";

const c = {
	faqSectionSelector: ".ccs-cc-inline-faq .ccs-cc-inline-features",
	stateAttr: "data-ccs-cc-inline-acc-state",
	expanded: "expanded",
	collapsed: "collapsed",
	sectionWithVideo: ".ccs-cc-inline-faq-item:has(.ccs-cc-inline-embedded-video)",
	sectionHeader: ".ccs-cc-inline-acc-header",
	sectionContent: ".ccs-cc-inline-acc-content"
};

export default function (ep, options) {
	const $faq = ep.find(c.faqSectionSelector);
	initAccordion($faq, options);
	initPlayPauseVideo($faq);
}

function initPlayPauseVideo($faq) {
	const $videoSections = $faq.find(c.sectionWithVideo);

	$videoSections.on("click", c.sectionHeader, function (e) {
		if ($(e.currentTarget).attr(c.stateAttr) === c.collapsed) {
			return;
		}

		const $content = $(e.delegateTarget).find(c.sectionContent);
		const $playerContainer = $content.find("div[data-player]");
		const player = $playerContainer.ccsVideoPlayer("getPlayer");
		if (player == null) {
			return;
		}

		player.pause();
	});
}
