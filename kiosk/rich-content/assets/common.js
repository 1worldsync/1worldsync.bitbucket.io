﻿export default {
	rootSelector: ".ccs-cc-inline",
	sectionSelector: ".ccs-cc-inline-section",
	mediaTypeAttr: "data-media-type",

	// features, video
	embeddedVideoSelector: ".ccs-cc-inline-embedded-video",
	noAutoplaySelector: ".ccs-cc-inline-noautoplay",

	// images
	imageSelector: ".ccs-cc-inline-thumbnail a.ccs-cc-inline-thumbnail-image",

	// videos
	videoSelector: ".ccs-cc-inline-thumbnail a.ccs-cc-inline-thumbnail-video"
};
