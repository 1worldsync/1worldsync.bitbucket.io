﻿import $ from "jquery";
import * as utils from "./utils";
import common from "./common";
import "slick";

const c = {
	carouselWrapperSelector: ".ccs-cc-fc",
	carouselSelector: ".ccs-cc-fc-items",
	activeSlideSelector: ".ccs-slick-active",
	clonedSlideSelector: ".ccs-slick-cloned",
	customControls: ".ccs-cc-fc-custom-controls",
	bgFeatureSelector: ".ccs-cc-inline-feature-background",
	playPauseBtnSelector: ".ccs-cc-fc-play-pause-btn",
	fcArrows: {
		class: "ccs-cc-fc-arrow",
		selector: ".ccs-cc-fc-arrow",
		next: ".ccs-cc-fc-arrow.ccs-slick-next",
		prev: ".ccs-cc-fc-arrow.ccs-slick-prev"
	}
};

$.extend(c, common);

export default class FeatureCarousel {
	constructor(container, o) {
		const _ = this;

		_.$container = $(container);
		_.settings = o.features.carousel;
		_.isRtl = o.isRtl;
		_.logCall = o.logCall;

		_.init();
	}

	init() {
		const _ = this;
		const $container = _.$container;
		const settings = _.settings;

		const autoplaySpeed = settings.speed;
		const showArrows = !!settings.arrows;
		const showDots = !!settings.points;

		const isAutoplay = _.isAutoplayEnabled();
		const isMobile = utils.isMobile();

		$container.toggleClass("ccs-cc-fc-mobile", isMobile);

		_.$slickWrapper = $container.find(c.carouselSelector);
		_.$customControls = $container.find(c.customControls);

		if (!showDots && !settings.playpause) {
			_.$customControls.remove();
		}

		if (!showArrows) {
			$container.find(c.fcArrows.selector).remove();
		}

		_.$slickWrapper.ccsSlick({
			autoplaySpeed: autoplaySpeed,
			speed: isMobile ? 100 : 500,
			arrows: showArrows,
			dots: showDots,
			slidesToScroll: 1,
			autoplay: isAutoplay,
			lazyLoad: "progressive",
			pauseOnHover: false,
			infinite: true,
			draggable: false,
			rtl: _.isRtl,
			swipe: isMobile,
			appendDots: _.$customControls,
			prevArrow: $container.find(c.fcArrows.prev),
			nextArrow: $container.find(c.fcArrows.next)
		});

		_.slick = _.$slickWrapper.ccsSlick("getSlick");
		_.currentSlideIndex = _.slick.currentSlide;
		_.initBackGroundVideoAutoPlay(_.$slickWrapper);

		_.$slickWrapper.on("beforeChange", function (e) {
			$(this).find(".ccs-slick-slide").css("visibility", "visible");
			_.currentSlideIndex = _.slick.currentSlide;

			const player = $(this).data("player-instance");
			if (!player) {
				return;
			}

			player.pause();
		});

		_.$slickWrapper.on("afterChange", function (e) {
			$(this).find(".ccs-slick-slide:not('.ccs-slick-active')").css("visibility", "hidden");
			const $self = $(this);
			_.initBackGroundVideoAutoPlay($self);
		});

		_.reportingListener();
		_.initPlayPauseBtn();
		_.fancyboxListener(_.$slickWrapper);
		_.hoverListener();

		_.$slickWrapper.on("ccsPlayer", c.embeddedVideoSelector, function (e) {
			if (e.originalEvent.type === "canplay" || e.originalEvent.type === "play") {

				_.$slickWrapper.data("player-instance", e.player);

				if (!isAutoplay) {
					return;
				}

				_.$slickWrapper.ccsSlick("pause");
				_.hoverListener(true);
			}

			if (e.originalEvent.type === "pause" || e.originalEvent.type === "ended") {

				_.$slickWrapper.data("player-instance", null);

				if (!isAutoplay) {
					return;
				}

				_.hoverListener();
			}
		});
	}

	fancyboxListener($container) {
		const _ = this;

		if (!_.isAutoplayEnabled()) {
			return;
		}

		$(document)
			.on("beforeShow.ccs-fb", function (e) {
				$container.ccsSlick("pause");
				_.hoverListener(true);
			})
			.on("afterClose.ccs-fb", function (e) {
				$container.ccsSlick("play");
				_.hoverListener();
			});
	}

	hoverListener(turnOff) {
		const _ = this;
		const $container = _.$container;
		const $slickWrapper = _.$slickWrapper;

		if (!_.isAutoplayEnabled()) {
			return;
		}

		if (turnOff) {
			$container.off("mouseleave mouseenter focusin focusout");
			return;
		}

		$container
			.on("mouseenter focusin", function (e) {
				$slickWrapper.ccsSlick("pause");
			})
			.on("mouseleave focusout", function (e) {
				let event = "play";
				const $targerElement = $(e.relatedTarget);
				if ($targerElement.hasClass("ccs-zoom-tracker") || $targerElement.hasClass("ccs-fancybox-container"))
					event = "pause";

				$slickWrapper.ccsSlick(event);
			});
	}

	isAutoplayEnabled() {
		const _ = this;
		const settings = _.settings;
		return settings.speed && !utils.isMobile();

	}

	reportingListener() {
		const _ = this;
		const $container = _.$container;
		const $wrapper = _.$slickWrapper;
		const $arrows = $container.find(c.fcArrows.selector);

		$arrows.on("click", function (e) {
			_.submitNavigateEvent("arrows");
		});

		$container.find(".ccs-slick-dots li").on("click", function (e) {
			_.submitNavigateEvent("points");
		});

		$wrapper[0].addEventListener("swipe", function (e) {
			_.submitNavigateEvent("swipe");
		}, true);
	}

	submitNavigateEvent(iteraction) {
		const _ = this;
		const slick = _.slick;
		const curSlide = _.currentSlideIndex;

		const newSlide = slick.currentSlide;
		const eventParams = {
			subelement: "carousel",
			triggeredby: iteraction,
			nextSlide: newSlide + 1,
			previousSlide: curSlide + 1
		};

		const direction = eventParams.nextSlide - eventParams.previousSlide;
		if (direction === 0) {
			return;
		}

		const actionContext = {
			subelement: eventParams.subelement,
			triggeredby: eventParams.triggeredby,
			scroll: direction > 0 ^ _.isRtl ? "right" : "left"
		};

		if (actionContext.subelement) {
			actionContext.target = eventParams.nextSlide;
		}

		utils.dispatchEvent(_.logCall, _.$container, "navigate-content", actionContext);
	}

	initPlayPauseBtn() {
		const _ = this;
		const settings = _.settings;
		const $container = _.$container;

		const $customControls = $container.find(c.customControls);
		const $btnPlayPause = $customControls.find(c.playPauseBtnSelector);

		if (!settings.playpause || !_.isAutoplayEnabled()) {
			$btnPlayPause.remove();
			return;
		}

		const $slickWrapper = $container.find(c.carouselSelector);
		$btnPlayPause.on("click", function (e) {
			const $self = $(this);
			if ($self.hasClass("ccs-play")) {
				$self.removeClass("ccs-play").addClass("ccs-pause");
				$self.attr("aria-label", "click to pause");
				$slickWrapper.ccsSlick("play");
				_.submitClickEvent("play");
				_.hoverListener();
			}
			else {
				_.hoverListener(true);
				$self.removeClass("ccs-pause").addClass("ccs-play");
				$self.attr("aria-label", "click to play");
				$slickWrapper.ccsSlick("pause");
				_.submitClickEvent("pause");
			}
		});

	}

	/*CCSDT-6841 - "autoplay won't work in Safari and Chrome if any of parents of video tag have any DOM manipulation*/
	/*the explicit trigger required*/
	initBackGroundVideoAutoPlay($bgContainer) {
		const _ = this;
		if (utils.isMobile()) {
			return;
		}

		const $bgVideo = $bgContainer.find(c.bgFeatureSelector).find("video");
		if ($bgVideo.length === 0) {
			return;
		}

		$bgVideo.each(function () {
			if ($(this).closest(c.activeSlideSelector).length === 0) {
				$(this)[0].pause();
			} else {
				$(this)[0].play();
			}

			$(this).prop("muted", true);
		});
	}

	submitClickEvent(iteraction) {

		const actionContext = {
			subelement: "carousel",
			click: iteraction
		};

		utils.dispatchEvent(this.logCall, this.$container, "navigate-content", actionContext);
	}

	getParams(eventName, next, prev) {
		return {
			subelement: eventName,
			nextSlide: next,
			previousSlide: prev
		};
	}

	getSlick($container) {
		return $container.ccsSlick("getSlick");
	}
}
