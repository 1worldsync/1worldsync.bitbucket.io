﻿import $ from "jquery";
import require from "require";
import * as utils from "./utils";

const c = {
	width: 480,
	height: 320,
	animate: utils.isMobile(),
	rootSelector: ".ccs-cc-inline",
	spinSelector: ".ccs-cc-inline-spritespin-container",
	preloadLayer: "div[data-preload-layer='true']",
	preloadContainer: "div[data-preload-container]",
	toolbox: {
		playPauseBtn: "span[data-play-pause='true']",
		defaultSliderHeight: 50,
		slider: "div[data-slider-ui=true]",
		attr: {
			slider: "data-slider-ui",
		},
		css: {
			play: "ccs-cc-inline-video-play-icon",
			pause: "ccs-cc-inline-video-pause-icon"
		}
	},
	spritespinImageSelector: ".spritespin-stage",
	altText: "data-default-alt-text"
};

export default function setup(options) {
	const o = $.extend({}, c, options.spritespin, { logCall: options.logCall });

	if (!o.source || !o.source.length) {
		return;
	}

	require("jqSpitespin");
	require("jqSlider");

	const ep = $(`#${options.inlineContentId}`);

	utils.updateOnVisible(ep, null, () => runInitialization(o));
}

function runInitialization(o) {

	const source = o.source;

	const width = o.width;
	const height = o.height;

	const $toolbox = $(`#${o.toolboxId}`);
	const $rootDiv = $toolbox.closest(c.rootSelector);
	const $preloadContainer = $rootDiv.find(o.preloadContainer);
	const $playPauseBtn = $toolbox.find(o.toolbox.playPauseBtn);

	let defaultWidth = width;
	let defaultHeight = height;

	let resizeTimeout;

	calculateImageDimensions($preloadContainer, source[0], function (calculatedWidth, calculatedHeight) {
		if (defaultWidth > calculatedWidth)
			defaultWidth = calculatedWidth;

		defaultHeight = calculateContainerHeight(defaultWidth, calculatedWidth, calculatedHeight);

		$(window).resize(function () {
			resizeTimeout = setTimeout(function () {
				onWindowResize.call(o, defaultWidth, defaultHeight, $rootDiv, $toolbox, $playPauseBtn);
			}, 100);
		});

		const api = onWindowResize.call(o, defaultWidth, defaultHeight, $rootDiv, $toolbox, $playPauseBtn, true);

		drawHelperTools.call(o, $toolbox, defaultWidth, $playPauseBtn, api);
	});
}

function calculateImageDimensions($preloadContainer, imgUrl, completeCallback) {
	const image = new Image();
	image.src = imgUrl;
	image.onload = function () {
		const calculatedWidth = this.width;
		const calculatedHeight = this.height;

		if ($.isFunction(completeCallback))
			completeCallback(calculatedWidth, calculatedHeight);
	};
}

function calculateContainerHeight(containerWidth, imageWidth, imageHeight) {
	return (containerWidth * imageHeight) / imageWidth;
}

function drawHelperTools($toolbox, width, $playPauseBtn, spriteSpinApi) {
	const settings = this;

	$toolbox.width(width);

	$playPauseBtn.on("keydown", function () {
		if (event.which == 13) {
			$(this).click();
		}
	});

	$playPauseBtn.click(function () {
		settings.animate = changePlayPauseState(
			$playPauseBtn,
			spriteSpinApi,
			settings.animate,
			settings.toolbox.css.pause);

		logChangeState(settings);
	});
}

function changePlayPauseState($playPauseBtn, api, isPlaying, cssSelector) {
	if (isPlaying) {
		$playPauseBtn.removeClass(cssSelector);
		api.stopAnimation();
	} else {
		$playPauseBtn.addClass(cssSelector);
		api.startAnimation();
	}

	return !isPlaying;
}

function resizeToolbox($toolbox, width) {
	$toolbox.width(width);
}

function initControl(width, height, $toolbox, $playPauseBtn, firstTime) {
	var settings = this;

	const id = settings.id;
	const source = settings.source;
	const animate = settings.animate;
	const fps = settings.fps;
	const rate = settings.rate;

	const $spritespinDiv = $("#" + id);
	const $spritespinContainer = $spritespinDiv.parent();
	let $slider;
	const $preloadDiv = $spritespinContainer.find(settings.preloadLayer);
	$preloadDiv.width(width);
	$preloadDiv.height(height + settings.toolbox.defaultSliderHeight);

	const animated = animate;
	let api;

	var rateFramesCount = fps * rate;
	var rateCounter = 0;
	const frameTime = 1000 / fps;

	$spritespinContainer.width(width);
	$spritespinDiv.spritespin({
		animate: animated,
		source: source,
		frames: source.length,
		framesX: 1,
		frameTime: frameTime,
		renderer: "image",
		width: width,
		height: height,
		reverse: false,
		onLoad: function () {

			$preloadDiv.remove();
			api = $spritespinDiv.spritespin("api");
			$slider = $toolbox.find(settings.toolbox.slider);

			if ($slider.length !== 0)
				return;

			$slider = $("<div>");
			$slider.attr(settings.toolbox.attr.slider, "true");

			$slider.slider({
				min: 0,
				max: settings.source.length - 1,
				slide: function (e, ui) {
					if (settings.animate)
						settings.animate = changePlayPauseState(
							$playPauseBtn,
							api,
							settings.animate,
							settings.toolbox.css.pause);

					rateCounter = 0;

					api.updateFrame(ui.value);
				},
				stop: function (e, ui) {
					logRotate(settings, "slider");
				}
			});

			$slider.appendTo($toolbox);

			resizeToolbox($toolbox, width);

			$spritespinDiv.trigger("ccsSpinLoad");
		},
		onFrame: function (e, data) {
			if (rate !== 0) {
				rateCounter++;

				if (rateCounter === rateFramesCount) {
					rateCounter = 0;
					api.stopAnimation();
				}
			}

			$slider.slider("value", data.frame);

			if (!api.isPlaying()) {
				settings.animate = true;
				settings.animate = changePlayPauseState(
					$playPauseBtn,
					api,
					settings.animate,
					settings.toolbox.css.pause);
			}
		},
		onDraw: function () {
			setImageAltText($spritespinDiv);
		}
	});

	$spritespinDiv.trigger("ccsSpinResize");

	api = $spritespinDiv.spritespin("api");

	if (!firstTime)
		return api;

	var playTrigger = false,
		mousedown = false;

	$spritespinDiv.on("mousedown touchstart", function () {
		playTrigger = true;
		mousedown = true;

		setTimeout(function () {
			playTrigger = false;
		}, 200);
	});

	$spritespinDiv.on("mouseup touchend", function (e) {

		if (playTrigger) {
			settings.animate = changePlayPauseState(
				$playPauseBtn,
				api,
				settings.animate,
				settings.toolbox.css.pause);
			playTrigger = false;

			logChangeState(settings);
		} else {
			if (mousedown) {
				logRotate(settings, "drag");
			}
		}

		mousedown = false;
	});

	$spritespinDiv.on("mouseleave", function (e) {
		if (e.buttons !== 0 && mousedown) {
			logRotate(settings, "drag");
		}

		mousedown = false;
	});

	return api;
}

function setImageAltText($spritespinDiv) {
	const altText = $spritespinDiv.attr(c.altText);
	const $spriteDiv = $spritespinDiv.find(c.spritespinImageSelector);
	const $sprite = $spriteDiv.find("img");
	$sprite.each(function (index) {
		$(this).attr("alt", `${altText} ${index +1}`);
	});
}

function onWindowResize(width, height, $rootContainer, $toolbox, $playPauseBtn, firstTime) {
	const settings = this;

	let widthToSet;
	let heightToSet;
	if ($rootContainer.width() < width) {
		heightToSet = Math.floor($rootContainer.width() * height / width);
		widthToSet = $rootContainer.width();
	} else {
		widthToSet = width;
		heightToSet = height;
	}

	const api = initControl.call(settings, widthToSet, heightToSet, $toolbox, $playPauseBtn, firstTime);
	resizeToolbox($toolbox, widthToSet);

	return api;
}

function logChangeState(settings) {
	const context = { type: settings.animate ? "play" : "stop" };
	logEvent(settings, context);
}

function logRotate(settings, type) {
	const context = { type: type };
	logEvent(settings, context);
}

function logEvent(settings, context) {
	utils.dispatchEvent(
		settings.logCall,
		`#${settings.toolboxId}`,
		"feature-click",
		context
	);
}