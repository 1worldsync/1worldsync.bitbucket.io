﻿import $ from "jquery";
import * as utils from "./utils";
import "Fancybox";

const c = {
	arSectionSelector: ".ccs-cc-inline-augmentedreality",
	arFancyBoxContent: ".ccs-inline-ar-fancybox-content"
};

var animationList = null;

export default function (ep, options) {
	const $container = ep.find(c.arSectionSelector);
	
	// in browser without customElements remove block
	if (!window.customElements) {
		$container.remove();
		return;
	}

	const $thumbnailCnt = $container.find(".ccs-inline-ar-thumbnail-cnt");
	const $viewerCnt = $container.find(".ccs-inline-ar-viewer-cnt");

	if (options.augmentedReality.autoLoad) {
		loadModelViewer();
	}

	$thumbnailCnt.on("keydown", function (event) {
		if (event.which == 13) {
			$(this).click();
		}
	});

	$thumbnailCnt.on("click",
		() => {
			if (options.augmentedReality.launchInLightbox) {
				var config = {
					content: `<div class='ccs-inline-ar-fancybox-content' tabindex='0'>${$viewerCnt.find("script").text()}</div>`,
					type: "html",
					baseClass: "ccs-cc-inline",
					smallBtn: false,
					touch: false,
					afterLoad: function (instance, fbContent) {
						const $viewer = fbContent.$content.find("ccs-model-viewer");
						setViewerHandler($viewer);
						ajustLightBoxSize();
					}
				}
				$.fancybox.open(config);
				return;
			}
			loadModelViewer();
		});

	function getErrorMessage(error) {
		return error && error.originalEvent
			? {
				message: error.originalEvent.detail.message,
				type: error.originalEvent.detail.type
			}
			: undefined;
	}

	function loadModelViewer() {
		$thumbnailCnt.hide();
		$viewerCnt.show();

		$viewerCnt.html($viewerCnt.find("script").text());
		const $viewer = $viewerCnt.find("ccs-model-viewer");
		setViewerHandler($viewer);
	}

	function setViewerHandler($viewer) {
		$viewer.on("error", error => {
			utils.dispatchEvent(options.logCall, $viewer, "error", getErrorMessage(error), "Submit");
		});

		$viewer.on("ar-status", event => {
			if (event.detail.status === "not-presenting") {
				$viewer.attr("ar-modes", $viewer.attr("ar-modes").replace("webxr").trim());
				$viewer[0].activateAR();
			}
		});

		utils.dispatchEvent(options.logCall, $viewer, "feature-click", { type: "play" });

		loadAnimation($viewer);
	}

	function loadAnimation(viewer) {
		if (options.augmentedReality.animation == null)
			return;

		// launch animation first time
		var playEvent = options.augmentedReality.animation.playEvent;
		animationList = options.augmentedReality.animation.animationNameList;
		if (playEvent === 'Click') {
			viewer.one('click',
				function () {
					setAnimationName(viewer);
					viewer[0].play();
					setChangeAnimationEvent(viewer);
				});
		} else if (playEvent === 'DoubleClick') {
			viewer.one('dblclick',
				function () {
					setAnimationName(viewer);
					viewer[0].play();
					setChangeAnimationEvent(viewer);
				});
		}
		else {
			var delay = options.augmentedReality.animation.delay;
			if (delay != null) {
				setTimeout(function () {
					launchAutoplayAnimation(viewer);
				}, delay);
			} else {
				launchAutoplayAnimation(viewer);
			}
		}
	}

	function setChangeAnimationEvent(viewer) {
		var changeEvent = options.augmentedReality.animation.changeEvent;
		if (changeEvent == 'Action') {
			viewer.on('click',
				function () {
					setAnimationName(viewer);
				});
		} else {
			var delay = options.augmentedReality.animation.delay;
			if (delay != null) {
				setInterval(function () {
					setAnimationName(viewer);
				}, delay);
			}
		}
	}

	function setAnimationName(viewer) {
		if (animationList == null || animationList.length === 0) {
			animationList = viewer[0].availableAnimations;
		}

		if (!viewer.attr('animation-name')) {
			viewer.attr('animation-name', animationList[0]);
			return;
		}

		var currentAnimationNameIndex = animationList.indexOf(viewer.attr('animation-name'));
		var newAnimationName = currentAnimationNameIndex == animationList.length - 1
			? options.augmentedReality.animation.repeat === true ? animationList[0] : null
			: animationList[currentAnimationNameIndex + 1];

		if (newAnimationName != null)
			viewer.attr('animation-name', newAnimationName);
		else {
			viewer[0].pause();
			viewer[0].currentTime = 0;
		}
	}

	function launchAutoplayAnimation(viewer) {
		setAnimationName(viewer);
		viewer.attr("autoplay", true);
		setChangeAnimationEvent(viewer);
	}

	function ajustLightBoxSize() {
		var ar = options.augmentedReality.aspectRatio == undefined
			|| options.augmentedReality.aspectRatio == 0
			? getDefaultAspectRatio() : options.augmentedReality.aspectRatio;

		var height = $container.height() / window.innerHeight * 100;
		var width = height / ar;
		$(c.arFancyBoxContent)
			.css("width", `${width}vw`)
			.css("height", `${height}vh`);
	}

	function getDefaultAspectRatio() {
		return window.innerWidth / window.innerHeight;
	}
}