﻿import $ from "jquery";
const c = {
	specsArea: ".ccs-cc-inline-specifications",
	showButton: ".ccs-cc-inline-specs-more-btn[data-ccs-cc-inline-specs-show]",
	hideButton: ".ccs-cc-inline-specs-more-btn[data-ccs-cc-inline-specs-hide]",
	expandableSpecs: "[data-ccs-cc-inline-specs-expandable]",
	specRow: ".ccs-cc-inline-specs-row"
};

export default function (ep) {

	const $specsArea = ep.find(c.specsArea);
	const $specsShowButton = $specsArea.find(c.showButton);
	const $specsHideButton = $specsArea.find(c.hideButton);
	const $expandableSpecs = $specsArea.find(c.expandableSpecs);

	$(c.specRow).each(function () {
		var imgs = $(this).find("img");
		imgs.each(function () {
			var fn = $(this).attr("src").split('/').pop().split('.').shift();
			$(this).attr("alt", fn);
		})
	});

	$specsHideButton.find("a").click(function () {
		$expandableSpecs.hide("fast");
		$specsShowButton.show();
		$specsHideButton.hide();
	});

	$specsShowButton.find("a").click(function () {
		$expandableSpecs.show("fast");
		$specsShowButton.hide();
		$specsHideButton.show();
	});

}