﻿import $ from "jquery";
import { updateOnVisible } from "./utils";
import GalleryFactory from "./gallery/factory";
import c from "./gallery/consts";

export default function (ep, o) {

	const factory = new GalleryFactory();

	updateOnVisible(ep, c.rootGallerySelector, el => {
		const $gallery = $(el);
		const gallery = factory.create($gallery, o);

		gallery.init();

		$gallery.data("gallery", gallery);
	});
}