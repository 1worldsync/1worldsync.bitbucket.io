﻿import $ from "jquery";
import common from "./common";

const c = {
	imageModeAttr: "data-image-mode",
	headerImageSelector: ".ccs-cc-inline-header-layout img"
};

$.extend(c, common);

// Initializes mobile images swap
export default function (ep, options) {

	const trackedImages = ep.find(c.headerImageSelector + "," + c.imageSelector + " img").get();
	const сheck = new RegExp(options.features.swapBreakpoint + "px");

	function handler() {
		const breakpoints = ep.attr("max-width");
		const mobile = breakpoints && breakpoints.match(сheck);
		const mode = mobile ? "mobile" : "desktop";

		swapImages(trackedImages, mode);
	}

	handler();
	showHeader(ep);

	ep.on("ccsBreakpointFired", handler);
}

// Swaps desktop and mobile images
function swapImages(images, mode) {

	for (let i = images.length - 1; i >= 0; i--) {
		const $img = $(images[i]);
		const $a = $img.parent(c.imageSelector);

		if (!hasMobileMode($img)) {
			images.splice(i, 1);
			continue;
		}

		if (!changeMode($img, mode)) {
			continue;
		}

		let actualSrc = pickAttribute($img, "data-src", "src");
		swapAttributes($img, actualSrc, "data-alternative-src");

		const altValue = swapAttributes($img, "data-large", "data-alternative-large");
		if (altValue && $a.length) {
			actualSrc = pickAttribute($a, "data-url", "href");
			$a.attr(actualSrc, altValue);
		}
	}
}

// Checks whether the mobile mode is applicable
function hasMobileMode($baseNode) {
	return $baseNode.length && $baseNode[0].hasAttribute(c.imageModeAttr);
}

// Changes the mode: mobile|desktop
function changeMode($baseNode, mode) {
	if ($baseNode.attr(c.imageModeAttr) === mode) {
		return false;
	}

	$baseNode.attr(c.imageModeAttr, mode);
	return true;
}

// Picks existing attribute
function pickAttribute($element, attr, fallbackAttr) {
	return $element[0].hasAttribute(attr)
		? attr
		: fallbackAttr;
}

// Swaps values of attributes
function swapAttributes($element, attr, altAttr) {
	const altAttrValue = $element.attr(altAttr);
	if (!altAttrValue) {
		return null;
	}

	$element.attr(altAttr, $element.attr(attr));
	$element.attr(attr, altAttrValue);
	return altAttrValue;
}

// Header image is hidden on mobile to avoid flickering
function showHeader(ep) {
	const $headerImage = ep.find(c.headerImageSelector);

	$headerImage.on("load", e => $(e.target).css("visibility", "visible"));

	if ($headerImage.length && $headerImage[0].complete) {
		$headerImage.trigger("load");
	}
}