﻿import $ from "jquery";
import "ElementQueries";

import bazaar from "./bazaar";
import comparisontable from "./comparisontable";
import compatibleitems from "./compatibleitems";
import faq from "./faq";
import features from "./features";
import footnotes from "./footnotes";
import gallery from "./gallery";
import hotspots from "./hotspots";
import jumplinks from "./jumplinks";
import specs from "./specs";
import spin from "./spin";
import augmentedreality from "./augmentedreality";

import capacityChart from "./sandisk/capacity";
import rangeChart from "./sandisk/range";

import * as utils from "./utils";
import { init as initAccordion } from "./accordion";
import { initBuyButton as initVideoByButton } from "./video";


export default function (options) {

	const ep = $(`#${options.inlineContentId}`);

	bazaar(ep, options);
	comparisontable(options);
	compatibleitems(ep);
	faq(ep, options);
	features(ep, options);
	footnotes(ep, options);
	gallery(ep, options);
	hotspots(ep, options);
	jumplinks(ep, options);
	specs(ep, options);
	spin(options);
	augmentedreality(ep, options);

	capacityChart(ep, options);
	rangeChart(ep);

	initVideoByButton(options);
	initAccordion(ep, options);
	initDocumentsReporting(ep, options);
	initSectionViewportReporting(ep, options);

	utils.fixBlockWidth(ep);
}

function initDocumentsReporting(ep, o) {

	ep.find(".ccs-cc-inline-assets")
		.on("click",
			".ccs-cc-inline-asset a",
			e => utils.dispatchEvent(o.logCall, e.target, "download", e.target.href));
}

function initSectionViewportReporting(ep, o) {

	const sectionViewports = ep.find(".ccs-cc-inline-section[data-elementid]")
		.map((i, el) => {
			const $acc = $(el).find(".ccs-cc-inline-acc-content");
			return $acc.length > 0 ? $acc[0] : el;
		}).toArray();

	utils.initViewportEvent(sectionViewports,
		function () {
			utils.dispatchEvent(o.logCall, this, "display", "viewport", "Submit");
		});
}
