﻿import $ from "jquery";
import { dispatchEvent } from "./utils";

const c = {
	headerSelector: ".ccs-cc-inline-acc-header",
	contentSelector: ".ccs-cc-inline-acc-content",
	stateAttr: "data-ccs-cc-inline-acc-state",
	expanded: "expanded",
	collapsed: "collapsed",
	ariaStateAttr: "aria-expanded"
};

export function init($container, options) {
	$container.find(c.headerSelector).each(function () {
		const $header = $(this);

		changeState($header, options, false, $header.attr(c.stateAttr) === c.expanded);
	});

	$container.on("click",
		c.headerSelector,
		function (e) {
			changeState($(this), options, e.originalEvent !== undefined);
			e.stopPropagation();
		}
	);
}

function changeState($header, o, withAnimation, expand) {

	let initialState;

	if (expand === undefined) { // not initialization
		initialState = $header.attr(c.stateAttr);
		expand = initialState !== c.expanded;
	}

	$header.attr(c.ariaStateAttr, expand);
	const targetState = expand ? c.expanded : c.collapsed;

	if (initialState === targetState) {
		return;
	}

	$header
		.attr(c.stateAttr, targetState)
		.attr("aria-label", `${$header.attr("title")} ${expand ? o.accordion.expandedText : o.accordion.collapsedText}`);

	const $content = $header.parent().parent().children(c.contentSelector);

	if (expand) {
		withAnimation ? $content.slideDown() : $content.css("display", "block");
		$content.trigger("accordionOpen");
	}
	else {
		withAnimation ? $content.slideUp() : $content.css("display", "none");
		$content.trigger("accordionClose");
	}

	if (initialState === undefined) {
		return;
	}

	const context = {
		type: `section-${targetState === c.expanded ? "open" : "close"}`
	};

	const index = $header.data("index");
	if (index) {
		context.index = index;
		context.title = $header.attr("title");
	}

	dispatchEvent(o.logCall, $header, "feature-click", context);
}

export function expand($area) {
	toggle($area, c.expanded);
}

export function collapse($area) {
	toggle($area, c.collapsed);
}

function toggle($area, state) {
	const $header = $area.filter(c.headerSelector).add($area.find(c.headerSelector));

	if ($header.length > 0 && $header.attr(c.stateAttr) !== state) {
		$header.trigger("click");
	}
}