﻿import $ from "jquery";

const c = {
	ensureStyleLoaded: ".ccs-cc-inline-ensure-style-loaded",
	hiddenHrefLinks: "a[data-url]",
	clickableClass: "ccs-cc-inline-clickable",
	urlAttr: "data-url"
};

export function isMobile() {
	return ccs_util.isMobile();
}

export function keepFocus() {
	return ccs_util.keepFocus(...arguments);
}

export function initViewportEvent() {
	return ccs_util.initViewportEvent(...arguments);
}

export function stringFormat() {
	return ccs_util.stringFormat(...arguments);
}

export function restoreHref($container) {

	$container
		.find(c.hiddenHrefLinks)
		.addBack(c.hiddenHrefLinks)
		.each(function () {
			const $link = $(this);
			const url = $link.attr(c.urlAttr);

			$link
				.attr("href", url)
				.removeAttr(c.urlAttr)
				.addClass(c.clickableClass);
		});
}

export function ensureStyles($container) {
	return $container.find(c.ensureStyleLoaded).height() > 0;
}

export function bindResizeEvent(func) {

	if (isMobile()) {
		window
			.matchMedia("(orientation: portrait)")
			.addListener(func);
	} else {
		$(window).on("resize", func);
	}
}

export function isElementInViewport(element, spread) {

	spread = spread || 0;

	const $element = $(element);
	if ($element.length === 0) {
		return false;
	}

	const windowsHeight = $(window).height();
	const viewportScrollTop = $(window).scrollTop();

	const areaTop = viewportScrollTop < spread ? 0 : viewportScrollTop - spread;
	const areaBottom = viewportScrollTop + windowsHeight + spread;

	const elementTop = $element.offset().top;
	const elementBottom = elementTop + $element.height();

	return elementBottom <= areaBottom
		&& elementTop >= areaTop
		&& $element.is(":visible");
}

export function dispatchEvent(logCall, element, action, actionContext, eventType) {
	const clientParams = makeClientParams(element, action, actionContext, eventType);
	ccs_cc_log.dispatchEvent(logCall, clientParams);
}

export function makeClientParams(element, action, actionContext, eventType) {
	const clientParams = {};
	const $parentSection = $(element).closest("[data-elementid]");

	clientParams.ElementIds = $parentSection.attr("data-elementid");

	const elementContext = $parentSection.attr("data-context");
	if (elementContext) {
		clientParams.ElementContext = elementContext;
	}

	if (action) {
		clientParams.Action = action;
	}

	if (actionContext) {
		if ($.isPlainObject(actionContext)) {
			actionContext = JSON.stringify(actionContext);
		}

		clientParams.ActionContext = actionContext;
	}

	if (eventType) {
		clientParams.Et = eventType;
	}

	return clientParams;
}

// https://www.w3schools.com/js/js_cookies.asp
export function setCookie(cname, cvalue, exdays) {
	var cookie = cname + "=" + cvalue + ";path=/";
	if (exdays) {
		const d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

		cookie += ";expires=" + d.toUTCString();
	}
	document.cookie = cookie;
}

export function getCookie(cname) {
	const name = cname + "=";
	const decodedCookie = decodeURIComponent(document.cookie);
	const ca = decodedCookie.split(";");
	for (let i = 0; i < ca.length; i++) {
		let c = ca[i];
		while (c.charAt(0) === " ") {
			c = c.substring(1);
		}
		if (c.indexOf(name) === 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
// end cookie

export function fixBlockWidth($ep) {
	var $block = $ep,
		timer;

	if ($block.closest("td").length) {
		const fixInvoker = function () {
			if ($block.is(":visible")) {
				$block.css("position", "absolute");

				const width = $block.parent().width();
				if (width !== 0) {
					$block.width(width);
				}

				$block.css("position", "");
			} else {
				clearTimeout(timer);
				timer = setTimeout(fixInvoker, 300);
			}
		};

		bindResizeEvent(fixInvoker);
		fixInvoker.call();
	}
}

export function updateOnVisible($ep, elements, callback, timeout) {

	if (!$ep || typeof callback !== "function") {
		console.error("updateOnVisible: '$ep' or 'callback' parameters are not defined");
		return;
	}

	elements = elements || $ep;
	timeout = timeout || 300;

	if (typeof elements === "string") {
		elements = $ep.find(elements);
	}

	if (elements instanceof $) {
		elements = elements.toArray();
	}

	if (!Array.isArray(elements)) {
		elements = [elements];
	}

	for (let i = 0; i < elements.length; i++) {
		updateOnVisibleImpl($ep, elements[i], callback, timeout);
	}
}

function updateOnVisibleImpl($ep, element, callback, timeout) {
	const $el = $(element);

	if ($el.is(":visible") && ensureStyles($ep)) {
		callback(element);

	} else {
		setTimeout(() => updateOnVisibleImpl(...arguments), timeout);
	}
}
