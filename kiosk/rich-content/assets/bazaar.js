﻿import "jquery";
import "Fancybox";
import { keepFocus } from "./utils";

export default function init(ep) {
	ep.find(".ccs-cc-bazaar-read-more")
		.fancybox({
			autoFocus: true,
			backFocus: true,
			iframe: {
				preload: false
			},
			afterShow(instance, current) {

				if (current.type === "iframe") {
					current.$content.attr("tabindex", 0);
					instance.$refs.toolbar.appendTo(instance.$refs.inner);
				}

				keepFocus(instance.$refs.container[0]);
			}
		});
}