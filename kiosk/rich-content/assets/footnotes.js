﻿import $ from "jquery";
import { expand as expandAccordion } from "./accordion";

const c = {
	linkClass: "ccs-cc-inline-footnote-link",
	footnoteSupLinkSelector: "sup a",
	headerSelector: ".ccs-cc-inline-footer .ccs-cc-inline-acc-header",
	footnoteIdAttr: "data-ccs-note-id",
	offScreenTextClass: "ccs-off-screen"
};

export default function (ep, options) {

	const footnoteIdSelector = `[${c.footnoteIdAttr}]`;

	ep.find(footnoteIdSelector).each(function () {
		const $footnote = $(this);

		const $footnoteAnchors = $footnote.find("a");

		$footnoteAnchors.length > 0
			? updateLink(options, ep, $footnoteAnchors)
			: createNewLink(options,ep, $footnote);
	});
	
	ep.on("click",
		c.footnoteSupLinkSelector,
		() => expandAccordion(ep.find(c.headerSelector)));
}

function updateLink(o, ep, $footnoteAnchors) {
	$footnoteAnchors.each((i, item) => {
		const $footNote = $(item);
		const icId = o.inlineContentId;

		const id = $footNote.attr("id");
		const refId = $footNote.attr("href");

		$footNote.attr({
			"id": id + icId,
			"href": refId + icId
		});

		const $link = ep.find(`a[id='${refId.substring(1)}']`);

		$link.attr({
			"id": $link.attr("id") + icId,
			"href": $link.attr("href") + icId
		});

		$footNote.prepend(
			$("<span/>")
				.addClass(c.offScreenTextClass)
				.text(o.footer.anchorOffScreenText));
	});
}

function createNewLink(o, ep, $footnote) {
	const noteId = $footnote.attr(c.footnoteIdAttr);

	let $footerLink;

	ep.find("sup")
		.each(function () {
			const $footnoteLinkCandidate = $(this);
			const text = $.trim($footnoteLinkCandidate.text());
			if (text !== noteId || $footnoteLinkCandidate.attr(c.footnoteIdAttr)) {
				return;
			}

			const $contentLink = createContentLink(o, noteId);

			if (!$footerLink) {
				$footerLink = createFooterLink(o, noteId);
			}

			$footnoteLinkCandidate.html($contentLink).addClass(c.linkClass);
		});

	if (!$footerLink) {
		$footerLink = $("<a/>").attr("href", "javascript:;").prepend(noteId);
	}

	$footerLink.prepend($footnote.find(`.${c.offScreenTextClass}`));

	$footnote
		.html($footerLink)
		.removeAttr("tabindex");
}

function createContentLink(o, noteId) {
	return createCrossLink(o, noteId, false);
};

function createFooterLink(o, noteId) {
	return createCrossLink(o, noteId, true);
};

function createCrossLink(o, noteId, isFooterLink) {
	const prefix = `ccs-f${o.inlineContentId}`;

	return $("<a/>")
		.text(noteId)
		.attr({
			"id": prefix + (isFooterLink ? "t" : "") + noteId,
			"href": "#" + prefix + (isFooterLink ? "" : "t") + noteId
		});
};
