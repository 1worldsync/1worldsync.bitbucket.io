﻿import $ from "jquery";
import require from "require";
import lightbox from "./lightbox";
import common from "./common";
import Carousel from "./featurecarousel";
import mobileImages from "./mobileimages";
import * as utils from "./utils";
import * as video from "./video";

const c = {
	mobilePrintingVideo: ".ccs-cc-mobileprinting-video a",
	carouselWrapperSelector: ".ccs-cc-fc",
	featureDescription: ".ccs-cc-inline-feature-description"
};

$.extend(c, common);

export default function (ep, o) {

	const $mediaElements = ep.find(c.imageSelector + "," + c.videoSelector + "," + c.mobilePrintingVideo);
	lightbox($mediaElements, o);

	initCarousel(ep, o);
	initFeaturesZoom(ep, o);
	initEmbeddedVideo(ep, o);
	mobileImages(ep, o);
	initFeaturesLazyLoad(ep, o);
	utils.restoreHref(ep.find(c.videoSelector));
	checkInputLabels(ep.find(c.featureDescription));

	if (o.features.autoPlayVideoInViewPort) {
		video.autoPlayInViewport(ep, o.logCall, o.video.pluginPath, o.video.disableCC);
	}

	if (utils.isMobile()) {
		ep.find(".ccs-cc-inline-feature-background").each((_, el) => {
			const $bg = $(el);
			const $video = $bg.find("video");

			if ($video.length) {
				const poster = $video.attr("poster");
				$bg.css("background-image", `url('${poster}')`);
				$video.remove();
			}
		});
	}
}

function initFeaturesZoom(ep, o) {
	const $featureImages = ep.find(c.imageSelector + " img");
	if (!$featureImages.length) {
		return;
	}

	const inlineZoomSettings = getInlineZoomSettings(ep, o);
	if (inlineZoomSettings) {
		require("zoomsl");
	}

	$featureImages.one("load", function () {
		const $img = $(this);

		if ($img.attr("src") === $img.attr("data-large")
			&& (this.clientWidth * 1.2 > this.naturalWidth || this.naturalWidth < 200)) {

			$img.parent().before($img).remove();
		}

		if (inlineZoomSettings && $img.parent().is("a")) {
			$img.imagezoomsl(inlineZoomSettings);
		}
	});
}

function getInlineZoomSettings(ep, o) {
	const zoom = o.features.zoom;

	if (!zoom.enabled || utils.isMobile()) {
		return null;
	}

	let $container = $(".ccs-zoom-container");
	if (!$container.length) {
		$container = $("<div>")
			.addClass("ccs-zoom-container")
			.appendTo(document.body);
	}

	$container.addClass(o.containerClass);

	const zoomSettings = {
		disablewheel: false,
		magnifycursor: "pointer",
		showstatus: false,
		originalsize: true,
		container: $container,
		viewport: ep,
		classmagnifier: "ccs-zoom-magnifier",
		classcursorshade: "ccs-zoom-cursorshade",
		classstatusdiv: "ccs-zoom-statusdiv",
		classtextdn: "ccs-zoom-textdn",
		classtracker: "ccs-zoom-tracker",
		onShow: e => {
			const context = {
				type: "image",
				url: e.setting.largeimage
			};

			utils.dispatchEvent(o.logCall, e.$img, "hover", context);
		}
	};

	if ($.isNumeric(zoom.width) && $.isNumeric(zoom.height)) {
		zoomSettings.magnifiersize = [zoom.width, zoom.height];
	}

	return zoomSettings;
}

function initFeaturesLazyLoad(ep, o) {
	const lazyLoadImages = ep.find(c.imageSelector);
	if (lazyLoadImages.length === 0) {
		return;
	}

	const startImagesLoading = function () {
		for (let i = lazyLoadImages.length - 1; i >= 0; i--) {
			const $img = $(lazyLoadImages[i]).find("img");

			if (o.features.disableLazyLoad || utils.isElementInViewport($img, 500)) {
				const src = $img.data("src");

				$img.attr("src", src)
					.removeAttr("data-src")
					.one("load", e => utils.restoreHref($(e.target).closest(c.imageSelector)));

				if ($img[0].complete) {
					$img.trigger("load");
				}

				lazyLoadImages.splice(i, 1);
			}
		}

		if (lazyLoadImages.length === 0) {
			window.removeEventListener("scroll", startImagesLoading, true);
			window.removeEventListener("resize", startImagesLoading, true);
		}
	}

	const isVisible = function () {
		if (ep.is(":visible")) {
			startImagesLoading();
			window.addEventListener("scroll", startImagesLoading, true);
			window.addEventListener("resize", startImagesLoading, true);
		} else {
			setTimeout(isVisible, 300);
		}
	};

	setTimeout(isVisible, 100); //wait the content loading for the correct calculation of the target area
}

function initEmbeddedVideo(ep, o) {

	ep.find(c.embeddedVideoSelector).each(function () {
		const $wrapper = $(this);

		/*hot-fix for featurecarousel. Do not init cloned videos that uses for infinite loop*/
		if ($wrapper.closest(".ccs-slick-cloned").length !== 0)
			return;

		if (utils.isMobile() && $wrapper.is("[data-video-url$='.flv']")) {
			$wrapper.html(function () {
				return $(this).find("img");
			});
			return;
		}

		$wrapper
			.one("click.initInlineVideo", function () {
				const playerId = $wrapper.data("player");
				if (!$(`#${playerId}`).length) {
					$wrapper.append(
						$("<div class='ccs-cc-inline-embedded-video-wrapper'>").append(
							$("<video>").attr("id", $wrapper.data("player"))));
				}

				$wrapper
					.removeAttr("tabindex")
					.off("keypress.initInlineVideo");

				video.initInline($wrapper, o.logCall, o.video.pluginPath, o.video.disableCC);
			})
			.on("keypress.initInlineVideo", e => e.keyCode === 13 && $wrapper.trigger("click.initInlineVideo"));

		if (utils.isMobile()) {
			const $img = $wrapper.find("img");

			$img.one("load", () => $wrapper.trigger("click.initInlineVideo"));

			if ($img[0].complete) {
				$img.trigger("load");
			}
		}
	});
}

function initCarousel(ep, o) {
	utils.updateOnVisible(ep, c.carouselWrapperSelector, el => new Carousel(el, o));
}

function checkInputLabels(featureDescription) {
	$(featureDescription).find("label").each((_, el) => {
		const $label = $(el);

		if ($label.attr("for").length) {
			const id = $label.attr("for");
			if (!$(c.featureDescription).find("#" + id).length && !$label.text().length) {
				$label.remove();
			}
		}
	});
}
