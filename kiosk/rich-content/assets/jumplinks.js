﻿import $ from "jquery";
import { expand as expandAccordion } from "./accordion";

const c = {
	areaSelector: "div[data-ccs-cc-inline-jumplink]",
	anchorIdAttr: "data-anchor-id",
	holderSelector: "[data-ccs-jumplink-container]",
	itemClass: "ccs-cc-inline-jumplink-item",
	separatorClass: "ccs-cc-inline-jumplink-separator",
	containerClass: "ccs-cc-inline-jumplink-container",
	textAttr: "data-ccs-cc-inline-jumplink",
	hiddenAttr: "data-ccs-jumplink-hidden"
};

export default function (ep, options) {
	const o = options.jumplinks;

	if (!o.enabled) {
		return;
	}

	let jumplinksHtml = o.description;

	ep.find(c.areaSelector).each(function () {
		const $jumplinkArea = $(this);

		const text = $jumplinkArea.attr(c.textAttr);
		const anchorId = $jumplinkArea.attr("id");

		if (!text || !anchorId) return;

		if (jumplinksHtml !== o.description) {
			jumplinksHtml += `<span class='${c.separatorClass}'>|</span>`;
		}

		jumplinksHtml += $("<a></a>")
			.addClass(c.itemClass)
			.attr("href", `#${anchorId}`)
			.attr(c.anchorIdAttr, anchorId)
			.attr("title", text)
			.text(text)
			.wrap("<div></div>")
			.parent()
			.html();
	});

	jumplinksHtml = `<span class='${c.containerClass}'>${jumplinksHtml}</span>`;

	ep.find(c.areaSelector).each(function () {
		const $jumplinkArea = $(this);

		if (!$jumplinkArea.attr(c.hiddenAttr)) {
			$jumplinkArea.find(c.holderSelector).append(jumplinksHtml);

			const anchorId = $jumplinkArea.attr("id");
			const $currentJumplinkItem = $jumplinkArea.find(`a[${c.anchorIdAttr}='${anchorId}']`);
			$currentJumplinkItem.prev(`.${c.separatorClass}`).remove();
			$currentJumplinkItem.remove();
		}
	});

	ep.on(
		"click",
		`.${c.itemClass}`,
		function (event) {
			const areaSelector = `#${$(event.currentTarget).attr(c.anchorIdAttr)}`;
			const $area = $(areaSelector);

			expandAccordion($area);
		}
	);
}
