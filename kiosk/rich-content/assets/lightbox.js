﻿import "Fancybox";
import $ from "jquery";
import c from "./common";
import { dispatchEvent, keepFocus } from "./utils";
import { init as initVideo } from "./video";

const playerId = "ccs-cc-videoplayer";

export default function ($mediaElements, o, overrides) {
	if (o.lightbox.disabled) {
		$mediaElements.on("click", e => {
			e.preventDefault();
			e.stopPropagation();
		});

		return;
	}

	initFancybox($mediaElements, o, overrides);
}

function initFancybox($mediaElements, o, overrides) {
	const config = {
		smallBtn: false,
		loop: true,
		autoFocus: true,
		isRtl: o.isRtl,
		touch: !o.isRtl,
		baseClass: `ccs-fancybox-inline-content ${o.containerClass}`,
		beforeLoad(instance, current) {
			const $link = current.opts.$orig;
			const mediaType = $link.attr(c.mediaTypeAttr);

			current.type = mediaType; // for images without extension

			if (mediaType === "video") {
				current.type = "html";
				current.contentType = "video";
				current.src = `<div><div id='${playerId}-${current.index}'></div></div>`;
			}
		},
		afterLoad(instance, current) {
			const $link = current.opts.$orig;
			const mediaType = $link.attr(c.mediaTypeAttr);
			const isImage = mediaType === "image";

			if (isImage) {
				addPoweredByLogo(current.$content, o.lightbox.poweredByLogoUrl);

				const alt = current.opts.caption || $link.find("img").attr("alt");
				current.$image.attr("alt", alt);

				if (instance.currIndex === current.index && instance.firstRun) {
					dispatchEvent(o.logCall, $link, "zoom-in", { type: mediaType, url: current.src });
				}
			}
		},
		beforeShow(instance, current) {
			if (instance.currIndex === instance.prevIndex) {
				return;
			}

			const $link = current.opts.$orig;
			const direction = instance.currIndex - instance.prevIndex;

			const context = {
				subelement: "lightbox",
				scroll: direction > 0 ^ o.isRtl ? "right" : "left",
				target: instance.currIndex
			};

			dispatchEvent(o.logCall, $link, "navigate-content", context);
		},
		afterShow(instance, current) {
			const $link = current.opts.$orig;
			const mediaType = $link.attr(c.mediaTypeAttr);

			if (mediaType === "video") {
				const settings = {
					playerId: playerId + "-" + current.index,
					file: $link.data("video-url") || $link.attr("href"),
					transcodedFile: $link.data("transcoded-video-url"),
					ccUrl: $link.data("cc-url"),
					ccLang: $link.data("cc-lang"),
					height: "100%",
					width: "100%",
					enableAutosize: true,
					playOnInit: true,
					pluginPath: o.video.pluginPath,
					disableCC: o.video.disableCC
				};

				initVideo($link, o.logCall, settings, true);
			}

			if (current.type === "image") {
				current.$image.attr("tabindex", 0);
				current.$image.focus();
			}

			keepFocus(instance.$refs.container[0]);
		}
	};

	if (o.isRtl === true) {
		config.btnTpl = {
			arrowRight: $.fancybox.defaults.btnTpl.arrowLeft,
			arrowLeft: $.fancybox.defaults.btnTpl.arrowRight
		};
	}

	$.extend(config, overrides);

	$mediaElements.fancybox(config);
}


function addPoweredByLogo($element, url) {
	if (!url) return;

	$("<div>")
		.addClass("ccs-cc-inline-poweredby-logo")
		.append(`<img src='${url}' alt='' />`)
		.bind("contextMenu", () => false)
		.appendTo($element);
}
