﻿import "jquery";

const c = {
	compatibleItemsSelector: ".ccs-cc-inline-section.ccs-cc-inline-compatibleitems",
	compatibleItemsExpandLinkSelector: ".ccs-cc-inline-compatibleitems-expand-link"
};

export default function (ep) {
	const $compatibleItems = ep.find(c.compatibleItemsSelector);

	const $compatibleItemsExpandLink = $compatibleItems.find(c.compatibleItemsExpandLinkSelector);

	$compatibleItemsExpandLink.on("click", () => {
		if (!$compatibleItems.hasClass("ccs-cc-inline-compatibleitems-expanded")) {
			$compatibleItems.addClass("ccs-cc-inline-compatibleitems-expanded");
		}
	});
}
