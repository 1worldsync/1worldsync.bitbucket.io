# 360 Photography Trade Show Kiosk Demo
Updated May 2020

Double click to open `start.html` in your browser to view demonstration

Directory structure:
- /css/ Shared styles
- /img/ Shared images
- /javascript/ Carousel plugin
- /magic360/ Gallery plugin
- /spin-gallery/ Photography content
- /studio-walkthrough/ Video content
- /start.html/ Entry point
